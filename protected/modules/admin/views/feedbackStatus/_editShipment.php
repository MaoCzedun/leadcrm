<div id="success" class="addShipment" >
    <div class="panel-heading border login_heading"></div>
    Ваши данные успешно обновлены
</div>
<div id="editShipment" class="addShipment">
    <div class="panel-heading border login_heading">Редактировать уведомление</div>
    
    <?php echo CHtml::beginForm();?> 
        <div class="form-group">
            <?php echo CHtml::activeLabel($shipmentModel, 'to_status_id')?>
            <?php echo CHtml::activeDropDownList($shipmentModel,'to_status_id',$listStatus,array('class'=>'form-control'));?>
        </div> 
        <div class="form-group">
            <?php echo CHtml::activeLabel($shipmentModel, 'form_id')?>
            <?php echo CHtml::activeDropDownList($shipmentModel,'form_id',$listForms,array('class'=>'form-control'));?>
        </div> 
    <div class="checkbox checkbox_margin">
        <label class="lable_margin">
            <?php echo CHtml::activeCheckBox($shipmentModel,'is_email',array('class'=>'isEmail'));?>
            <p class="pull-left"> is Email</p>
        </label>
        <label class="lable_margin"> 
            <?php echo CHtml::activeCheckBox($shipmentModel,'is_sms',array('class'=>'isSms'));?>
            <p class="pull-left"> is SMS</p>
        </label>
        <?php  echo CHtml::activeHiddenField($shipmentModel,'id')?> 
        <div class="smsTextField">
            <?php echo CHtml::activeTextField($shipmentModel,'sms_text',array('placeholder'=>'Текст sms сообщения'));  ?>
        </div>
        <div class="emailTextField">
             <?php echo CHtml::activeTextField($shipmentModel,'email_text',array('placeholder'=>'Текст email сообщения'));  ?>
        </div>
        <?php echo CHtml::ajaxSubmitButton('Добавить',$this->createUrl('feedbackStatus/updateShipment'),
                array(    
                    'success'=>'js:function(data){
                        succes(data)
                    }',
              ),array(
                    'class'=>'btn btn-default pull-right',
        ));?>
       </div>
    
      <?php echo CHtml::endForm();?> 
</div>
<script type="text/javascript">
 function succes(data)
 {
     
    var resultValue      =  JSON.parse(data);
    var id          = $('#editShipment  input[id="Shipment_id"] ').val();
    var toStatusId  = $('#editShipment select[id=Shipment_to_status_id]').val() ;
    var idForm      = $('#editShipment select[id="Shipment_form_id"]').val();
    var isEmail     = $('#editShipment input[id="Shipment_is_email"]').attr('checked');
    var isSms       = $('#editShipment input[id="Shipment_is_sms"]').attr('checked');
    var textForm    = $('#editShipment select[id=Shipment_form_id] option[value='+idForm+']').text()
    var textStatus  = $('#editShipment select[id=Shipment_to_status_id] option[value='+toStatusId+']').text()
    $('tr[id=Shipment'+id+'] .idFormShipment').text(idForm);
    $('tr[id=Shipment'+id+'] .idStatusShipment').text(toStatusId);
    $('tr[id=Shipment'+id+'] .nameStatusShipment').text(textStatus);
    $('tr[id=Shipment'+id+'] .nameFormShipment').text(textForm);
    $('#success .login_heading').text('Обновление прошло успешно ');
    var mgpup =  $.magnificPopup.instance;
    mgpup.open({
        items:{
            src:'#success',
            type:'inline',
        }
    });
//    if(isSms){
//        $('tr[id=Shipment'+id+'] .idSms').checked = true;
//    }
//    if(isEmail){
//        $('tr[id=Shipment'+id+'] .idEmail').checked = true;
//    }

}
</script>
