<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/rc/js/plugins/magpoup/jquery.magnific-popup.js',  CClientScript::POS_END);?>
<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl.'/rc/bootstrap/bootstrap.min.js',  CClientScript::POS_END);?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/rc/js/plugins/magpoup/magnific-popup.css');?>

<div id="newShipment" class="addShipment">
    <div class="panel-heading border login_heading">Добавить уведомление</div>
    <div id="inner-errors"></div>
    <?php echo CHtml::beginForm();?> 
        <div class="form-group">
            <?php echo CHtml::activeDropDownList($shipmentModel,'to_status_id',$listStatus,array('class'=>'form-control'));?>
        </div> 
        <div class="form-group">
            <?php echo CHtml::activeDropDownList($shipmentModel,'form_id',$listForms,array('class'=>'form-control'));?>
        </div> 
    <div class="checkbox checkbox_margin">
        <label class="label_margin">
            <?php echo CHtml::activeCheckBox($shipmentModel,'is_email',array('class'=>'isEmail'));?>
            <p class="pull-left">is Email</p>
        </label>
    </div>
    <div class="emailTextField">
        <?php echo CHtml::activeTextField($shipmentModel,'email_text',array('placeholder'=>'Текст email сообщения'));  ?>
    </div>
    <div class="checkbox checkbox_margin">
        <label class="label_margin"> 
            <?php echo CHtml::activeCheckBox($shipmentModel,'is_sms',array('class'=>'isSms'));?>
            <p class="pull-left">is SMS</p>
        </label>
    </div>    
    <div class="smsTextField">
        <?php echo CHtml::activeTextField($shipmentModel,'sms_text',array('placeholder'=>'Текст sms сообщения'));  ?>
    </div>
    <!-- Возможно стоит  сделать выпадающий список  --> 
<!--    <div class="form-group">
        <?php // echo CHtml::activeTextField($post,'arrivalDate',array('placeholder'=>'Дата прибытия заказа'));?>
    </div>-->
    <!--<div class="form-group">-->
        <?php // echo CHtml::activeTextField($post,'declarationNumber',array('placeholder'=>'ИНН  заказа'));?>
    <!--</div>-->
    <!--<div class="form-group">-->  
  <?php // $this->widget('zii.widgets.jui.CJuiDatePicker', array(
//                                  'name' => 'created',
//                                  'model' => $post,
//                                  'attribute' => 'arrivalDate',
//                                  'language' => 'ru',
//                                   'options'=>array(
//                                        'dateFormat'=>'yy-mm-dd',
//                                   ),
//                                  'defaultOptions'=>array(
//                                       'dateFormat'=>'yy-mm-dd',
//                                   ),
//                                  'htmlOptions' => array(
//                                        'style' => ' ;',
//                                        'placeholder'=>'Дата  прибытия  заказа',
//       
//                                    ),
//                    ));
                                  ?>
    <!--</div>-->
    <?php echo CHtml::ajaxSubmitButton('Добавить',$this->createUrl('feedbackStatus/addShipment'),
                array(
                    'success'=>'js:function(data){'
                    .' toClose(data);'
                    .'}'
              ),array(
                    'class'=>'btn btn-default pull-right addShipmentButton',
        ));?>
    <?php echo CHtml::endForm();?> 
</div>

<script type="text/javascript">
$(function(){
   $('.isEmail, .isSms').on("click",function(){
        var textareaClass = '';
        
        if($(this).hasClass('isEmail')) textareaClass = 'emailTextField';
        else textareaClass = 'smsTextField';
            
        if($(this).is(":checked"))  $('.'+textareaClass).show();
        else  $('.'+textareaClass).hide();
   });  
});

function toClose(data)
 {
     var resultArray = JSON.parse(data);
     console.log(resultArray);
     if(resultArray.result==='error')
     {
      $('div[id=inner-errors]').html(resultArray.textErorr);
     }
     else if(resultArray.result==='succes')
     {
        $.magnificPopup.close();   
     }
      console.log(resultArray);
 }
 $(document).ready(function() {
	$('.popup-with-form ').click().magnificPopup({
		type: 'inline',
		preloader: false,
                showCloseBtn:true,
//		callbacks:
//                        {
//                          beforeOpen:function(){
//                                $("#newShipment  select[id='Shipment_status_id']  option[value='0']").attr('selected','selected');
//                                $("#newShipment  select[id='Shipment_form_id'] option[value='15']").attr('selected','selected');
//                                $('#newShipment  input[id="Shipment_is_email"] ').attr('checked');
//                                $('#newShipment  input[id="Shipment_is_sms"] ').attr('checked');
//                                console.log("Popup init  beforeOpen");
//                          }, 
//               },
	});
});
</script>
