<?php
$this->pageHeading = Yii::t('admin.crud', 'Feedback Information');

$this->breadcrumbs = array(
    Yii::t('admin.crud', 'Feedback') => Yii::app()->user->checkAccess('view_feedback') ? array('index') : false,
);

$this->menu = array(
    array(
        'label' => '<i class="glyphicon glyphicon-plus"></i> ' . Yii::t('admin.crud', 'Create Status Feedback'),
        'url' => array('create'),
        'visible' => Yii::app()->user->checkAccess('create_status_feedback'),
    ),
    array(
        'label' => '<i class="glyphicon glyphicon-pencil"></i> ' . Yii::t('admin.crud', 'Update Status Feedback'),
        'url' => array('update', 'id' => $model->id),
        'visible' => Yii::app()->user->checkAccess('update_status_feedback'),
    ),
    array(
        'label' => '<i class="glyphicon glyphicon-trash"></i> ' . Yii::t('admin.crud', 'Delete Status Feedback'),
        'url' => '#',
        'linkOptions' => array(
            'submit' => array('delete', 'id' => $model->id),
            'confirm' => Yii::t('admin.crud', 'Вы уверены, что хотите удалить этот статус заказа?'),
        ),
        'visible' => Yii::app()->user->checkAccess('delete_status_feedback'),
    ),
    array(
        'label' => '<i class="glyphicon glyphicon-wrench"></i> ' . Yii::t('admin.crud', 'Manage Feedback'),
        'url' => array('index'),
        'visible' => Yii::app()->user->checkAccess('view_status_feedback'),
    ),
);

?>