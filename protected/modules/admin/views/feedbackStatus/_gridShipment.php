<?php
 $this->widget('GridView',array(
        'id'=>'shipment-grid',
        'dataProvider'=>$dataProvider,
        'rowHtmlOptionsExpression' => 'array("id"=>"Shipment"."$data[id]","data-shipment-id"=>$data[id])',
        'columns'=>array(
            array(
                'name'=>'№',
                'value'=>'$data[id]',
                'htmlOptions'=>array('class'=>'idShipment'), 
            ),
            array(
                'name'=>'Имя формы ',
                'value'=>'$data->Form->name',
                'htmlOptions'=>array('class'=>'nameFormShipment'), 
            ),
            array(
                'name'=>'Статус',
                'value'=> '$data->Status->name',
                'htmlOptions'=>array('class'=>'nameStatusShipment'), 
            ),
            array(
//               тут стоит использовать  checkbox 
                'name'=>'СМС',
                'type'=>'raw',
                'value'=>'CHtml::checkBox(null,$data[is_sms]==1 ? true :false,array('."'"."class"."'=>"."'"."isSms"."'".','."'"."disabled"."'=>"."'"."disabled"."'".'))',
//                'value'=>'$data[is_sms]',
            ),
            array(
                
                'name'=>'Эллектронный  адрес ',
                'type'=>'raw',
                'value'=>'CHtml::checkBox(null,$data[is_email]==1 ? true :false,array('."'"."class"."'=>"."'"."isEmail"."'".','."'"."disabled"."'=>"."'"."disabled"."'".'))',
//                'value'=>'$data[is_email]',
            ),
//            array(
//           CHtml::link($text, $url, ''),
//                'type'=>'raw',  
//                'value'=>'',  
//                ''=>'',  
//            ),
            
            array(
                'name'=>'Редактировать',
                'type'=>'html',
                'value'=>'CHtml::link("editPost", "#Shipment'.'$data[id]'.'",array("class"=>"form-edit"))',
            ),
            array(
                'name'=>'FormId',
                'type'=>'html',
                'value'=>function($data){
                    return '<p class="form-id">'.$data->Form->id.'</p>';
                },
                'htmlOptions'=>array('class'=>'idFormShipment'),
            ),
            array(
                'name'=>'StatusId',
                'type'=>'html',
                'value'=>function($data){
                    return '<p class="status-id">'.$data->Status->id.'</p>';
                },
                'htmlOptions'=>array('class'=>'idStatusShipment'),
            ),
                        
        ),
    ));
 ?>