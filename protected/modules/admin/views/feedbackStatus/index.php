<?php

$this->pageHeading = Yii::t('admin.crud', 'Manage FeedbackStatus');

$this->breadcrumbs = array(
    Yii::t('admin.crud', 'Feedback'),
);

$this->menu = array(
    array(
        'label' => '<i class="glyphicon glyphicon-plus"></i> ' . Yii::t('admin.crud', 'Create Feedback Status'),
        'url' => array('create'),
        'visible' => Yii::app()->user->checkAccess('create_feedback_status'),
    ),

);

?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo $this->pageHeading; ?></h3>
    </div>
    <?php
//        Yii::app()->clientScript->registerScript('editname', '
//
//
//        ');


        $url = $this->createUrl('feedbackStatus/ajaxUpdate');

        Yii::app()->clientScript->registerScript('colpick', '
            $(".color").colpick({
                layout:"hex",
                submit: true,
                colorScheme:"dark",
                onChange:function(hsb,hex,rgb,el,bySetColor) {
                    $(el).css("border-right-color","#"+hex);
                    if(!bySetColor) $(el).val(hex);
                },
                onSubmit:function(hsb,hex,rgb,el) {
                    var url = "'.$url.'";

                    var itemId = $(el).parents("tr.feedbackStatusItem").find("td[data-name=\"id\"]").text();
                    var itemName = $(el).parents("tr.feedbackStatusItem").find("td[data-name=\"name\"]").text();
                    var itemColor = $(el).parents("tr.feedbackStatusItem").find("td[data-name=\"color\"] input").val();

                    var postData = {
                        "id": itemId,
                        "name": itemName,
                        "color": itemColor,
                    };

                    $.ajax(
                    {
                        type: "POST",
                        url: url,
                        data: postData,
                        success: function (result)
                        {
                            alert(result);
                        }
                    });

                    $(el).colpickHide();
                },
                submitText: "ОК",
            }).keyup(function(){
                $(this).co, onslpickSetColor(this.value);
            });


            function sendAjaxUpdate(el) {
                var url = "'.$url.'";

                var itemId = $(el).parents("tr.feedbackStatusItem").find("td[data-name=\"id\"]").text();
                var itemName = $(el).parents("tr.feedbackStatusItem").find("td[data-name=\"name\"]").text();
                var itemColor = $(el).parents("tr.feedbackStatusItem").find("td[data-name=\"color\"] input").val();

                var postData = {
                    "id": itemId,
                    "name": itemName,
                    "color": itemColor,
                };

                $.ajax(
                {
                    type: "POST",
                    url: url,
                    data: postData,
                    success: function (result)
                    {
                        alert(result.message);
                    }
                });
            }

            ');



    ?>
    <?php $this->widget('GridView', array(
        'id' => 'feedback-grid',
        'dataProvider' => $provider,
        'rowCssClassExpression' => 'feedbackStatusItem',
        'columns' => array(
            array(
                'name' => 'id',
                'value' => '$data->id',
                'htmlOptions'=>array('data-name' => 'id'),
            ),
            array(
                'name' => 'name',
                'value' => 'CHtml::encode($data->name)', // <i class="glyphicon glyphicon-pencil"></i>
                'htmlOptions'=>array('data-name' => 'name', 'class' => 'edit_name'),
            ),
            array(
                'name' => 'color',
                'type' => 'raw',
                'value'=>function($data){
                    return '<input class="color" style=" width: 95px; height: 25px; border-right: 20px solid #'.$data->color.';" value="'.$data->color.'" type="text">';
                },
                'htmlOptions'=>array('data-name' => 'color'),
            ),
//            array(
//                'class' => 'ButtonColumn',
//                'deleteConfirmation' => 'Вы уверены, что хотите удалить этот статус?',
//                'template' => '{view}' .
//                    (Yii::app()->user->checkAccess('update_feedback_status') ? '{update}' : '').
//                    (Yii::app()->user->checkAccess('delete_feedback_status') ? '{delete}' : ''),
//            ),
        ),
    )); ?>
   
</div>