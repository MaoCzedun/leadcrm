<div class="add-new-sending">
    <h1>Отправка уведомлений</h1>
    <div class="add-new-button"> 
        <a class="popup-with-form" href="#newShipment"> <i class="fa fa-plus-square"></i> <span>Добавить отправку уведомления</span> </a>
    </div>
    <div class="view-status-table">
    <?php 
    $this->renderPartial('_gridShipment',array('dataProvider' =>$dataProvider));
      ?>
    </div>
</div>
<style>
    #yw0_c5,.form-id
    {
        /*visibility: collapse;*/
        display: none;
    }
</style>
<div class="mfp-hide white-popup-block">
    <?php
        $this->renderPartial('addShipment',array('post'=>$post,'shipmentModel'=>$shipmentModel,'listStatus'=>$listStatus,'listForms'=>$listForms));
        $this->renderPartial('_editShipment',array('shipmentModel'=>$shipmentModel,'listStatus'=>$listStatus,'listForms'=>$listForms));
    ?>
</div>
<script type="text/javascript">
jQuery(document).on('click',' a.form-edit',function(){ 
    var rowId = $(this).parents('tr').attr('data-shipment-id'); 
    
    var idShipment = $('tr[id=Shipment' + rowId + '] .idShipment').text();
    var idForm= $('tr[id=Shipment' + rowId + '] .idFormShipment').text();
    var idStatus = $('tr[id=Shipment' + rowId + '] .idStatusShipment').text();
    var isEmailCheckbox = $('tr[id=Shipment' + rowId + '] .isEmail').prop('checked');
    var isSmsCheckbox = $('tr[id=Shipment' + rowId + '] .isSms').prop('checked');
    
    
    
    setSelectedForm(idForm);
    setSelectedStatus(idStatus);
    setPopupCheckbox('Shipment_is_email', isEmailCheckbox, 'emailTextField');
    setPopupCheckbox('Shipment_is_sms', isSmsCheckbox, 'smsTextField');
    
    $('a.form-edit').magnificPopup({
        items: {
            src: '#editShipment',
            type: 'inline'
        },
        callbacks:
                {
                    beforeClose:function()
                      {
                          
                          unsetFormCheckBoxes(idStatus,idForm)
                          unsetPopupCheckboxes();
                      },
                }
    });
});

function unsetPopupCheckboxes(){
    $('#editShipment input[id="Shipment_is_email"]').prop('checked', false);
    $('#editShipment input[id="Shipment_is_sms"]').prop('checked', false);    
}

function setPopupCheckbox(checkboxId, status, textareaClass) {
    var $textarea = $('#editShipment .' + textareaClass);
    
    $('#editShipment input[id="'+checkboxId+'"] ').prop('checked', status);
    status ? $($textarea).show() : $($textarea).hide();
}

function setSelectedForm(idForm) {
    $('#editShipment select[id="Shipment_form_id"] option[value="'+idForm+'"]').prop('selected',true);
}

function setSelectedStatus(idStatus) {
    $('#editShipment select[id="Shipment_to_status_id"] option[value="'+idStatus+'"]').prop('selected',true);
}
function unsetFormCheckBoxes(idStatus,idForm)
{
    console.log(idStatus);
    console.log(idForm);
    $('#editShipment select[id="Shipment_form_id"] option[value="'+idForm+'"]').prop('selected',false);
    $('#editShipment select[id="Shipment_to_status_id"] option[value="'+idStatus+'"]').prop('selected',false);
}
</script>