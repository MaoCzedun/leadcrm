<?php echo CHtml::button('Экспорт',array('submit'=>'client/exportClients')); ?>;
<?php $this->widget('GridView', array(
    'id' => 'client-grid',
    'dataProvider' => $dataProvider,
    'columns' => array(
        'id',
        'name',
        'phone',
        'email',
        'city',
        array(
            'class' => 'ButtonColumn',
            'deleteConfirmation' => 'Вы уверены, что хотите удалить этого клиента?',
             'template' => '{view}'.
                  '{update}' .
                  '{delete}' ,
        ),
    ),
)); ?>
