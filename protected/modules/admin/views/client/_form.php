
<div class="form-group">
    <?php echo CHtml::activeLabel($client,'name',array('class'=>'col-sm-3 control-label'));?>
    <div class="col-sm-9">
                   
                   <?php echo CHtml::activeTextField($client,'name');?>
    </div>
</div>  
<div class="form-group">
    <?php echo CHtml::activeLabel($client,'phone',array('class'=>'col-sm-3 control-label'));?>
    <div class="col-sm-9">
                   
                   <?php $this->widget('CMaskedTextField',array(
                       'mask'=>'+99(999)999-99-99',
                       'model'=>$client,
                       'attribute'=>'phone',
                       'placeholder'=>'9',
                       'htmlOptions'=>array(
                           'class'=>'form-control',
                       ),
                   ));?>
    </div>
</div>
<div class="form-group">
    <?php echo CHtml::activeLabel($client, 'city',array('class'=>'col-sm-3 control-label'));?>
    <div class="col-sm-9">
        <?php echo CHtml::activeTextField($client,'city',array('class' => 'form-control'));?>
    </div>
</div>
<div class="form-group">
    <?php echo CHtml::activeLabel($client,'email',array('class'=>'col-sm-3 control-label'));?>
    <div class="col-sm-9">
        <?php echo CHtml::activeTextField($client,'email');?>        
    </div>
</div>