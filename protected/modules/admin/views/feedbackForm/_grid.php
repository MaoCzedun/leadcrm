<?php
$this->widget('GridView',array(
    'id'=>'good-grid',
  
    'dataProvider'=>$dataProvider,
    'columns'=>array(
        array(
            'name'=>'id',
            'value'=>'$data[id]',
        ),
        array(
            'name'=>'good',
            'value'=>'$data[good_id]',
        ),
        array(
            'name'=>'upsale_url',
            'value'=>'$data[upsale_url]',
        ),
         array(
            
            'class'=>'FormDataColumn',
            'name'=>'code',
            'value'=>$data,       
//            'visible'=>false,
        ),
        //на просмотр
//        array (
//            'name'=> '',
//            'type'=>'raw',
//            'value'=> 'CHtml::link("view", "#FormData'.'$data[id]'.'",array("class"=>"FordDataCodeClass"))',   
//        ),
        array(
            'class'=>'CButtonColumn',
            'buttons'=>array(
//                стоит  накинуть  url иконок 
                'view'=>array(
                    'url'=>  function ($data) {
                        return '#FormData'.$data['id'];    
                    },
                    'options'=>array('class'=>'FordDataCodeClass'),
                    'label'=>'Посмотреть',
                ), 
            ),
            'template'=>'{view}{delete}',
            'deleteButtonUrl' => 'Yii::app()->controller->createUrl('."'".'feedbackForm/delete'."'".',array("extaction"=>"delete","id"=>$data[id]))',
        ),
       

    ),
    
));
?>