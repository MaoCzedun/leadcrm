             
<div class="container clear_both padding_fix">
                    <!--\\\\\\\ container  start \\\\\\-->
                    <div class="row">
                        <div class="col-lg-6">
                            <section class="panel default blue_title h2">
                                <div class="panel-heading">Генератор <span class="semi-bold">формы</span> 
                                </div>
                                <div class="panel-body">

                                    <div class="panel-group accordion accordion-semi" id="accordion3">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title"> <a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#ac3-1"> <i class="fa fa-angle-right"></i> Основные настройки </a> </h4>
                                            </div>
                                            <div style="height: 0px;" id="ac3-1" class="panel-collapse collapse">

                                              <!-- Тут  стоит добавить dropDownList -->
                                                <div class="panel-body">
                                                    <div id="errorFormModel"></div>
                                                    
                                                    <?php echo CHtml::beginForm();?> 
                                                      <h5>Name:</h5>
                                                      <?php echo CHtml::activeTextField($formModel, 'name');?>
                                                      <?php echo CHtml::error($formModel,'name');?>
                                                       <h5>Продукт:</h5>
                                                      <?php echo CHtml::activeDropDownList($formModel,'good_id',  Good::model()->getListGood() , $htmlOptions);?>
                                                       <h5>Upsale Url:</h5>
                                                      <?php echo CHtml::activeTextField($formModel, 'upsale_url', array('value'=>'http://'));?>
                                                      <?php echo CHtml::error($formModel,'upsale_url');?>
                                                       
                                                      <?php echo CHtml::activeHiddenField($formModel,'code');?>
                                                      <?php echo CHtml::activeHiddenField($formModel,'code');?>
                                                      <?php echo CHtml::ajaxSubmitButton('toServer',  $this->createUrl('feedbackForm/create'),array(
                                                       
                                                       'success'=>'function(data) { succesSubmit(data); }',  
                                                      ));
                                                      
                                                      ?>
                                                       <?php echo CHtml::endForm();?>
                                                </div>
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                            function succesSubmit(data){
                                              var json = $.parseJSON(data);  
                                              $.each(json, function(key, value) {
                                              
                                              $('#errorFormModel').text(key).text(value);
                                             });      
                                              console.log(data);
                                         } 
                                        </script>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title"> <a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#ac3-2"> <i class="fa fa-angle-right"></i> Поля формы </a> </h4>
                                            </div>
                                            <div id="ac3-2" class="panel-collapse collapse" style="height: 0px;">
                                                <div class="panel-body">
                                                    <div class="row ">
                                                        <input id="optionFiledNameCheckbox" type="checkbox" checked disabled>
                                                        <label for="optionFiledNameCheckbox">Имя</label>
                                                    </div>
                                                    <div class="row">
                                                        <input id="optionFiledPhoneCheckbox" type="checkbox" checked disabled>
                                                        <label for="optionFiledPhoneCheckbox">Телефон</label>
                                                    </div>
                                                    <div class="row">
                                                        <input id="optionFiledEmailCheckbox" type="checkbox">
                                                        <label for="optionFiledEmailCheckbox">Email</label>
                                                    </div>
                                                    <div class="row">
                                                        <input id="optionFiledAddressCheckbox" type="checkbox">
                                                        <label for="optionFiledAddressCheckbox">Адрес</label>
                                                    </div>
<!--
                                                    <div class="row">
                                                        <input id="innerTextCheckbox" type="checkbox">
                                                        <label for="innerTextCheckbox">Отображать текст внутри полей?</label>
                                                    </div>
-->
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title"> <a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#ac3-3"> <i class="fa fa-angle-right"></i> Вид блока </a> </h4>
                                            </div>
                                            <div id="ac3-3" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <h4>Тень:</h4>
                                                    <ul class="shadowType">
                                                        <li>без тени</li>
                                                    </ul>
                                                    <h4>Текстура:</h4>
                                                    <ul class="textureType">
                                                        <li>без текстуры</li>
                                                    </ul>
                                                    <h4>Прозрачность блока:</h4>
                                                    <div class="sliderWrapper">
                                                        <div id="blockOpacity" class="sliderContainer"></div>
                                                        <div id="blockOpacityText" class="sliderText">1</div>
                                                    </div>
                                                    <h4>Закругление углов блока:</h4>
                                                    <div class="sliderWrapper">
                                                        <div id="blockBorderRadius" class="sliderContainer"></div>
                                                        <div id="blockBorderRadiusText" class="sliderText">0 px</div>
                                                    </div>
                                                    <h4>Цвет блока:</h4>
                                                    <input id="blockBackgroundColor" type="text" class="picker">
                                                    <h4>Внутренние отступы:</h4>
                                                    <div class="row">
                                                        <label for="blockPaddingTop">Верхний:</label>
                                                        <input id="blockPaddingTop" value="20" type="number">
                                                        <span> px</span>
                                                    </div>
                                                    <div class="row">
                                                        <label for="blockPaddingRight">Правый:</label>
                                                        <input id="blockPaddingRight" value="20" type="number">
                                                        <span> px</span>
                                                    </div>
                                                    <div class="row">
                                                        <label for="blockPaddingBottom">Нижний:</label>
                                                        <input id="blockPaddingBottom" value="20" type="number">
                                                        <span> px</span>
                                                    </div>
                                                    <div class="row">
                                                        <label for="blockPaddingLeft">Левый:</label>
                                                        <input id="blockPaddingLeft" value="20" type="number">
                                                        <span> px</span>
                                                    </div>
                                                    <h4></h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title"> <a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#ac3-4"> <i class="fa fa-angle-right"></i> Вид формы </a> </h4>
                                            </div>
                                            <div id="ac3-4" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <h4>Ширина полей:</h4>
                                                    <div class="sliderWrapper">
                                                        <div id="textFieldWidth" class="sliderContainer"></div>
                                                        <div id="textFieldWidthText" class="sliderText">100 px</div>
                                                    </div>
                                                    <h4>Закругление углов полей:</h4>
                                                    <div class="sliderWrapper">
                                                        <div id="textFieldBorderRadius" class="sliderContainer"></div>
                                                        <div id="textFieldBorderRadiusText" class="sliderText">0 px</div>
                                                    </div>
                                                    <h4>Цвет полей:</h4>
                                                    <input id="textFieldBackgroundColor" type="text" class="picker">
                                                    <h4>Цвет текста меток полей:</h4>
                                                    <input id="textFieldLabelColor" type="text" class="picker">
                                                    <h4>Цвет текста полей:</h4>
                                                    <input id="textFieldColor" type="text" class="picker">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title"> <a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="#ac3-5"> <i class="fa fa-angle-right"></i> Вид кнопки </a> </h4>
                                            </div>
                                            <div id="ac3-5" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="innerBlock">
                                                        <h4>Ширина кнопки:</h4>
                                                        <div class="sliderWrapper">
                                                            <div id="buttonWidth" class="sliderContainer"></div>
                                                            <div id="buttonWidthText" class="sliderText">100 px</div>
                                                        </div>
                                                        <h4>Высота кнопки:</h4>
                                                        <div class="sliderWrapper">
                                                            <div id="buttonHeight" class="sliderContainer"></div>
                                                            <div id="buttonHeightText" class="sliderText">30 px</div>
                                                        </div>
                                                        <h4>Внешние отступы:</h4>
                                                        <div class="row">
                                                            <label for="buttonMarginTop">Верхний:</label>
                                                            <input id="buttonMarginTop" value="20" type="number">
                                                            <span> px</span>
                                                        </div>
                                                        <div class="row">
                                                            <label for="buttonMarginRight">Правый:</label>
                                                            <input id="buttonMarginRight" value="20" type="number">
                                                            <span> px</span>
                                                        </div>
                                                        <div class="row">
                                                            <label for="buttonMarginBottom">Нижний:</label>
                                                            <input id="buttonMarginBottom" value="20" type="number">
                                                            <span> px</span>
                                                        </div>
                                                        <div class="row">
                                                            <label for="buttonMargingLeft">Левый:</label>
                                                            <input id="buttonMarginLeft" value="20" type="number">
                                                            <span> px</span>
                                                        </div>
                                                        <h4>Закругление углов кнопки:</h4>
                                                        <div class="sliderWrapper">
                                                            <div id="buttonBorderRadius" class="sliderContainer"></div>
                                                            <div id="buttonBorderRadiusText" class="sliderText">0 px</div>
                                                        </div>
                                                        <h4>Текст кнопки:</h4>
                                                        <input id="orderButtonText" type="text" value="Заказать">
                                                        <h4>Цвет кнопки:</h4>
                                                        <input id="buttonBackgroundColor" type="text" class="picker">
                                                        <h4>Цвет текста кнопки:</h4>
                                                        <input id="buttonTextColor" type="text" class="picker">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </section>
                        </div>
                        <div class="col-lg-6">
                            <section class="panel default blue_title h2">
                                <div class="panel-heading">Конечный <span class="semi-bold"> вид</span> 
                                </div>
                                <div class="panel-body formResultWrapper">

                                    <div class="innerBlock" id="formResult">
                                    </div>


                                </div>
                                <div class="panel-heading">Код <span class="semi-bold"> формы</span> 
                                </div>
                                <div class="panel-body">
                                    <?php echo CHtml::textArea('codeForm');?>
                                </div>
                                <?php echo CHtml::button('Копировать',array('class'=>'orderButton'));?>
                            </section>
                        </div>
                    </div>


                </div>
<!--                 <script src="js/jquery-2.1.0.js"></script>-->
    <!--<script src="js/bootstrap.min.js"></script>-->
    <!--<script src="js/common-script.js"></script>-->
    <!--<script src="js/jquery.slimscroll.min.js"></script>-->
    <!--<script src="js/jquery.sparkline.js"></script>-->
    <!--<script src="js/sparkline-chart.js"></script>-->
    <!--<script src="js/graph.js"></script>-->
    <!--<script src="js/edit-graph.js"></script>-->
    <!--<script type="text/javascript" src="js/jquery-ui.min.js"></script>-->
    <!--<script type="text/javascript" src="js/doT.min.js"></script>-->
    <!--<script type="text/javascript" src="js/form-generator.js"></script>-->
<!--    <script src="js/colpick.js" type="text/javascript"></script>-->

<!--    <script src="plugins/kalendar/kalendar.js" type="text/javascript"></script>-->
    <!--<script src="plugins/kalendar/edit-kalendar.js" type="text/javascript"></script>-->

    <!--<script src="plugins/sparkline/jquery.sparkline.js" type="text/javascript"></script>-->
    <!--<script src="plugins/sparkline/jquery.customSelect.min.js"></script>-->
    <!--<script src="plugins/sparkline/sparkline-chart.js"></script>-->
    <!--<script src="plugins/sparkline/easy-pie-chart.js"></script>-->
    <!--<script src="plugins/morris/morris.min.js" type="text/javascript"></script> -->
    <!--<script src="plugins/morris/raphael-min.js" type="text/javascript"></script>  -->
    <!--<script src="plugins/morris/morris-script.js"></script> -->





    <!--<script src="plugins/demo-slider/demo-slider.js"></script>-->
    <!--<script src="plugins/knob/jquery.knob.min.js"></script>-->




    <!--<script src="js/jPushMenu.js"></script>-->
    <!--<script src="js/side-chats.js"></script>-->
<!--    <script src="js/jquery.slimscroll.min.js"></script>-->
    <!--<script src="plugins/scroll/jquery.nanoscroller.js"></script>-->
    
    <?php $hash ="LeadCrm".crc32(time()."mamamilaramu");?>
    <div id="formTemplate" class="hidden">
        <style>
         <?php echo ".".$hash?> .form-wrapper{
                min-height: 150px;
                margin: 36px;
                
                border-radius: {{=it.blockBorderRadius}}; 
                -moz-border-radius: {{=it.blockBorderRadius}};
                -webkit-border-radius: {{=it.blockBorderRadius}};
                
                margin: 20px auto;
                width: {{=it.textFieldWidth}}; 
                 
                padding: {{=it.blockPaddingTop}} {{=it.blockPaddingRight}} {{=it.blockPaddingBottom}} {{=it.blockPaddingLeft}}; 
                background: rgb(0, 0, 0) transparent; 
                background-color: {{=it.blockBackgroundColor}};
                filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000);
                -ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";
            }
            <?php echo ".".$hash ?> .form-wrapper .field {
                padding: 12px 0 5px 12px;
                opacity: 1;
            }
            <?php echo ".".$hash ?> .form-wrapper .field .div {
                display: block;
            }
            <?php echo ".".$hash ?> .generatedForm {
                padding: 0px;
            }
           <?php echo ".".$hash ?> .generatedForm:after {
                content: '';
                display: block;
                clear: both;
            }
          <?php echo ".".$hash ?> .generatedForm label {
                color: {{=it.textFieldLabelColor}};
                text-align: left;
                font: 14px/24px Helvetica Neue, "Arial", Helvetica, Verdana, sans-serif;
                margin-bottom: 5px;
                display: inline-block;
            }
          <?php echo ".".$hash ?> .generatedForm input[type="text"],
          <?php echo ".".$hash ?> .generatedForm input[type="email"],
          <?php echo ".".$hash ?> .generatedForm input[type="phone"],
          <?php echo ".".$hash ?> .generatedForm textarea,
          <?php echo ".".$hash ?> .generatedForm select {
                width: 93%;
                font: 12px/14px Helvetica Neue, "Arial", Helvetica, Verdana, sans-serif;
                padding: 6px 0;
                color: {{=it.textFieldColor}};
                border: 1px solid #c2d3d7;
                outline: none;
                display: inline-block;
                position: relative;
                z-index: 2;
                background-color: {{=it.textFieldBackgroundColor}};
/*
                box-shadow: 0 0 0 5px #f2f7f9;
                -moz-box-shadow: 0 0 0 5px #f2f7f9;
                -webkit-box-shadow: 0 0 0 5px #f2f7f9;
*/
                border-radius: {{=it.textFieldBorderRadius}};
                -webkit-border-radius: {{=it.textFieldBorderRadius}};
                -moz-border-radius: {{=it.textFieldBorderRadius}};
                
                -webkit-transition: .3s ease-in-out;
                -moz-transition: .3s ease-in-out;
            }
           <?php echo ".".$hash ?> .orderButton {
                float: left;
                border: 1px solid #690000;
                color: {{=it.buttonTextColor}};
                opacity: 1;
                height: {{=it.buttonHeight}};
                width: {{=it.buttonWidth}}; 
                
                border-radius: {{=it.buttonBorderRadius}}; 
                -webkit-border-radius: {{=it.buttonBorderRadius}};
                -moz-border-radius: {{=it.buttonBorderRadius}};
                
                background-color: {{=it.buttonBackgroundColor}}; 
                margin: {{=it.buttonMarginTop}} {{=it.buttonMarginRight}} {{=it.buttonMarginBottom}} {{=it.buttonMarginLeft}};
            }
            .f_100 {
                width: 96%;
                display: inline;
                float: left;
                margin-left: 2%;
                margin-right: 2%;
            }
           .boxNoneShadow {
                -webkit-box-shadow: none;
                -moz-box-shadow: none;
                box-shadow: none;
            }
           .picker {
                margin: 0;
                width: 100px;
                line-height: 20px;
                z-index: 10000;
                border: 1px solid #CECECE;
                border-right: 20px solid #CECECE;
                padding: 5px;
            }
           .transparent {
                width: 100%;
                height: 100%;
                background: #fff;
            }
           .formResultWrapper {
                background: lightgray;
            }
        </style>
        
        
        <div class="<?php echo $hash ?>">
        <div   class='form-wrapper {{=it.shadowTypeCssClass}}'>
          <!--   стоит сменить  экшен   -->
          
           <!-- Данную форму стоит обрабатывать  ajaксом  -->
           <!-- стоит   добавить скрипт проверки правильности  телефона  -->
           <!-- Стоит  записывать  в  данную  форму  скрипт  отправки данных  на форму -->
           <form class='generatedForm'  method='post' >
               <div id="erorDataUrl"></div>           
<!--                <?php echo CHtml::hiddenField('public_key','-----BEGIN PUBLIC KEY-----'.'\n'.
'MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDVd/gb2ORdLI7nTRHJR8C5EHs4'.'\n'.
'RkRBcQuQdHkZ6eq0xnV2f0hkWC8h0mYH/bmelb5ribwulMwzFkuktXoufqzoft6Q'.'\n'.
'6jLQRnkNJGRP6yA4bXqXfKYj1yeMusIPyIb3CTJT/gfZ40oli6szwu4DoFs66IZp'.'\n'.
'JLv4qxU9hqu6NtJ+8QIDAQAB'.'\n'.
'-----END PUBLIC KEY-----');?>  -->
                <?php echo CHtml::hiddenField('good');?>  
                <?php echo CHtml::hiddenField('upsaleUrlForm');?>  
                <?php echo CHtml::hiddenField('leadCrmBaseUrl', Yii::app()->request->hostInfo);?>
                {{?it.isFieldName}}
                <div id='field1-container' class='field f_100 ui-resizable'>
                    <label for='field1'>Имя</label>
                    <input type='text' name='fieldName' id='field1'>
                    <div class='ui-resizable-handle ui-resizable'></div>
                </div>
                {{?}} {{?it.isFieldPhone}}
                <div id='field2-container' class='field f_100 ui-resizable'>
                    <label for='field2'>Телефон</label>
                    <input type='phone' name='fieldPhone' id='field2' >
                    <div class='ui-resizable-handle ui-resizable-e'></div>
                </div>
                
                
                <!-- 
                
                
                   данные окна  всплываються   в завасимости от состояния поля it
                   
                
                -->
                {{?}} {{?it.isFieldEmail}}
                <div id='field3-container' class='field f_100 ui-resizable'>
                    <label for='field3'>Email</label>
                    <input type='phone' name='fieldEmail' id='field3' required='required'>
                    <div class='ui-resizable-handle ui-resizable-e'></div>
                </div>
                
                
                {{?}} {{?it.isFieldAddress}}
                <div id='field4-container' class='field f_100 ui-resizable'>
                    <label for='field4'>Адрес</label>
                    <input type='phone' name='fieldAddres' id='field4' required='required'>
                    <div class='ui-resizable-handle ui-resizable-e'></div>
                </div>
                {{?}}
                <input onSubmit id="orderButtonToServer"   class='orderButton' style='' type='submit' value='{{=it.buttonText}}'>
               
            </form>
        </div>
        </div>
        
        
        
    </div>
    
    <?php 
    
    Yii::app()->clientScript->registerScriptFile('/rc/js/bootstrap.min.js', CClientScript::POS_END);   
    
    Yii::app()->clientScript->registerScriptFile('/rc/js/jquery.slimscroll.min.js', CClientScript::POS_END);   
    
    Yii::app()->clientScript->registerScriptFile('/rc/js/jquery.sparkline.js', CClientScript::POS_END);
    
    Yii::app()->clientScript->registerScriptFile('/rc/js/sparkline-chart.js', CClientScript::POS_END); 
    
    Yii::app()->clientScript->registerScriptFile('/rc/js/graph.js', CClientScript::POS_END);
    
    Yii::app()->clientScript->registerScriptFile('/rc/js/edit-graph.js', CClientScript::POS_END);
    
    Yii::app()->clientScript->registerScriptFile('/rc/js/jquery-ui.min.js', CClientScript::POS_END);
    
    Yii::app()->clientScript->registerScriptFile('/rc/js/doT.min.js', CClientScript::POS_END);
    
    Yii::app()->clientScript->registerScriptFile('/rc/js/form-generator.js', CClientScript::POS_END);   
    
    Yii::app()->clientScript->registerScriptFile('/rc/js/colpick.js', CClientScript::POS_END);   
    
    Yii::app()->clientScript->registerScriptFile('/rc/js/plugins/kalendar/kalendar.js', CClientScript::POS_END);   
    
    Yii::app()->clientScript->registerScriptFile('/rc/js/plugins/kalendar/edit-kalendar.js', CClientScript::POS_END);   
    
    Yii::app()->clientScript->registerScriptFile('/rc/js/plugins/sparkline/jquery.customSelect.min.js', CClientScript::POS_END); 
    
    Yii::app()->clientScript->registerScriptFile('/rc/js/plugins/demo-slider/demo-slider.js', CClientScript::POS_END);   
    
    Yii::app()->clientScript->registerScriptFile('/rc/js/plugins/knob/jquery.knob.min.js', CClientScript::POS_END);   
    
    Yii::app()->clientScript->registerScriptFile('/rc/js/jPushMenu.js', CClientScript::POS_END);   
    
    Yii::app()->clientScript->registerScriptFile('/rc/js/side-chats.js', CClientScript::POS_END);  
    

    
    
    ?>