    <div class="col-md-12 col-xs-6">
        <a href="<?php echo Yii::app()->homeUrl; ?>" class="btn btn-default">
            <i class="glyphicon glyphicon-globe"></i>
            <?php echo Yii::t('admin.crud', 'View the site'); ?>
        </a>
    </div>
    <?php if(Yii::app()->user->checkAccess('view_user')): ?>
        <div class="col-md-12 col-xs-6">
            <a href="<?php echo $this->createUrl('/admin/user'); ?>" class="btn btn-default">
                <i class="glyphicon glyphicon-user"></i>
                <?php echo Yii::t('admin.crud', 'Users'); ?>
            </a>
        </div>
    <?php endif; ?>
    <?php if(Yii::app()->user->checkAccess('view_course')): ?>
        <div class="col-md-12 col-xs-6">
            <a href="<?php echo $this->createUrl('/admin/course'); ?>" class="btn btn-default">
                <i class="glyphicon glyphicon-list"></i>
                <?php echo Yii::t('admin.crud', 'Course'); ?>
            </a>
        </div>
    <?php endif; ?>
    <?php if(Yii::app()->user->checkAccess('view_feedback')): ?>
        <div class="col-md-12 col-xs-6">
            <a href="<?php echo $this->createUrl('/admin/feedback'); ?>" class="btn btn-default">
                <i class="glyphicon glyphicon-list"></i>
                <?php echo Yii::t('admin.crud', 'Feedback'); ?>
            </a>
        </div>
    <?php endif; ?>
    <?php if(Yii::app()->user->checkAccess('view_file')): ?>
        <div class="col-md-12 col-xs-6">
            <a href="<?php echo $this->createUrl('/admin/file'); ?>" class="btn btn-default">
                <i class="glyphicon glyphicon-file"></i>
                <?php echo Yii::t('admin.crud', 'Files'); ?>
            </a>
        </div>
    <?php endif; ?>
    <?php if(Yii::app()->user->checkAccess('view_file_category')): ?>
        <div class="col-md-12 col-xs-6">
            <a href="<?php echo $this->createUrl('/admin/fileCategory'); ?>" class="btn btn-default">
                <i class="glyphicon glyphicon-list"></i>
                <?php echo Yii::t('admin.crud', 'File Categories'); ?>
            </a>
        </div>
    <?php endif; ?>
    <?php if(Yii::app()->user->checkAccess('update_feedback_status_options')): ?>
        <div class="col-md-12 col-xs-6">
            <a href="<?php echo $this->createUrl('/admin/options/feedbackStatusOptions'); ?>" class="btn btn-default">
                <i class="glyphicon glyphicon-wrench"></i>
                <?php echo Yii::t('admin.crud', 'Feedback Status Options'); ?>
            </a>
        </div>
    <?php endif; ?>
    <?php if(Yii::app()->user->checkAccess('update_mail_options')): ?>
        <div class="col-md-12 col-xs-6">
            <a href="<?php echo $this->createUrl('/admin/options/mailOptions'); ?>" class="btn btn-default">
                <i class="glyphicon glyphicon-wrench"></i>
                <?php echo Yii::t('admin.crud', 'Mail Options'); ?>
            </a>
        </div>
    <?php endif; ?>
        <div class="col-md-12 col-xs-6">
            <a href="<?php echo $this->createUrl('/admin/feedbackStatus/'); ?>" class="btn btn-default">
                <i class="glyphicon glyphicon-wrench"></i>
                <?php echo Yii::t('admin.crud', 'Feedback Status'); ?>
            </a>
        </div>
</div>
</div>
<!--\\\\\\\ content panel end \\\\\\-->
</div>
<!--\\\\\\\ inner end\\\\\\-->
</div>
<!--\\\\\\\ wrapper end\\\\\\-->
<!-- Modal -->
<!--<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">-->
<!--    <div class="modal-dialog">-->
<!--        <div class="modal-content">-->
<!--            <div class="modal-header">-->
<!--                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
<!--                <h4 class="modal-title" id="myModalLabel">Compose New Task</h4>-->
<!--            </div>-->
<!--            <div class="modal-body"> content </div>-->
<!--            <div class="modal-footer">-->
<!--                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
<!--                <button type="button" class="btn btn-primary">Save changes</button>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!--<div class="demo"><span id="demo-setting"><i class="fa fa-cog txt-color-blueDark"></i></span> <form><legend class="no-padding margin-bottom-10" style="color:#6e778c;">Layout Options</legend><section><label><input type="checkbox" class="checkbox style-0" id="smart-fixed-header" name="subscription"><span>Fixed Header</span></label><label><input type="checkbox" class="checkbox style-0" id="smart-fixed-navigation" name="terms"><span>Fixed Navigation</span></label><label><input type="checkbox" class="checkbox style-0" id="smart-rigth-navigation" name="terms"><span>Right Navigation</span></label><label><input type="checkbox" class="checkbox style-0" id="smart-boxed-layout" name="terms"><span>Boxed Layout</span></label><span id="smart-bgimages" style="display: none;"></span></section><section><h6 class="margin-top-10 semi-bold margin-bottom-5">Clear Localstorage</h6><a id="reset-smart-widget" class="btn btn-xs btn-block btn-primary" href="javascript:void(0);"><i class="fa fa-refresh"></i> Factory Reset</a></section> <h6 class="margin-top-10 semi-bold margin-bottom-5">Ultimo Skins</h6><section id="smart-styles"><a style="background-color:#23262F;" class="btn btn-block btn-xs txt-color-white margin-right-5" id="dark_theme" href="javascript:void(0);"><i id="skin-checked" class="fa fa-check fa-fw"></i> Dark Theme</a><a style="background:#E35154;" class="btn btn-block btn-xs txt-color-white" id="red_thm" href="javascript:void(0);">Red Theme</a><a style="background:#34B077;" class="btn btn-xs btn-block txt-color-darken margin-top-5" id="green_thm" href="javascript:void(0);">Green Theme</a><a style="background:#56A5DB" class="btn btn-xs btn-block txt-color-white margin-top-5" data-skinlogo="img/logo-pale.png" id="blue_thm" href="javascript:void(0);">Blue Theme</a><a style="background:#9C6BAD" class="btn btn-xs btn-block txt-color-white margin-top-5" id="magento_thm" href="javascript:void(0);">Magento Theme</a><a style="background:#FFFFFF" class="btn btn-xs btn-block txt-color-black margin-top-5" id="light_theme" href="javascript:void(0);">Light Theme</a></section></form> </div>-->




