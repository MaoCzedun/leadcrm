<?php $this->widget('GridView', array(
                     'id' => 'feedback-grid',
                   //    'filter' => $model,
                     'dataProvider' => $provider,
                     'rowCssClassExpression' => function($row, $data) {
                                    return 'feedbackStatus' . "{$data['status_id']}";
                       },
                      'columns' => array(
                     'id',
                     array(
                         'name'=> 'status.name',
                         'value'=> 'CHtml::encode($data->status->name)',
                      ),
                     'create_time:datetime',
                     'update_time:datetime',
                      //     стоит  переработать  немного данный виджет так  что бы  тут  были видны `client_id`
                      array(
                       'name'=> 'manager',
                       // данные  получаем благодаря  связи manager прописаной в модели FeedBack
                       'value'=> 'CHtml::encode($data->manager->username)',
                        ),
                       array(
                       'name'=> 'client.name',
                       // данные  получаем благодаря  связи manager прописаной в модели FeedBack
                       'value'=> 'CHtml::encode($data->client->name)',
                        ),   
                       
                          
array(
'class' => 'ButtonColumn',
'deleteConfirmation' => 'Вы уверены, что хотите удалить эту заявку?',
'template' => '{view}'.
(Yii::app()->user->checkAccess('update_feedback') ? '{update}' : '').
(Yii::app()->user->checkAccess('delete_feedback') ? '{delete}' : ''),
),
),
)); 
              
                       ?>