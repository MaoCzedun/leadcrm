<div class="form-content">
        <?php  $form=$this->beginWidget('ActiveForm', array(
		'id' => 'feedback-form',
		'htmlOptions' => array(
			'class'=>'form-horizontal',
		),
		'enableClientValidation' => true,
		'clientOptions' => array(
			'validateOnSubmit' => true,
			'afterValidate' => 'js:function(f,d,e) {
				if (e) $("html, body").animate({scrollTop: $("#feedback-form").offset().top - 50}, 1000);
				return true;
			}',
		),
	)); ?>
      <?php if(isset($client)) : ?>
	<?php echo $form->errorSummary($client, null, null, array('class' => 'alert alert-danger')); ?>
        <?php echo $form->errorSummary($feedBack, null, null, array('class' => 'alert alert-danger')); ?>
        <?php // echo $form->errorSummary($newPostRecord, null, null, array('class' => 'alert alert-danger')); ?>
	<div class="form-group">
                    <?php  echo $form->labelEx($client, 'name', array('class'=>'col-sm-3 control-label')); ?>
		<div class="col-sm-9">
                    <?php  echo $form->textField($client, 'name', array('class' => 'form-control',)); ?>
                    <?php  echo $form->error($client, 'name'); ?>
		</div>
	</div>
	<div class="form-group">                
                  <?php   echo $form->label($client, 'email', array('class'=>'col-sm-3 control-label')); ?>
                    <div class="col-sm-9">
                        <?php echo $form->textField($client, 'email', array('class' => 'form-control',)); ?>
                        <?php echo $form->error($client, 'email',array('class'=>'help-inline')); ?>
		</div>
	</div>
	<div class="form-group">
                
		<!--<div class="row">-->
                    <?php echo $form->labelEx($client, 'phone', array('class'=>'col-sm-3 control-label')); ?>
		<div class="col-sm-9">
                    <?php 
                        $form->widget('CMaskedTextField',array(
                            'mask'=>'+99(999)999-99-99',
                            'model'=>$client,
                            'attribute'=>'phone',
                            'placeholder'=>'9', 
                            'htmlOptions'=>array(
                                   'class'=>'form-control',
                               ),
                                ));?>
<!-- -->
<!--			--><?php echo $form->error($client, 'phone'); ?>
		</div>
                  </div>
	<div class="form-group">	   
                    <?php  echo $form->label($client, 'city', array('class'=>'col-sm-3 control-label')); ?>
              <div class="col-sm-9">
		    <?php echo $form->textField($client, 'city',array('class' => 'form-control'));?>	
                    <?php  echo $form->error($client, 'city', array('class'=>'help-inline')); ?>
            </div>
        </div>   
        <?php endif;?> 
       <!-- feedback не являеться  объектом  -->
           <div class="form-group">
               <!-- Стоит  пересмотреть метод getStatusList -->
                  	<?php  echo $form->label($feedBack, 'status_id', array('class'=>'col-sm-3 control-label')); ?>
                <div class="col-sm-9">
			<?php  echo $form->dropdownList($feedBack, 'status_id', Feedback::getStatusList(), array(
				'class' => 'form-control',
                                'prompt' => Yii::t('admin.crud', 'Select Value'),
                                'options'=>array(
                                      "1"=>array('selected'=>'selected'),
                                ),
			)); ?>
                </div>
            </div>
            <div class="form-group">
		        <?php echo $form->label($feedBack,'comment',array('class'=>'col-sm-3 control-label')); ?>
                   <div class="col-sm-9">
			<?php echo $form->textField($feedBack,'comment',array('class' => 'form-control'))?>
			<?php echo $form->error($feedBack,'comment',array('class'=>'help-inline')); ?>
                   </div>
            </div>
                

              <?php 
               //                 тут  стоит  добавить виджет отображения  товаров CGridView  or CListView
//                стоит  подумать над тем , что бы связать  таблицу Client и таблицу Good 

               $list = Good::getListGood();
               if($list):?>
                <div class="form-group">
                  	<?php echo $form->label($feedBack, 'good_id', array('class'=>'col-sm-3 control-label')); ?>
                <div class="col-sm-9">
			<?php 
                           if($feedBack->getScenario()=='create')
                           { 
                           //        $selected = "";  
                           }
                           else
                           {
                           //        $selected = "1";
                           } 
                           $selected = "";  
                           echo $form->dropdownList($feedBack, 'good_id', $list, array(
				'class' => 'form-control',
//                                "1"=>array('selected'=>'selected'),
                                 'prompt' => Yii::t('admin.crud', 'Select Value'),
                                 'options'=>array(
                                      //$selected=>array('selected'=>'selected'),
                                  ),
			)); ?>
                    <?php echo $form->error($feedBack,'good_id',array('class'=>'help-inline')); ?>
                      <!-- Добавим поле вывода ошибки -->
                      </div>
                    </div>
                    <div class="form-group">
                        <?php echo $form->label($feedBack, 'declarationNumber', array('class'=>'col-sm-3 control-label'));?>
                        <div class="col-sm-9">
                            <?php echo $form->textField($feedBack, 'declarationNumber',array('class'=>'form-control'));?>
                        </div>    
                        <?php echo $form->error($feedBack,'declarationNumber',array('class'=>'help-inline'));?>
                    </div>
               <?php endif;?>
              
        
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9"> 
			<?php echo CHtml::submitButton(Yii::t('admin.crud', $feedBack->isNewRecord ? 'Create' : 'Update'), array('class'=>'btn btn-primary')); ?>
                    </div>
                </div>
  	<?php $this->endWidget(); ?>
</div> 
