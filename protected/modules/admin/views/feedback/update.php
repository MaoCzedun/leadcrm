<?php

$this->pageHeading = Yii::t('admin.crud', 'Updating Feedback');

$this->breadcrumbs = array(
	Yii::t('admin.crud', 'Feedback') => Yii::app()->user->checkAccess('view_feedback') ? array('index') : false, 
	Yii::t('admin.crud', 'Update'),
);

$this->menu = array(
	array(
		'label' => '<i class="glyphicon glyphicon-plus"></i> ' . Yii::t('admin.crud', 'Create Feedback'), 
		'url' => array('create'),
		'visible' => Yii::app()->user->checkAccess('create_feedback'),
	),
	array(
		'label' => '<i class="glyphicon glyphicon-eye-open"></i> ' . Yii::t('admin.crud', 'View Feedback'), 
		'url' => array('view', 'id' => $feedBack->id),
		'visible' => Yii::app()->user->checkAccess('view_feedback'),
	),
	array(
		'label' => '<i class="glyphicon glyphicon-wrench"></i> ' . Yii::t('admin.crud', 'Manage Feedback'), 
		'url'=>array('index'),
		'visible' => Yii::app()->user->checkAccess('view_feedback'),
	),
);

?>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title"><?php echo $this->pageHeading; ?></h3>
	</div>
	<div class="panel-body">
		<?php $this->renderPartial('_form', array(
			'feedBack' => $feedBack,
                         'client'=>$client,
		)); ?>
	</div>
</div>
