<?php

$this->pageHeading = Yii::t('admin.crud', 'Manage Feedback');

$this->breadcrumbs = array(
    Yii::t('admin.crud', 'Feedback'),
);

$this->menu = array(
    array(
        'label' => '<i class="glyphicon glyphicon-plus"></i> ' . Yii::t('admin.crud', 'Create Feedback'),
        'url' => array('create'),
        'visible' => Yii::app()->user->checkAccess('create_feedback'),
    ),
    array(
        'label' => '<i class="glyphicon glyphicon-search"></i> ' . Yii::t('admin.crud', 'Search'),
        'url' => '#',
        'linkOptions' => array(
            'class' => 'search-button',
            'data-toggle' => 'search-form',
        ),
    ),
    array(
        'label' => '<i class="glyphicon glyphicon-save"></i> Выгрузка в Excel',
        'url' => '#',
        'linkOptions' => array(
            'class' => 'search-button',
            'data-toggle' => 'export-form',
        ),
    ),
);

?>

<div class="panel panel-default search-form" style="display: none;">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo Yii::t('admin.crud', 'Search'); ?></h3>
    </div>
    <div class="panel-body">
        <?php $this->renderPartial('_search',array(
            'model' => $model,
        )); ?>
    </div>
</div>

<div class="panel panel-default export-form" style="display: none;">
    <div class="panel-heading">
        <h3 class="panel-title">Выгрузка в Excel</h3>
    </div>
    <div class="panel-body">
        <?php $this->renderPartial('_export',array(
            'model' => new FeedbackExportFilter(),
        )); ?>
    </div>
</div>
<?php 
// данынй  код  стоит   перенести в  другое место 
//$str = Messages::model()->findByAttributes(array('subject'=>'Доставлено'));
//$data = NewPost::model()->findByPk(1);
// $str->response = str_replace('{client_name}',$data->feedback->client->name,$str->response);
// $str->response = str_replace('TTN',$data['declarationNumber'],$str->response);
// $str->response = str_replace('{дата  прибытия}',$data['arrivalDate'],$str->response);
// echo $str->response;
// 
// 
// 
// 
//Yii::import('application.extensions.nova-poshta-api-2-master.*');
//use LisDev\Tests;
//use LisDev\Delivery;
//$testNP = new NovaPoshtaApi2('27c72a92f4a4499c10671926929cc649');
// Перед генерированием ЭН необходимо получить данные отправителя
// Получение всех отправителей
//$senderInfo = $testNP->getCounterparties('Sender', 1, '', '');
// Выбор отправителя в конкретном городе (в данном случае - в первом попавшемся)
//$sender = $senderInfo['data'][0];
// Информация о складе отправителя


// Список отделений  для  определенного города
//$senderWarehouses = $testNP->getWarehouses($sender['City']);
//$result =  $testNP->documentsTracking('20450002165074');

//                $params =  array(
//                      'IntDocNumber'=>'20450002165074'
//                );
//$result = $testNP->getDocumentList($params);
//// Генерирование новой накладной
//$result = $testNP->newInternetDocument(
//    // Данные отправителя
//    array(
//        // Данные пользователя
//        'FirstName' => $sender['FirstName'],
//        'MiddleName' => $sender['MiddleName'],
//        'LastName' => $sender['LastName'],
//        // Вместо FirstName, MiddleName, LastName можно ввести зарегистрированные ФИО отправителя или название фирмы для юрлиц
//        // (можно получить, вызвав метод getCounterparties('Sender', 1, '', ''))
//        // 'Description' => $sender['Description'],
//        // Необязательное поле, в случае отсутствия будет использоваться из данных контакта
//        // 'Phone' => '0631112233',
//        // Город отправления
//        // 'City' => 'Белгород-Днестровский',
//        // Область отправления
//        // 'Region' => 'Одесская',
//        'CitySender' => $sender['City'],
//        // Отделение отправления по ID (в данном случае - в первом попавшемся)
//        'SenderAddress' => $senderWarehouses['data'][0]['Ref'],
//        // Отделение отправления по адресу
//        // 'Warehouse' => $senderWarehouses['data'][0]['DescriptionRu'],
//    ),
//    // Данные получателя
//    array(
//        'FirstName' => 'Петя',
//        'MiddleName' => 'Пятишевич',
//        'LastName' => 'Пяточкин',
//        'Phone' => '0509923877',
//        'City' => 'Киев',
//        'Region' => 'Киевская',
//        'Warehouse' => 'Отделение №3: ул. Калачевская, 13 (Старая Дарница)',
//    ),
//    array(
//        // Дата отправления
//        'DateTime' => date('d.m.Y'),
//        // Тип доставки, дополнительно - getServiceTypes()
//        'ServiceType' => 'WarehouseWarehouse',
//        // Тип оплаты, дополнительно - getPaymentForms()
//        'PaymentMethod' => 'Cash',
//        // Кто оплачивает за доставку
//        'PayerType' => 'Recipient',
//        // Стоимость груза в грн
//        'Cost' => '1500',
//        // Кол-во мест
//        'SeatsAmount' => '10',
//        // Описание груза
//        'Description' => 'Кастрюля',
//        // Тип доставки, дополнительно - getCargoTypes
//        'CargoType' => 'Cargo',
//        // Вес груза
//        'Weight' => '15',
//        // Объем груза в куб.м.
//        'VolumeGeneral' => '1.5',
//        // Обратная доставка
//        'BackwardDeliveryData' => array(
//            array(
//                // Кто оплачивает обратную доставку
//                'PayerType' => 'Recipient',
//                // Тип доставки
//                'CargoType' => 'Money',
//                // Значение обратной доставки
//                'RedeliveryString' => 4552,
//            )
//        )
//    )
//);
//$testNP = new NovaPoshtaApi2Test();
//VarDumper::dump($result);
  ?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo $this->pageHeading; ?></h3>
    </div>
    
    <?php $this->widget('application.modules.admin.components.StatusStyles'); ?>
    <?php // $this->widget(Yii::getPathOfAlias('application').'\modules\admin\components\StatusStyles')?>
    <?php $this->renderPartial('_grid', array('model' => $model, 'provider' => $provider)); ?>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="pull-right">
            На странице:
            <a href="<?php echo $this->createUrl('index', array('param_feedbackPerPage' => 10, 'feedbackStatusId' => $feedbackStatusId)); ?>">10</a>
            <a href="<?php echo $this->createUrl('index', array('param_feedbackPerPage' => 20, 'feedbackStatusId' => $feedbackStatusId)); ?>)); ?>">20</a>
            <a href="<?php echo $this->createUrl('index', array('param_feedbackPerPage' => 50, 'feedbackStatusId' => $feedbackStatusId)); ?>)); ?>">50</a>
        </div>
    </div>
</div>
 