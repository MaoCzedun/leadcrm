<?php

$this->pageHeading = Yii::t('admin.crud', 'Feedback Information');

$this->breadcrumbs = array(
	Yii::t('admin.crud', 'Feedback') => Yii::app()->user->checkAccess('view_feedback') ? array('index') : false, 
);

$this->menu = array(
	array(
		'label' => '<i class="glyphicon glyphicon-plus"></i> ' . Yii::t('admin.crud', 'Create Feedback'), 
		'url' => array('create'),
		'visible' => Yii::app()->user->checkAccess('create_feedback'),
	),
	array(
		'label' => '<i class="glyphicon glyphicon-pencil"></i> ' . Yii::t('admin.crud', 'Update Feedback'), 
		'url' => array('update', 'id' => $model->id),
		'visible' => Yii::app()->user->checkAccess('update_feedback'),
	),
	array(
		'label' => '<i class="glyphicon glyphicon-trash"></i> ' . Yii::t('admin.crud', 'Delete Feedback'), 
		'url' => '#', 
		'linkOptions' => array(
			'submit' => array('delete', 'id' => $model->id),
			'confirm' => Yii::t('admin.crud', 'Are you sure you want to delete this feedback?'),
		),
		'visible' => Yii::app()->user->checkAccess('delete_feedback'),
	),
	array(
		'label' => '<i class="glyphicon glyphicon-wrench"></i> ' . Yii::t('admin.crud', 'Manage Feedback'), 
		'url' => array('index'),
		'visible' => Yii::app()->user->checkAccess('view_feedback'),
	),
);

?>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title"><?php echo $this->pageHeading; ?></h3>
	</div>
	<?php $this->widget('DetailView', array(
		'data' => $model,		
		'attributes' => array(
//                     Имеем неправильные  поля ,  так как   загружаем модель FeedBack  в которой  нет  таких полей
			'id:text:№',
                    // стоит   использовать отношение
                        'good.name',
			'client.name',
			'client.email',
			'client.phone',
			'mobile_device:boolean',
			'create_time:datetime',
			'update_time:datetime',
                                            
// тут  вызываем связь manager , прописаную в  модели feed_back
			'manager',
//                     вызываем  связь client1
                        'client.id',
			array(
				'name' => 'utm_content',
				'visible' => Yii::app()->user->checkAccess('admin'),
			),
			array(
				'name' => 'utm_campaign',
				'visible' => Yii::app()->user->checkAccess('admin'),
			),
			array(
				'name' => 'utm_medium',
				'visible' => Yii::app()->user->checkAccess('admin'),
			),
			array(
				'name' => 'utm_source',
				'visible' => Yii::app()->user->checkAccess('admin'),
			),
			array(
				'name' => 'utm_term',
				'visible' => Yii::app()->user->checkAccess('admin'),
			),
			array(
				'name' => 'referrer',
				'type' => 'url',
				'visible' => Yii::app()->user->checkAccess('admin'),
			),
		),
	)); ?>
</div>
<!---->
<!--<div class="panel panel-default">-->
<!--	<div class="panel-heading">-->
<!--		<h3 class="panel-title">История статусов</h3>-->
<!--	</div>-->
<!--	--><?php //$this->widget('GridView', array(
//		'id' => 'feedback-grid',
//		'dataProvider' => $model->statusLog(),
//		'columns' => array(
//			'id',
//			'status',
//			'comment',
//			'manager',
//		),
//	)); ?>
<!--</div>-->

