<?php 

header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
header ( "Pragma: no-cache" );
header ( "Content-type: application/x-msexcel" );
header ( "Content-Disposition: attachment; filename=feedback.xlsx" );

$this->widget('XlsxRenderer', array(
	'provider' => $provider,
	'attributes' => array(
		'id',
		'name',
		'email',
		'phone',
		'mobile_device:boolean',
		array(
			'name' => 'confirmed',
			'type' => 'boolean',
			'visible' => Yii::app()->user->checkAccess('admin'),
		),
		'status',
		'comment',
		'create_time:datetime',
		'update_time:datetime',
		'manager',
		array(
			'name' => 'utm_content',
			'visible' => Yii::app()->user->checkAccess('admin'),
		),
		array(
			'name' => 'utm_campaign',
			'visible' => Yii::app()->user->checkAccess('admin'),
		),
		array(
			'name' => 'utm_medium',
			'visible' => Yii::app()->user->checkAccess('admin'),
		),
		array(
			'name' => 'utm_source',
			'visible' => Yii::app()->user->checkAccess('admin'),
		),
		array(
			'name' => 'utm_term',
			'visible' => Yii::app()->user->checkAccess('admin'),
		),
		array(
			'name' => 'referrer',
			'visible' => Yii::app()->user->checkAccess('admin'),
		),
	),
));
