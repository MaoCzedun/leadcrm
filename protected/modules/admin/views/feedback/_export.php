<div class="form-content">

	<?php $form = $this->beginWidget('ActiveForm', array(
		'action' => Yii::app()->controller->createUrl('export'),
		'htmlOptions' => array(
			'class'=>'form-horizontal',
		),
		'method' => 'get',
	)); ?>
		
		<div class="form-group">
			<?php echo $form->label($model, 'id_start', array('class'=>'col-sm-3 control-label')); ?>
			<div class="col-sm-9">
				<?php echo $form->textField($model, 'id_start', array(
					'class' => 'form-control',
				)); ?> 
			</div>
		</div>
		
		<div class="form-group">
			<?php echo $form->label($model, 'id_end', array('class'=>'col-sm-3 control-label')); ?>
			<div class="col-sm-9">
				<?php echo $form->textField($model, 'id_end', array(
					'class' => 'form-control',
				)); ?> 
			</div>
		</div>
		
		<div class="form-group">
			<?php echo $form->label($model, 'date_start', array('class'=>'col-sm-3 control-label')); ?>
			<div class="col-sm-9">
				<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
					'attribute' => 'date_start',
					'model' => $model,
					'options' => array(
						'dateFormat' => 'yy-mm-dd',
						'maxDate' => 'today',
					),
					'htmlOptions' => array(
						'class' => 'form-control timepicker-control datepicker-form-control',
						'readonly' => true,
					),
				)); ?> 
			</div>
		</div>
		
		<div class="form-group">
			<?php echo $form->label($model, 'date_end', array('class'=>'col-sm-3 control-label')); ?>
			<div class="col-sm-9">
				<?php $this->widget('zii.widgets.jui.CJuiDatePicker', array(
					'attribute' => 'date_end',
					'model' => $model,
					'options' => array(
						'dateFormat' => 'yy-mm-dd',
						'maxDate' => 'today',
					),
					'htmlOptions' => array(
						'class' => 'form-control timepicker-control datepicker-form-control',
						'readonly' => true,
					),
				)); ?> 
			</div>
		</div>
		
		<div class="form-group">
			<div class="col-sm-offset-3 col-sm-9">
				<?php echo CHtml::submitButton('Выгрузить', array('class'=>'btn btn-default')); ?>
			</div>
		</div>

	<?php $this->endWidget(); ?>

</div>
