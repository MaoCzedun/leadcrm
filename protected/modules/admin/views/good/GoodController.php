<?php

class ClientController extends AdminController
{
    public function actionIndex()
    {
        $model = $this->createSearchModel('Client');
        $model->unsetAttributes();

        $dataProvider = $model->search();
        $dataProvider->pagination = array(
            'pageSize'=>10,
        );

        $this->render('index', array(
            'model' => $model,
            'dataProvider' => $dataProvider,
        ));
    }
    public function  actionView($id){
        $clientModel = Client::model()->findByPk($id);
        $this->render('view',array('client'=>$clientModel));
    }
    public function  actionUpdate($id)
    {
       $clientModel = Client::model()->findByPk($id);
        if(isset($_POST['Client']))
        {
            $clientModel->attributes = $_POST['Client'];
            if($clientModel->validate()){
                $clientModel->save(false);
                $this->redirect($this->createUrl('client/index'));
            }
        }
        $this->render('update',array('client'=>$clientModel));
    }
    public function  actionDelete($id)
    {
        Client::model()->deleteByPk($id);
    }
    public function  actionExportClients()
    {
        Yii::import('application.extensions.ExportXLS.ExportXLS');
        $headerColumns = array();
        foreach (Client::model()->getAttributes() as $key=>$value){
            $headerColumns[]=$key;
        }
        $filename = 'Clients.xls';
        $xls = new ExportXLS($filename);
        $xls->addHeader($headerColumns);
        $clients = Client::model()->findAll();
        foreach ($clients as $client){
            $tmp = array_values($client->attributes);
            $xls->addRow($tmp);
        }
        $xls->sendFile(); 
    }
    public function filters()
    {
        return array(
            'accessControl',
            'postOnly + delete',
        );
    }
}