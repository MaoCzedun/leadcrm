<?php

$this->pageHeading = Yii::t('admin.crud', 'Manage Goods');

$this->breadcrumbs = array(
    Yii::t('admin.crud', 'Good'),
);

$this->menu = array(
    array(
        'label' => '<i class="glyphicon glyphicon-plus"></i> ' . Yii::t('admin.crud', 'Add Goods'),
        'url' => array('create'),
        'visible' => Yii::app()->user->checkAccess('create_goods'),
    ),

);

?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo $this->pageHeading; ?></h3>
    </div>
    <?php $this->widget('GridView', array(
        'id' => 'feedback-grid',
        'dataProvider' => $provider,
        'columns' => array(
            'id',
            'name',
            'description',
            'price',
            'amount',
            'img',
            array(
                'class' => 'ButtonColumn',
                'deleteConfirmation' => 'Вы уверены, что хотите удалить этот статус?',
                'template' =>
                (Yii::app()->user->checkAccess('update_goods') ? '{update}' : '').
                (Yii::app()->user->checkAccess('delete_goods') ? '{delete}' : ''),
            ),

        ),
    )); ?>
</div>