<?php

$this->pageHeading = Yii::t('admin.crud', 'Update Goods');

$this->breadcrumbs = array(
    Yii::t('admin.crud', 'Good') => Yii::app()->user->checkAccess('view_goods') ? array('index') : false,
    Yii::t('admin.crud', 'Update'),
);

?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><?php echo $this->pageHeading; ?></h3>
    </div>
    <div class="panel-body">
        <?php $this->renderPartial('_form', array(
            'model' => $model,
        )); ?>
    </div>
</div>