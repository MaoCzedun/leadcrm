<div class="form-content">
    <?php echo CHtml::beginForm();?>
    <div class="form-group">
        <div  class="col-sm-9">
            <?php echo CHtml::errorSummary($good);?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabel($good,'name',array('class'=>'col-sm-3 control-label'));?>
        <div  class="col-sm-9">
            <?php echo CHtml::activeDropDownList($good, 'id', $listGoods, array('onChange'=>'changeGoodId(this.value)'));?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabel($good, 'amount',array('class'=>'col-sm-3 control-label'));?>
        <div  class="col-sm-9">
            <?php echo CHtml::activeTextField($good,'amount',array('readonly'=>'readonly'));?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabel($good,'numberSaledGoods',array('class'=>'col-sm-3 control-lable'));?>
        <div  class="col-sm-9">
            <?php echo CHtml::activeTextField($good,'numberSaledGoods');?>
        </div>
    </div>
    <div class="form-group">
        <div  class="col-sm-9">
            <?php echo CHtml::submitButton('На сервер'); ?>
        </div>
    </div>
    <?php echo CHtml::endForm();?>
</div>
<script type="text/javascript">
function changeGoodId(data)
{
    var url = "<?php  echo Yii::app()->controller->createAbsoluteUrl('/admin/good/getGoodById');?>";
    $.post(
            url,
            {"Good":{'id':data}},
            function(data)
            {
                
               var  parseData = JSON.parse(data);  
               $('#Good_amount').val(parseData.amount);
               console.log(parseData);
               $('#Good_user_id').val(parseData.user_id);
               $('#Good_price').val(parseData.price);
               $('#Good_purchase_price').val(parseData.purchase_price);
                console.log(parseData);   
            }
            );
    console.log(data);
    console.log(url);
}
</script>