<div class="form-content">
    <?php echo CHtml::beginForm();?>
    <div class="form-group">
        <div class="col-sm-9">
            <?php echo CHtml::errorSummary($good);?>
        </div>
    </div>
    <div class="form-group">     
        <?php echo CHtml::activeLabel($good,'name',array('class'=>'col-sm-3 control-label'));?>
	<div class="col-sm-9">
            <?php echo CHtml::activeDropDownList($good, 'id', $listGoods, array('onChange'=>'changeGoodId(this.value)'));?>        
	</div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabel($good,'amount',array('class'=>'col-sm-3 control-label'));?>
        <div class="col-sm-9">
            <?php echo CHtml::activeTextField($good,'amount');?>    
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabel($good,'user_id',array('class'=>'col-sm-3 control-label'));?>
        <div class="col-sm-9">
            <?php echo CHtml::activeTextField($good,'user_id',array('class'=>'col-sm-3','readonly'=>'readonly'));?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabel($good,'price',array('class'=>'col-sm-3 control-label'));?>
        <div class="col-sm-9">
            <?php echo CHtml::activeTextField($good,'price',array('class'=>'col-sm-3'));?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabel($good,'purchase_price',array('class'=>'col-sm-3 control-label'));?>
        <div class="col-sm-9">
            <?php echo CHtml::activeTextField($good,'purchase_price',array('class'=>'col-sm-3 control-label'));?>
        </div>
    </div>
    <div class="form-group">
        <?php echo CHtml::activeLabel($good,'date_added',array('class'=>'col-sm-3 control-label'));?>
        <div class="col-sm-9">
            <?php 
               $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                    'name' => 'created',
                    'model' => $good,
                    'attribute' => 'date_added',
                    'language' => 'ru',
                    'options' => array(
                        'showAnim' => 'fold',
                        'dateFormat' => 'yy-mm-dd',
                    ),
                ));?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-3 col-sm-9"> 
            <?php echo CHtml::submitButton('Добавить поступление',array('class'=>'btn btn-primary'))?>
        </div>
    </div>
    <?php echo CHtml::endForm();?>
</div>
<script type="text/javascript">
function changeGoodId(data)
{
    var url = "<?php  echo Yii::app()->controller->createAbsoluteUrl('/admin/good/getGoodById');?>";
    $.post(
            url,
            {"Good":{'id':data}},
            function(data)
            {
               var  parseData = JSON.parse(data);  
               $('#Good_amount').val(parseData.amount);
               $('#Good_user_id').val(parseData.user_id);
               $('#Good_price').val(parseData.price);
               $('#Good_purchase_price').val(parseData.purchase_price);
                console.log(parseData);   
            }
            );
    console.log(data);
    console.log(url);
}
</script>