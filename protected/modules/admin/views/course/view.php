<?php

$this->pageHeading = Yii::t('admin.crud', 'Course Information');

$this->breadcrumbs = array(
	Yii::t('admin.crud', 'Course') => Yii::app()->user->checkAccess('view_course') ? array('index') : false, 
);

$this->menu = array(
	array(
		'label' => '<i class="glyphicon glyphicon-plus"></i> ' . Yii::t('admin.crud', 'Create Course'), 
		'url' => array('create'),
		'visible' => Yii::app()->user->checkAccess('create_course'),
	),
	array(
		'label' => '<i class="glyphicon glyphicon-pencil"></i> ' . Yii::t('admin.crud', 'Update Course'), 
		'url' => array('update', 'id' => $model->id),
		'visible' => Yii::app()->user->checkAccess('update_course'),
	),
	array(
		'label' => '<i class="glyphicon glyphicon-trash"></i> ' . Yii::t('admin.crud', 'Delete Course'), 
		'url' => '#', 
		'linkOptions' => array(
			'submit' => array('delete', 'id' => $model->id),
			'confirm' => Yii::t('admin.crud', 'Are you sure you want to delete this course?'),
		),
		'visible' => Yii::app()->user->checkAccess('delete_course'),
	),
	array(
		'label' => '<i class="glyphicon glyphicon-wrench"></i> ' . Yii::t('admin.crud', 'Manage Course'), 
		'url' => array('index'),
		'visible' => Yii::app()->user->checkAccess('view_course'),
	),
);

?>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title"><?php echo $this->pageHeading; ?></h3>
	</div>
	<?php $this->widget('DetailView', array(
		'data' => $model,		
		'attributes' => array(
			'name',
 
		),
	)); ?>
</div>
