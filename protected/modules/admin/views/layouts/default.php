<?php $this->beginContent('admin.views.layouts.main'); ?>
<div class="wrpaper">
    <div class="header_bar">
        <!--\\\\\\\ header Start \\\\\\-->
        <div class="brand">
            <!--\\\\\\\ brand Start \\\\\\-->
            <div class="logo" style="display:block"><span class="theme_color">Lead</span>-CRM</div>
            <div class="small_logo" style="display:none"><img src="" width="50" height="47" alt="s-logo">
                <img src="" width="122" height="20" alt="r-logo"></div>
        </div>
        <!--\\\\\\\ brand end \\\\\\-->
        <div class="header_top_bar">
            <!--\\\\\\\ header top bar start \\\\\\-->
            <a href="javascript:void(0);" class="menutoggle"> <i class="fa fa-bars"></i> </a>
            <a href="<?php echo $this->createUrl('/admin/feedback/create'); ?>" class="add_user"> <i class="fa fa-plus-square"></i> <span> Добавить заказ</span> </a>
             <!-- -->
            

            <div class="top_right_bar">
                <div class="top_right">
                    <div class="top_right_menu">
                        <ul>
                            <li class="dropdown"> <a href="javascript:void(0);" data-toggle="dropdown"> Уведомления <span class="badge badge color_2">6</span> </a>
                                <div class="notification_drop_down dropdown-menu">
                                    <div class="top_pointer"></div>
                                    <div class="box"> <a href="inbox.html"> <span class="block primery_6"> <i class="fa fa-envelope-o"></i> </span> <span class="block_text">Mailbox</span> </a> </div>
                                    <div class="box"> <a href="calendar.html"> <span class="block primery_2"> <i class="fa fa-calendar-o"></i> </span> <span class="block_text">Calendar</span> </a> </div>
                                    <div class="box"> <a href="maps.html"> <span class="block primery_4"> <i class="fa fa-map-marker"></i> </span> <span class="block_text">Map</span> </a> </div>
                                    <div class="box"> <a href="todo.html"> <span class="block primery_3"> <i class="fa fa-plane"></i> </span> <span class="block_text">To-Do</span> </a> </div>
                                    <div class="box"> <a href="task.html"> <span class="block primery_5"> <i class="fa fa-picture-o"></i> </span> <span class="block_text">Tasks</span> </a> </div>
                                    <div class="box"> <a href="timeline.html"> <span class="block primery_1"> <i class="fa fa-clock-o"></i> </span> <span class="block_text">Timeline</span> </a> </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="user_admin dropdown">
                    <a href="javascript:void(0);" data-toggle="dropdown">
                        <span class="user_adminname"><?php echo Yii::app()->user->name; ?></span> <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <div class="top_pointer"></div>
                        <li> <a href="<?php echo $this->createUrl('/admin/user/view/', array('id'=> Yii::app()->user->id)); ?>"><i class="fa fa-user"></i>Профиль</a></li>
                        <li><?php echo CHtml::link('<i class="fa fa-power-off"></i> ' . Yii::t('admin.crud', 'Logout'), array('/admin/default/logout')); ?></li>
                        <!--                        <li> <a href="#"><i class="fa fa-power-off"></i>Выход</a></li>-->
                    </ul>
                </div>
            </div>
        </div>
        <!--\\\\\\\ header top bar end \\\\\\-->
    </div>
</div>
<div class="inner">
    <!--\\\\\\\ inner start \\\\\\--><div class="left_nav">

        <!--\\\\\\\left_nav start \\\\\\-->
        <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 826px;"><div class="left_nav_slidebar" style="overflow: hidden; width: auto; height: 826px;">
                <ul>
                    <li class="left_nav_active theme_border">
                        <a href="<?php echo $this->createUrl('/admin'); ?>">
                            <i class="fa fa-bar-chart-o"></i> Консоль
                            <span class="left_nav_pointer"></span>
                    <span class="plus">
                        <i class="fa fa-plus"></i>
                    </span>
                        </a>
                    </li>
                    <?php if(Yii::app()->user->checkAccess('view_feedback')): ?>
                        <li>
                            <a href="javascript:void(0);">
                                <i class="fa fa-edit"></i> Заявки
                                <span class="plus">
                                    <i class="fa fa-plus"></i>
                                </span>
                            </a>
                            <?php $this->widget('application.modules.admin.components.LeftStatusMenu'); ?>
                        </li>
                    <?php endif; ?>
                    <!-- в дальнейшем стоит  использовать  для данного   эллемента  класс  аналогичный fa-dropbox --> 
                    <li> <a href="javascript:void(0);"> <i class="fa "></i> Уведомления  <span class="plus"><i class="fa fa-plus"></i></span> </a>
                        <ul>
                            <li> <a href="<?php echo $this->createUrl('/admin/feedbackStatus/viewShipment'); ?>"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Просмотр уведомлений </b> </a> </li>
                            <!--<li> <a href="<?php // echo $this->createUrl('/admin/feedbackStatus/addShipmentPage'); ?>"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Добавить уведомление </b> </a> </li>-->
                           <!-- Тут  стоит  добавить   ссылки на  экшены    -->
                        </ul>
                    </li>
                    <li> <a href="javascript:void(0);"> <i class="fa fa-dropbox"></i> Склад <span class="plus"><i class="fa fa-plus"></i></span> </a>
                        <ul>
                            <li> <a href="<?php echo $this->createUrl('/admin/good'); ?>"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Все товары</b> </a> </li>
                            <li> <a href="<?php echo $this->createUrl('/admin/good/create'); ?>"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Добавить товар</b> </a> </li>
                            <li> <a href="<?php echo $this->createUrl('/admin/good/saleOfGoods'); ?>"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Реализайия товара</b> </a> </li>
                            <li> <a href="<?php echo $this->createUrl('/admin/good/goodSupply'); ?>"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Новое поступление</b> </a> </li>
                           <!-- Тут  стоит  добавить   ссылки на  экшены    -->
                            <li> <a href="lockscreen.html"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Поступления</b> </a> </li>
                            <li> <a href="blankpage.html"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Журнал изменений</b> </a> </li>
                        </ul>
                    </li>
                    <li> <a href="javascript:void(0);"> <i class="fa fa-tasks"></i> Генератор форм <span class="plus"><i class="fa fa-plus"></i></span></a>
                        <ul>
                            <li> <a href="<?php echo $this->createUrl('/admin/feedbackForm'); ?>"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Все формы</b> </a> </li>
                            <li> <a href="<?php echo $this->createUrl('/admin/feedbackForm/create'); ?>"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Добавить форму</b> </a> </li>
                        </ul>
                    </li>
                    <li> <a href="javascript:void(0);"> <i class="fa fa-users icon"></i> База клиентов <span class="plus"><i class="fa fa-plus"></i></span> </a>
                        <ul>
                            
                            <!-- стоит  добавить clientController  в href  -->
                            <li> <a href="<?php echo $this->createUrl('/admin/client');?>"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Все клиенты</b> </a> </li>
                            <li> <a href="task.html"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Добавить клиента</b> </a> </li>
                        </ul>
                    </li>
                    <li> <a href="javascript:void(0);"> <i class="fa fa-bar-chart-o"></i> Статистика <span class="plus"><i class="fa fa-plus"></i></span> </a></li>
                    
                    
                    <!-- стоит  добавить  еще один контроллер  statisticsController  -->
                    
                    <li> <a href="javascript:void(0);"> <i class="fa fa-wrench"></i> Настройки <span class="plus"><i class="fa fa-plus"></i></span> </a></li>
                    
                    <!-- стоит  добавить  еще один контроллер  settingsController  -->
                    
                </ul>
            </div><div class="slimScrollBar" style="width: 5px; position: absolute; top: 0px; opacity: 0.4; border-radius: 7px; z-index: 99; right: 1px; height: 826px; display: none; background: rgb(161, 178, 189);"></div><div class="slimScrollRail" style="width: 5px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; opacity: 0.2; z-index: 90; right: 1px; background: rgb(51, 51, 51);"></div></div>
    </div>
    <!--\\\\\\\left_nav end \\\\\\-->
    <div class="contentpanel">



        <?php// $this->renderPartial('admin.views.feedback.create'); ?>

        <div class="container">
            <?php $this->renderPartial('admin.views.layouts.include.flash'); ?>
            <div class="pull-left">
                <?php  $this->widget('zii.widgets.CBreadcrumbs', array(
                    'homeLink' => '<li>' . CHtml::link('<i class="glyphicon glyphicon-home"></i>', array('/admin')) . '</li>',
                    'links' => $this->breadcrumbs,
                    'separator' => ' ',
                    'tagName' => 'ol',
                    'htmlOptions' => array('class' => 'breadcrumb'),
                    'activeLinkTemplate' => '<li><a href="{url}">{label}</a></li>',
                    'inactiveLinkTemplate' => '<li class="active">{label}</li>',
                )); ?>
            </div>
            <div class="pull-right">
                <?php $this->widget('zii.widgets.CMenu', array(
                    'items' => $this->menu,
                    'encodeLabel' => false,
                    'activateItems' => true,
                    'htmlOptions' => array(
                        'class' => 'nav nav-pills',
                    ),
                )); ?>
            </div>
            <div class="clearfix"></div>

            <!--\\\\\\\ contentpanel start\\\\\\-->
            <?php echo $content; ?>
        </div>
    </div>
</div>
<?php $this->endContent(); ?>


