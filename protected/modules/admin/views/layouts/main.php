<!DOCTYPE html>
<html>
	<head>
		<title><?php echo CHtml::encode(Yii::app()->name); ?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/rc/css/normalize.css" type="text/css">
        
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/rc/dashboard/css/font-awesome.css" type="text/css" />
        
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/rc/dashboard/css/bootstrap.min.css" type="text/css" />
        
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/rc/dashboard/css/admin.css" type="text/css">
        
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/rc/dashboard/css/animate.css" type="text/css">
        
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/rc/dashboard/css/font-awesome.css" type="text/css">
        
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/rc/dashboard/css/font-awesome-animation.min.css" type="text/css">
        
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/rc/dashboard/css/jquery-ui.min.css" type="text/css">
        
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/rc/dashboard/css/jquery-ui.theme.min.css" type="text/css">
        
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/rc/dashboard/css/jquerysctipttop.css" type="text/css">
        
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/rc/dashboard/css/select2.css" type="text/css">
        
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/rc/dashboard/css/style.css" type="text/css">
        
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/rc/dashboard/css/style-responsive.css" type="text/css">
        
        <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/rc/css/colpick.css" type="text/css">

        <link href="<?php echo Yii::app()->request->baseUrl; ?>/img/favicon.ico" rel="shortcut icon" />

		<?php
			$cs = Yii::app()->clientScript;

            /**************************************            
              
             *какой  умный  человек  так регестрирует скрипты             
                         
            **************************************/            
            $cs->registerScriptFile('/rc/dashboard/js/jquery-2.1.0.js', CClientScript::POS_HEAD);
            
            $cs->registerScriptFile('/rc/dashboard/js/bootstrap.min.js', CClientScript::POS_END);
            
            $cs->registerScriptFile('/rc/dashboard/js/common-script.js', CClientScript::POS_END);
            
            $cs->registerScriptFile('/rc/dashboard/js/jquery.slimscroll.min.js', CClientScript::POS_END);
            
            $cs->registerScriptFile('/rc/dashboard/js/jquery.sparkline.js', CClientScript::POS_END);
            
            $cs->registerScriptFile('/rc/dashboard/js/sparkline-chart.js', CClientScript::POS_END);
            
            $cs->registerScriptFile('/rc/dashboard/js/graph.js', CClientScript::POS_END);
            
            $cs->registerScriptFile('/rc/dashboard/js/edit-graph.js', CClientScript::POS_END);
//            $cs->registerScriptFile('/rc/dashboard/plugins/sparkline/jquery.sparkline.js', CClientScript::POS_END);
//            $cs->registerScriptFile('/rc/dashboard/plugins/sparkline/jquery.customSelect.min.js', CClientScript::POS_END);
//            $cs->registerScriptFile('/rc/dashboard/plugins/sparkline/sparkline-chart.js', CClientScript::POS_END);
            
            $cs->registerScriptFile('/rc/dashboard/plugins/knob/jquery.knob.min.js', CClientScript::POS_END);
            
            $cs->registerScriptFile('/rc/dashboard/js/jPushMenu.js', CClientScript::POS_END);
            
            $cs->registerScriptFile('/rc/dashboard/js/side-chats.js', CClientScript::POS_END);
            
            $cs->registerScriptFile('/rc/dashboard/js/jquery.slimscroll.min.js', CClientScript::POS_END);
            
            $cs->registerScriptFile('/rc/js/colpick/colpick.js', CClientScript::POS_END);
            
            $cs->registerScriptFile('/rc/js/jeditable/jquery.jeditable.js', CClientScript::POS_END);
            
            $cs->registerScriptFile('/rc/js/admin.js', CClientScript::POS_END);             
                
            
//            Yii::app()->clientScript->registerCssFile('/rc/js/plugins/magpoup/magnific-popup.css');
            $cs->scriptMap=array(
              "jquery.js"=>'/rc/dashboard/js/jquery-2.1.0.js',  
            );
		?>
		
	</head>
	<body class="dark_theme fixed_header left_nav_fixed">
        <div id="main_content">
		    <?php echo $content; ?>
        </div>
	</body>
</html>

