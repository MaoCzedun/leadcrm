<?php if($userModel):?>
    <?php echo CHtml::beginForm();?>
    <br>
    <?php CHtml::$errorCss='parsley-error';?>
    <?php CHtml::$errorMessageCss='parsley-error-label';?>
    <?php echo CHtml::activeLabel($userModel,'email');?>
    <?php echo CHtml::activeTextField($userModel,'email',array('class'=>'form-control','value'=>$userModel->email));?>
    <?php echo CHtml::error($userModel,'email');?>
    <br>
    <?php echo CHtml::activeLabel($userModel,'username');?>
    <?php echo CHtml::activeTextField($userModel,'username',array('class'=>'form-control'));?>
    <?php echo CHtml::error($userModel,'username');?> 
    <br>
    <?php echo CHtml::activeLabel($userModel,'first_Name');?>
    <?php echo CHtml::activeTextField($userModel,'first_Name',array('class'=>'form-control'));?>
    <?php echo CHtml::error($userModel,'first_Name');?>
    <br>
    <?php echo CHtml::activeLabel($userModel,'password_plain');?>
    <?php echo CHtml::activePasswordField($userModel, 'password_plain',array('class'=>'form-control'));?>  
    <br>
    <?php echo CHtml::activeLabel($userModel,'password_confirm');?>
    <?php echo CHtml::activePasswordField($userModel, 'password_confirm',array('class'=>'form-control'));?>  
    <?php echo CHtml::error($userModel,'password_confirm');?>
    <br>
      <!-- тут  возможн  стоит  сделать onSubmit -->
    <?php echo CHtml::submitButton('Save',array('class'=>'btn btn-primary'));?>  
    <?php echo CHtml::endForm();?>  
<?php else:?>
   <p>Sorry than  was error</p>
<?php endif; ?>
