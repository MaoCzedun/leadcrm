<?php

$this->pageHeading = Yii::t('admin.crud', 'Mail Options');

$this->breadcrumbs = array(
	Yii::t('admin.crud', 'Mail Options'),
);

?>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title"><?php echo $this->pageHeading; ?></h3>
	</div>
	<div class="panel-body">	
		<div class="form-content">
			<?php $form=$this->beginWidget('ActiveForm', array(
				'id' => 'mailoptions-form',
				'htmlOptions' => array(
					'class'=>'form-horizontal',
				),
				'enableClientValidation' => true,
				'clientOptions' => array(
					'validateOnSubmit' => true,
					'afterValidate' => 'js:function(f,d,e) {
						if (e) $("html, body").animate({scrollTop: $("#mailoptions-form").offset().top - 50}, 1000);
						return true;
					}',
				),
			)); ?>
			
			<?php echo $form->errorSummary($model, null, null, array('class' => 'alert alert-danger')); ?>

			<div class="form-group">
				<?php echo $form->labelEx($model, 'admin_email', array('class'=>'col-sm-3 control-label')); ?>
				<div class="col-sm-9">
					<?php echo $form->textField($model, 'admin_email', array(
						'class' => 'form-control',
					)); ?> 
					<?php echo $form->error($model, 'admin_email', array('class'=>'help-inline')); ?>
				</div>
			</div>
			<div class="form-group">
				<?php echo $form->labelEx($model, 'subject', array('class'=>'col-sm-3 control-label')); ?>
				<div class="col-sm-9">
					<?php echo $form->textField($model, 'subject', array(
						'class' => 'form-control',
					)); ?> 
					<?php echo $form->error($model, 'subject', array('class'=>'help-inline')); ?>
				</div>
			</div>
			<div class="form-group">
				<?php echo $form->labelEx($model, 'method', array('class'=>'col-sm-3 control-label')); ?>
				<div class="col-sm-9">
					<?php echo $form->dropdownList($model, 'method', array(
						'mail' => 'mail',
						'smtp' => 'smtp',
					), array(
						'class' => 'form-control',
						'prompt' => Yii::t('admin.crud', 'Select Value'),
					)); ?> 
					<?php echo $form->error($model, 'method', array('class'=>'help-inline')); ?>
				</div>
			</div>
			<div class="form-group">
				<?php echo $form->labelEx($model, 'smtp_login', array('class'=>'col-sm-3 control-label')); ?>
				<div class="col-sm-9">
					<?php echo $form->textField($model, 'smtp_login', array(
						'class' => 'form-control',
					)); ?> 
					<?php echo $form->error($model, 'smtp_login', array('class'=>'help-inline')); ?>
				</div>
			</div>
			<div class="form-group">
				<?php echo $form->labelEx($model, 'smtp_pass', array('class'=>'col-sm-3 control-label')); ?>
				<div class="col-sm-9">
					<?php echo $form->textField($model, 'smtp_pass', array(
						'class' => 'form-control',
					)); ?> 
					<?php echo $form->error($model, 'smtp_pass', array('class'=>'help-inline')); ?>
				</div>
			</div>
			<div class="form-group">
				<?php echo $form->labelEx($model, 'smtp_host', array('class'=>'col-sm-3 control-label')); ?>
				<div class="col-sm-9">
					<?php echo $form->textField($model, 'smtp_host', array(
						'class' => 'form-control',
					)); ?> 
					<?php echo $form->error($model, 'smtp_host', array('class'=>'help-inline')); ?>
				</div>
			</div>
			<div class="form-group">
				<?php echo $form->labelEx($model, 'smtp_port', array('class'=>'col-sm-3 control-label')); ?>
				<div class="col-sm-9">
					<?php echo $form->textField($model, 'smtp_port', array(
						'class' => 'form-control',
					)); ?> 
					<?php echo $form->error($model, 'smtp_port', array('class'=>'help-inline')); ?>
				</div>
			</div>
			<div class="form-group">
				<?php echo $form->labelEx($model, 'smtp_ssl', array('class'=>'col-sm-3 control-label')); ?>
				<div class="col-sm-9">
					<?php echo $form->dropdownList($model, 'smtp_ssl', array(
						1 => Yii::t('admin.crud', 'Yes'),
						0 => Yii::t('admin.crud', 'No'),
					), array(
						'class' => 'form-control',
						'prompt' => Yii::t('admin.crud', 'Select Value'),
					)); ?> 
					<?php echo $form->error($model, 'smtp_ssl', array('class'=>'help-inline')); ?>
				</div>
			</div>
			
			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-9">
					<?php echo CHtml::submitButton(Yii::t('admin.crud', 'Update'), array('class'=>'btn btn-primary')); ?>
				</div>
			</div>

			<?php $this->endWidget(); ?>
		</div>
	</div>
</div>
