<?php

$this->pageHeading = Yii::t('admin.crud', 'Feedback Status Options');

$this->breadcrumbs = array(
	Yii::t('admin.crud', 'Feedback Status Options'),
);

?>
<div class="panel panel-default">
	<div class="panel-heading">
		<h3 class="panel-title"><?php echo $this->pageHeading; ?></h3>
	</div>
	<div class="panel-body">	
		<div class="form-content">
			<?php $form=$this->beginWidget('ActiveForm', array(
				'id' => 'feedbackstatusoptions-form',
				'htmlOptions' => array(
					'class'=>'form-horizontal',
				),
				'enableClientValidation' => true,
				'clientOptions' => array(
					'validateOnSubmit' => true,
					'afterValidate' => 'js:function(f,d,e) {
						if (e) $("html, body").animate({scrollTop: $("#feedbackstatusoptions-form").offset().top - 50}, 1000);
						return true;
					}',
				),
			)); ?>

			<?php echo $form->errorSummary($model, null, null, array('class' => 'alert alert-danger')); ?>

			<div class="form-group">
				<?php echo $form->labelEx($model, 'new_status', array('class'=>'col-sm-3 control-label')); ?>
				<div class="col-sm-9">
					<?php echo $form->textField($model, 'new_status', array(
						'class' => 'form-control',
					)); ?>
					<?php echo $form->error($model, 'new_status', array('class'=>'help-inline')); ?>
				</div>
			</div>
			<div class="form-group">
				<?php echo $form->labelEx($model, 'expired_status', array('class'=>'col-sm-3 control-label')); ?>
				<div class="col-sm-9">
					<?php echo $form->textField($model, 'expired_status', array(
						'class' => 'form-control',
					)); ?>
					<?php echo $form->error($model, 'expired_status', array('class'=>'help-inline')); ?>
				</div>
			</div>
			<div class="form-group">
				<?php echo $form->labelEx($model, 'valid', array('class'=>'col-sm-3 control-label')); ?>
				<div class="col-sm-9">
					<?php echo $form->textArea($model, 'valid', array(
						'class' => 'form-control',
					)); ?>
					<?php echo $form->error($model, 'valid', array('class'=>'help-inline')); ?>
				</div>
			</div>
			<div class="form-group">
				<?php echo $form->labelEx($model, 'invalid', array('class'=>'col-sm-3 control-label')); ?>
				<div class="col-sm-9">
					<?php echo $form->textArea($model, 'invalid', array(
						'class' => 'form-control',
					)); ?>
					<?php echo $form->error($model, 'invalid', array('class'=>'help-inline')); ?>
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-9">
					<?php echo CHtml::submitButton(Yii::t('admin.crud', 'Update'), array('class'=>'btn btn-primary')); ?>
				</div>
			</div>

			<?php $this->endWidget(); ?>
<!--            --><?php //$this->widget('GridView', array(
//                'id' => 'states-grid',
//                'dataProvider' => $provider,
//                'columns' => array(
//                    'id',
//                    'name',
//                    array(
//                        'name' => 'color',
//                        'header' => 'Цвет',
//                        'value' => $data->color,
//                    ),
//                    array(
//                        'name' => 'is_default',
//                        'header' => 'Статус по умолчанию?',
//                        'type' => 'boolean',
//                        'filter' => Yii::app()->format->booleanFormat,
//                    ),
//                    array(
//                        'class' => 'ButtonColumn',
//                        'deleteConfirmation' => 'Вы уверены, что хотите удалить эту заявку?',
//                        'template' => '{view}'.
//                            (Yii::app()->user->checkAccess('update_feedback') ? '{update}' : '').
//                            (Yii::app()->user->checkAccess('delete_feedback') ? '{delete}' : ''),
//                    ),
//                ),
//            )); ?>
		</div>
	</div>
</div>
