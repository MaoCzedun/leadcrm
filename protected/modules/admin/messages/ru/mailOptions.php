<?php

return array(
	'Admin Email' => 'Email администратора',
	'Method' => 'Способ отправки',
	'SMTP host' => 'SMPT хост',
	'SMTP login' => 'SMPT логин',
	'SMTP password' => 'SMPT пароль',
	'SMTP port' => 'SMPT порт',
	'SMTP security' => 'Использовать SSL для SMPT',
	'Subject' => 'Тема письма-оповещения о заявке',
);
