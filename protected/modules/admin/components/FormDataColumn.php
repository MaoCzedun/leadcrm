<?php
Yii::import('zii.widgets.grid.CDataColumn');
class FormDataColumn extends CDataColumn
{
    protected  function renderDataCellContent($row, $data) {
        echo '<div  class="mfp-hide white-popup-block" id="FormData'.$data['id'].'">'.$data['code'].'</div>';
    }
}