<?php

Yii::import('ext.xlsxwriter.*');

class XlsxRenderer extends CWidget
{
	public $showHeads = true;
	public $attributes;
	public $provider;
	public $sheetName = 'Sheet 1';
	
	protected $_generator;
	protected $_sheet;
	protected $_formatter;
	
	public function run()
	{
		$this->prepareAttributes();
		
		$this->_generator = new XlsxWriter();
		$this->_generator->setTempDir(Yii::getPathOfAlias('application.runtime'));
		$this->_sheet = $this->_generator->addSheet($this->sheetName);
		
		if ($this->showHeads) {
			$this->renderHeads();
		}
		
		foreach ($this->provider->getData() as $item) {
			$this->renderItem($item);
		}
		
		$this->_generator->writeData();
	}
	
	protected function renderItem($item)
	{
		$formatter = $this->getFormatter();
		$vals = array();
		$this->_sheet->addRow();
		foreach ($this->attributes as $attribute) {
			if (isset($attribute['value'])) {
				$value = $this->evaluateExpression($attribute['value'], array (
					'data' => $item,
				));
			} else {
				$value = CHtml::value($item, $attribute['name']);
			}
			$value = $formatter->format($value, $attribute['type']); 
			$this->renderCell($value, $attribute['type'] == 'number');
		}
	}
	
	protected function renderHeads()
	{
		$model = $this->provider->model;
		$vals = array();
		$this->_sheet->addRow();
		foreach ($this->attributes as $attribute) {
			$val = isset($attribute['label']) ? $attribute['label'] : $model->getAttributeLabel($attribute['name']);
			$this->renderCell($val);
		}
	}
	
	protected function renderCell($value, $number=false)
	{
		if ($number) {
			$this->_sheet->addCell($value, null, true);
		} else {
			$this->_sheet->addCell($value, null, false);
		}
	}
	
	protected function prepareAttributes()
	{
		foreach ($this->attributes as $n => $attribute) {
			if(is_string($attribute)) {
				if(!preg_match('/^([\w\.]+)(:(\w*))?(:(.*))?$/',$attribute, $matches)) {
					throw new CException(Yii::t('zii','The attribute must be specified in the format of "Name:Type:Label", where "Type" and "Label" are optional.'));
				}
				$attribute = array( 
					'name' => $matches[1], 
					'type' => isset($matches[3]) ? $matches[3] : 'text', 
				);
				if(isset($matches[5])) {
					$attribute['label'] = $matches[5]; 
				}
				$this->attributes[$n] = $attribute;
			}
			if (isset($attribute['visible']) && !$attribute['visible']) {
				unset($this->attributes[$n]);
				continue;
			}
			if (! isset($attribute['type'])) {
				$this->attributes[$n]['type'] = 'raw';
			}
		}
	}
	
	public function getFormatter()
	{ 
		if($this->_formatter === null) {
			$this->_formatter = Yii::app()->format; 
		}
		return $this->_formatter; 
	}
	
	public function setFormatter($value) 
	{ 
		$this->_formatter = $value; 
	}
}
