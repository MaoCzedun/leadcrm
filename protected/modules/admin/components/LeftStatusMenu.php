<?php

class LeftStatusMenu extends CWidget {
    public function run() {
        $criteria = new CDbCriteria;
        $criteria->condition = 'user_id=0';

        $statuses = FeedbackStatus::model()->findAll($criteria);

        $this->render('leftStatusMenu',array('statuses'=>$statuses));
    }
}