<?php
class  SmsCurier
{
//    класс   работает неккоректно , стоит  усилить обработку ошибок 
    public $auth=array();
    public $password;
    public $sender;
    public $client; 
//    доступные  нам  кредит
    public $credits;
    public function __construct($login,$password,$sender) {
//    получаем данны для  авторизации ;
        $this->auth['login']= $login;
        $this->auth['password']= $password;
        $this->sender = $sender;
//       наш ресурс отправки сообщений 
       $this->client = new SoapClient('http://turbosms.in.ua/api/wsdl.html');  
    }
//     Функцию  авторизации 
    public function auth()
    {
       $this->client->Auth($this->auth);
    }
    public  function sendSms($textMessage,$phoneNumber)
    {
        $sms=array();
//        $sms['text']=iconv('windows-1251','UTF-8',$textMessage);
        $sms['text']=$textMessage; 
        $sms['destination']=$phoneNumber;
        $sms['sender']=$this->sender;
        $result = $this->client->SendSms($sms); 
        return $result;
    }
    public static  function modernText($str,$data=array())
    {
        if(strpos($str,'client_name'))
        {
            $str= str_replace('{client_name}',$data['name'],$str);
        }
        if(strpos($str,'TTN'))
        {    
            $str= str_replace('TTN',$data['declarationNumber'],$str);
        }
        if(strpos($str,'{дата  прибытия}')){
            $str = str_replace('{дата  прибытия}',$data['arrivalDate'],$str);
        }
        return $str;
    }
    public function  sentDeliveredSms($modelShipment,$feedback)
    {
        $clientModel =  $feedback->client;
        $newPost = $feedback->newPostRecord;
        $phone = str_replace(array('(',')','-'),'', $clientModel->phone);
        $textToSms = SmsCurier::modernText($modelShipment->sms_text,array(
            'name'=>$clientModel->name,
            'declarationNumber'=>$newPost->declarationNumber,
            'arrivalDate'=>$newPost->arrivalDate,
            ));
        $this->auth();
        $this->sendSms($textToSms,$phone);
    }
    public static function getCharset($str)
    {
        define('LOWERCASE',3);
        define('UPPERCASE',1);
        $charsets = Array(
            'k' => 0,
            'w' => 0,
            'd' => 0,
            'i' => 0,
            'm' => 0
        );
        for ( $i = 0, $length = strlen($str); $i < $length; $i++ ) {

            $char = ord($str[$i]);
      //non-russian characters
            if ($char < 128 || $char > 256) continue;
      //CP866
            if (($char > 159 && $char < 176) || ($char > 223 && $char < 242))

                $charsets['d']+=LOWERCASE;
            if (($char > 127 && $char < 160)) $charsets['d']+=UPPERCASE;
      //KOI8-R
            if (($char > 191 && $char < 223)) $charsets['k']+=LOWERCASE;
            if (($char > 222 && $char < 256)) $charsets['k']+=UPPERCASE;
      //WIN-1251
            if ($char > 223 && $char < 256) $charsets['w']+=LOWERCASE;
            if ($char > 191 && $char < 224) $charsets['w']+=UPPERCASE;
      //MAC
            if ($char > 221 && $char < 255) $charsets['m']+=LOWERCASE;
            if ($char > 127 && $char < 160) $charsets['m']+=UPPERCASE;
      //ISO-8859-5
            if ($char > 207 && $char < 240) $charsets['i']+=LOWERCASE;
            if ($char > 175 && $char < 208) $charsets['i']+=UPPERCASE;
        }
        arsort($charsets);
        return key($charsets);
    }
}
