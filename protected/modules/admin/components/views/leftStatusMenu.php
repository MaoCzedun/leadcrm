<ul class="" style="display: none;">
    <li> <a href="/admin/feedback"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Все заказы</b> </a> </li>
    <?php
    foreach ($statuses as $status) {
        echo '<li>'.CHtml::link('<span>&nbsp;</span> <i style="color:#'.$status->color.';" class="fa fa-circle"></i><b>'. $status->name . '</b>', array('feedback/', 'feedbackStatusId' => $status->id)).'</li>';
    }
    ?>
</ul>