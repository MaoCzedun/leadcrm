<?php

class FeedbackFormController extends AdminController
{
    public function actionIndex()
    {
        $dataProvider = new CActiveDataProvider('Form');
        if(isset($_GET['ajax'])){
            $this->renderPartial('_grid',array('dataProvider'=>$dataProvider));
        }
        else{
            $this->render('index',array('dataProvider'=>$dataProvider));
        }
        
    }
    // тут стоит  добавить логике обработку    формы,   и сохранения в  БД  в поле  code 
    public function actionCreate()
    {
        $rsa =  new Crypt_RSA();
        $rsa->setPrivateKeyFormat(CRYPT_RSA_PRIVATE_FORMAT_PKCS1);
        $rsa->setPublicKeyFormat(CRYPT_RSA_PRIVATE_FORMAT_PKCS1);
        
        extract($rsa->createKey());
//        $publickey2 = "-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQC\/tI7cw+gnUPK2LqWp50XboJ1i\njrLDn+4\/gPOe+pB5kz4VJX2KWwg9iYMG9UJ1M+AeN33qT7xt9ob2dxgtTh7Mug2S\nn1TLz4donuIzxCmW+SZdU1Y+WNDINds194hWsAVhMC1ClMQTfldUGzQnI5sXvZTF\nJWp\/9jheCNLDRIkAnQIDAQAB\n-----END PUBLIC KEY-----\n";
        $publickey2  = "-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDVd/gb2ORdLI7nTRHJR8C5EHs4
RkRBcQuQdHkZ6eq0xnV2f0hkWC8h0mYH/bmelb5ribwulMwzFkuktXoufqzoft6Q
6jLQRnkNJGRP6yA4bXqXfKYj1yeMusIPyIb3CTJT/gfZ40oli6szwu4DoFs66IZp
JLv4qxU9hqu6NtJ+8QIDAQAB
-----END PUBLIC KEY-----";
        $formModel = new Form();   
        if(Yii::app()->request->isAjaxRequest)
        {
            if(isset($_POST['Form']))
            {
//                    $formModel->attributes = $_POST['Form'];
                $formModel->code = $_POST['Form']['code'];
                $formModel->good_id= $_POST['Form']['good_id'];
                $formModel->upsale_url = $_POST['Form']['upsale_url'];
                $formModel->name= $_POST['Form']['name'];
                $formModel->user_id = Yii::app()->user->getId();
                if($formModel->validate())
                {
                    $formModel->save(false);
                    $this->refresh();
                    echo CJSON::encode('succes');
                }
                else 
                {
                    echo CJSON::encode($formModel->getErrors()); 
                }            
            }
       
        }
        else 
        {
            $this->render('create',array('formModel'=>$formModel,'publickey'=>$publickey,'publickey2'=>$publickey2));
        }
//        VarDumper::dump($publickey);
    }
    public function  actionTest()
    {
       
        $code = Yii::app()->db->createCommand()
                ->select('code')
                ->from('feedback_form')
                ->queryAll();
        $this->render('test',array('model'=>$code));
    }
    public function actionDelete()
    {
       Yii::app()->db->createCommand()
               ->delete('feedback_form',"id=".$_GET['id']);
    }
    public function filters()
    {
        return array(
            'accessControl',
            'postOnly + delete',
        );
    }
}