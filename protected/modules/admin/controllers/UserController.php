<?php

class UserController extends AdminController 
{
	public function actionIndex()
	{
		$model = $this->createSearchModel('User');
		$provider = $model->search();
		$this->render('index', array(
			'model' => $model,
			'provider' => $provider,
		));
	}
	
	public function actionCreate()
	{
		$model = new User('create');
		if ($this->saveModel($model)) {
			$this->redirect(array('view', 'id' => $model->id));
		}
		$this->render('create', array(
			'model' => $model,
		));
	}
	
	public function actionUpdate($id)
	{
            $userModel = User::model()->findByPk($id);
//                $model->turbosms_password = $_POST['User']['turbosms_password'];
//                $model->turbosms_sender = $_POST['User']['turbosms_sender'];
//                $model->turbosms_login = $_POST['User']['turbosms_login'];
//                $model->newPostKey = $_POST['User']['newPostKey'];
//		if ($this->saveModel($model)) {
                if(isset($_POST['User'])) 
                {   
                    $userModel->attributes = $_POST['User'];
                    if($userModel->validate())
                    {
                        $userModel->update();
                        $this->redirect(array('view', 'id' => $userModel->id));
                    }
                }
		$this->render('update', array(
                    'model' => $userModel,
		));
	}
	
	public function actionDelete($id)
	{
            $model = $this->loadModel($id, 'User');
            $model->delete();
            if(!isset($_GET['ajax'])) {
                $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
            }
	}
	
	public function actionView($id)
	{
            $this->render('view', array(
		'model' => $this->loadModel($id, 'User'),
            ));
	}
        public  function actionDeleteRegister($id,$conString)
        {
            if(isset($id)&&isset($conString))
            {
               if(md5($id."mamamilaramu")===$conString)
               {
                    User::model()->deleteByPk($id);
               } 
            }
        }
        public function actionProfile()
        {
           if(isset($_GET['id']))
           {
                $userModel=User::model()->findByPk($_GET['id']);
                if(md5($_GET['id']."mamamilaramu")===$_GET['conString']){
                    $userModel->status=1;
                    $userModel->role='user';
                }
                if(isset($_POST['User']))       
                {
                    $userModel->attributes=$_POST['User'];
                    if($userModel->validate())
                    {
                        $userModel->update();
                        Yii::app()->user->setFlash('succes','Information added ');
                        $this->refresh();
                    }
                }   
                 
            }
           $this->render('profile',array('userModel'=>$userModel)); 
        }
	public function actionSignup() {
            $id;
            $registerUserModel = new RegisterUser();
            if(Yii::app()->request->isAjaxRequest)
            { 
                if(isset($_POST['User']))
                {
                    $registerUserModel->attributes=  Yii::app()->request->getPost('User');
                    if($registerUserModel->validate())
                    {
                        if(!User::model()->findByAttributes(array('email'=>$registerUserModel->email)))
                        {
                        
                            $user = new User(User::SCENARIO_SIGNUP);
                            $user->initRegister($registerUserModel);
                            $user->save(false);
                            $message = new Messages();
//                            $message->response = 'please check your mail';
//                            $message->subject = 'registration';
//                            $message->save();
                            $message = Messages::model()->findByAttributes(array('subject'=>'registration'));
                            $mail = new Mail();
                            $mail->debug =  false;
                            $mail->message = $message;
                            $mail->registrationMalling($user,$user->id);
//                            echo CJSON::encode($user->email);
                            $return = array();
                            $return['response'] = $message->response; 
                            echo CJSON::encode($return);
                        }
                        else
                        {
                            $return = array();
                            $return['response'] = 'Пользователь  с таким email уже зарегестрирован'; 
                            echo CJSON::encode($return);
                        }
                        
                    }
                    else 
                    {
                        echo CJSON::encode('error ');
                    }
                }
                else
                {
                    echo CJSON::encode('post not set');
                }
            }    
        }
	public function filters()
	{
		return array(
			'accessControl',
			'postOnly + delete', 
		);
	}
	
	public function accessRules()
	{
		return array(
			array('allow',
				'actions' => array('create'),
				'roles' => array('create_user'),
			),
			array('allow',
				'actions' => array('view', 'index'),
				'roles' => array('view_user'),
			),
			array('allow',
				'actions' => array('update'),
				'roles' => array('update_user'),
			),
			array('allow',
				'actions' => array('delete'),
				'roles' => array('delete_user'),
			),
			array('allow',
				'actions' => array('deleteRegister'),
				'roles' => array('deleteRegister_user'),
			),
			array('allow',
				'actions' => array('signup','profile'),
				'roles' => array('guest'),
			),
			array('deny'),
		);
	}
}
