<?php

class FeedbackStatusController extends AdminController {

    public function actionIndex()
    {
        $model = $this->createSearchModel('FeedbackStatus');
        $provider = $model->search();
        $provider->pagination = array(
            'pageSize' => $this->getStoredParam('feedbackPerPage', 10),
        );
        $this->render('index', array(
            'model' => $model,
            'provider' => $provider,
        ));
    }
//     добавим  тут    обработку  аякс запроса 
    public function actionAddShipment()
    {
        $currentUser =  User::getCurrentUser();
        $mail = new Mail();
//        $newPost = new NewPost();
        $modelShipment = new Shipment();
        $clientModel = new Client();
        if(Yii::app()->request->isAjaxRequest)
        {
            
//            тут  стоит  использовать Mail класс  
            if(isset($_POST['Shipment'])){
//                $newPost->declarationNumber= $_POST['NewPost']['declarationNumber'];
//                $newPost->arrivalDate= $_POST['NewPost']['arrivalDate'];
                $modelShipment->form_id  =     $_POST['Shipment']['form_id'];
                $modelShipment->to_status_id = $_POST['Shipment']['to_status_id'];
                $modelShipment->is_sms  =      $_POST['Shipment']['is_sms'];
                $modelShipment->is_email =     $_POST['Shipment']['is_email'];
                if(isset($_POST['Shipment']['sms_text'])){
                    $modelShipment->sms_text = $_POST['Shipment']['sms_text'];
                    
                }
                if(isset($_POST['Shipment']['email_text'])){
                    $modelShipment->email_text = $_POST['Shipment']['email_text'];

                }
                
                
                if($modelShipment->validate()){
                    $modelShipment->save(false);
//                    $newPost->save(false);
                    $to = $currentUser->email;
                    $message = new Messages();
                    $message->response = 'please check your mail';
                    $message->subject = 'registration';
                    $message->save();
                    $mail->isHtml = true;
                    $mail->debug = true;
                    $mail->message = $message;
                    $mail->from ='alexpetrov-95@mail.ru';
                    $mail->render('application.components.mailBodyShipment');
                    $mail->send($to,'Shipment');
                    $return = array();
                    
                     if($modelShipment->to_status_id==2)
                    {
//                        $password = 'power1990';
//                        $login = 'biglapa';
//                        $sender = 'BigLapa.com'; 
                         
                         
//                        $sms = new SmsCurier($login,$password,$sender);
                        $sms = SmsCurier($currentUser->turbosms_login,$currentUser->turbosms_password,$currentUser->turbosms_sender);
                        $feedback =  $modelShipment->Form->feedback;
//                        $sms->sentDeliveredSms($modelShipment);
                        $clientModel =  $feedback->client;
                        $newPost = $feedback->newPostRecord;
                        $textToSms = SmsCurier::modernText($modelShipment->sms_text,array(
                                    'name'=>$clientModel->name,
                                    'declarationNumber'=>$newPost->declarationNumber,
                                    'arrivalDate'=>$newPost->arrivalDate,
                            ));
                        $sms->auth();
                        $sms->sendSms($textToSms,$clientModel->phone);
                        $return['sms']=$clientModel->phone;
                        $return['phone']=$textToSms;
                        $return['client'] = $clientModel->name ;
                        $return['declarationNumber']= $newPost->declarationNumber;
                        $return['arrivalDate']= $newPost->arrivalDate;
                                
                    }
                    
                    
                    
                    $return['result'] = 'succes';
                    
                    echo CJSON::encode($return);
                }
                else
                { 
                    $return = array();
                    $return['result'] = 'error';
                    foreach ($modelShipment->getErrors() as $attrbute=>$errors)
                    {
//                      $return ['textErorr'] .=$attrbute.'<span>'.$textError.'<br>';
                        foreach ($errors as $error){
                            $return ['textErorr'] .=$attrbute.'<span>'.$error.'<br>';
                        }
                    }
//                    foreach ($newPost->getErrors() as $attrbute=>$errors)
//                    {
////                      $return ['textErorr'] .=$attrbute.'<span>'.$textError.'<br>';
//                        foreach ($errors as $error){
//                            $return['textErorr'] .=$attrbute.'<span>'.$error.'<br>';
//                        }
//                    } 
                    echo CJSON::encode($return);
                }      
            }
        }
    }
    public function actionUpdateShipment(){
        $shipmentModel = new Shipment();
        if(Yii::app()->request->isAjaxRequest)
        {
            if(isset($_POST['Shipment']['id']))
            {
                $shipmentModel=$this->loadModel($_POST['Shipment']['id'],'Shipment');
                $shipmentModel->form_id= $_POST['Shipment']['form_id'];
                $shipmentModel->to_status_id= $_POST['Shipment']['to_status_id'];
                $shipmentModel->is_sms= $_POST['Shipment']['is_sms'];
                $shipmentModel->is_email= $_POST['Shipment']['is_email'];
                if($this->saveModel($shipmentModel))
                {
                    $returnValue = array();
                    $returnValue['result']='success';
                    $returnValue['flag']='success';
                    echo CJSON::encode($returnValue);
                }
            } 
        }
    }
    public function actionAddShipmentPage()
    {
        $listForms = CHtml::listData(Yii::app()->db->createCommand()->select()->from('feedback_form')->queryAll(),'id','id');
        $listStatus = Feedback::getStatusList(); 
        $shipmentModel = new Shipment();
        $this->render('addShipment',array('shipmentModel'=>$shipmentModel,'listStatus'=>$listStatus,'listForms'=>$listForms));
    }
    public  function actionSendSms()
    {
        $password = 'power1990';
        $login = 'biglapa';
        $sender = 'BigLapa.com';         
        $sms = new SmsCurier($login,$password,$sender);

        $this->render('sendsms',array('sms'=>$sms));
    }
    public function actionViewShipment()
    {  
        $modelNewPost = new NewPost();
        $shipmentModel = new Shipment();
        $listForms = CHtml::listData(Yii::app()->db->createCommand()->select()->from('feedback_form')->queryAll(),'id','name');
        $listStatus = Feedback::getStatusList(); 
        $activeDataProvider = new CActiveDataProvider('Shipment',array(
            'pagination'=>array(
                'pageSize'=>10,
            ),
        ));
        $this->render('viewShipment',array('post'=>$modelNewPost,'dataProvider'=>$activeDataProvider,'shipmentModel'=>$shipmentModel,'listStatus'=>$listStatus,'listForms'=>$listForms));
    }
    public function actionCreate()
    {
        $model = new FeedbackStatus('create');
        if ($this->saveModel($model)) {
            $this->redirect(array('index'));
        }
        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'FeedbackStatus');

        if (isset($_POST)){
            if (isset($_POST['field']) && $_POST['field'] == 'name'){
                $model->name = $_POST['value'];
            }
            if (isset($_POST['field']) && $_POST['field'] == 'color'){
                $model->color = $_POST['value'];
            }
            if ($model->save()){
                $this->redirect(array('index'));
            }
        }

        if ($this->saveModel($model)) {
            $this->redirect(array('index'));
        }
        $this->render('update', array(
            'model' => $model,
        ));

    }

    public function actionAjaxUpdate()
    {
        if (!YII_DEBUG && !Yii::app()->request->isAjaxRequest) {
            throw new CHttpException('403', 'Forbidden access.');
        }
        if (isset($_POST)){
            $post_id = Yii::app()->request->getParam('id');
            $post_name = Yii::app()->request->getParam('name');
            $post_color = Yii::app()->request->getParam('color');

            $model = $this->loadModel($post_id, 'FeedbackStatus');

            $model->name = isset($post_name) ? $post_name : $model->name;
            $model->color = isset($post_color) ? $post_color  : $model->color;

            if ($model->save()){
                echo CJSON::encode(array('status'=>'ok'));
            }
        }
    }

    public function actionDelete($id)
    {
        $model = $this->loadModel($id, 'FeedbackStatus');
        $model->delete();
        if(!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
    }

    public function filters()
    {
        return array(
            'accessControl',
            'postOnly + delete',
        );
    }

//    public function accessRules()
//    {
//        return array(
//            array('allow',
//                'actions' => array('index'),
//                'roles' => array('update_feedback_status'),
//            ),
//            array('allow',
//                'actions' => array('update'),
//                'roles' => array('update_feedback_status'),
//            ),
//            array('allow',
//                'actions' => array('delete'),
//                'roles' => array('delete_feedback_status'),
//            ),
//            array('allow',
//                'actions' => array('create'),
//                'roles' => array('create_feedback_status'),
//            ),
//            array('deny'),
//        );
//    }

}