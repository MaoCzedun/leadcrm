<?php

class OptionsController extends AdminController 
{
	public function actionFeedbackStatusOptions()
	{
        $provider = new CActiveDataProvider('State', array(
//            'criteria' => array(
//                'condition'=>'is_default=1',
//            ),
            'pagination' => array(
                'pageSize' => Yii::app()->params['postsPerPage'],
            ),
        ));

		$model = FeedbackStatusOptions::instance();
		if ($this->saveModel($model)) {
			Yii::app()->user->setFlash('message', Yii::t('admin.crud', 'Options have been updated'));
			$this->refresh();
		}
		$this->render('feedbackStatusOptions', array(
			'model' => $model,
            'provider' => $provider
		));
	}
	
	public function actionMailOptions()
	{
		$model = MailOptions::instance();
		if ($this->saveModel($model)) {
			Yii::app()->user->setFlash('message', Yii::t('admin.crud', 'Options have been updated'));
			$this->refresh();
		}
		$this->render('mailOptions', array(
			'model' => $model,
		));
	}
	

	public function filters()
	{
		return array(
			'accessControl',
		);
	}
	
	public function accessRules()
	{
		return array(
			array('allow',
				'actions' => array('feedbackStatusOptions'),
				'roles' => array('update_feedback_status_options'),
			),
			array('allow',
				'actions' => array('mailOptions'),
				'roles' => array('update_mail_options'),
			),
			array('deny'),
		);
	}
}
