<?php

class GoodController extends AdminController {


    public function actionIndex()
    {
        $model = $this->createSearchModel('Good');
        $provider = $model->search();
        $provider->pagination = array(
            'pageSize' => $this->getStoredParam('feedbackPerPage', 10),
        );
        $this->render('index', array(
            'model' => $model,
            'provider' => $provider,
        ));
    }

    public function actionCreate()
    {
        $model = new Good('create');
        $model->user_id= User::getCurrentUser()->id;
        if ($this->saveModel($model)) {
            $this->redirect(array('index'));
        }
        $this->render('create', array(
            'model' => $model,
        ));
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Good');

        if ($this->saveModel($model)) {
            $this->redirect(array('index'));
        }
        $this->render('update', array(
            'model' => $model,
        ));

    }

    public function actionDelete($id)
    {
        $model = $this->loadModel($id, 'Good');
        $model->delete();
        if(!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
    }
    public function actionGetGoodById()
    {
        if(Yii::app()->request->isAjaxRequest)
        {
            if(isset($_POST['Good']['id']))
            {
                $id = $_POST['Good']['id'];
                $currentGood = Good::model()->findByPk($id);
                echo CJSON::encode($currentGood);
				// echo json_encode($currentGood);
            }
        }
    }
    public  function actionGoodSupply()
    {
        $good = new Good();
        $good->setScenario('supply');
        if(isset($_POST['Good']))
        {
            $id = $_POST['Good']['id'];
            $good = Good::model()->findByPk($id);
            $good->attributes = $_POST['Good'];
            if($good->validate())
            {
                $good->update();
            }
        }
        $listGoods = Good::getListGood();
        $this->render('goodSupply',array(
            'listGoods'=>$listGoods,
            'good'=>$good, 
        ));
    }
//     необходимо  знать количество  каждого товара на складе 
    public function actionSaleOfGoods()
    {
        $good = new Good('update');
        $listGoods = Good::getListGood();
        if(isset($_POST['Good']))
        {
            $id = $_POST['Good']['id'];
            $good = $good->findByPk($id);
            $good->attributes = $_POST['Good'];
            if($good->validate())
            {
                $good->amount -= $good->numberSaledGoods;
                $good->update();
            }
        }
        $this->render('saleOfGoods',array(
            'good'=>$good,
            'listGoods'=>$listGoods,
        ));
        
    }

//    стоит   тут добавить  еще некоторые  действия  
    public function filters()
    {
        return array(
            'accessControl',
            'postOnly + delete',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('index'),
                'roles' => array('view_goods'),
            ),
            array('allow',
                'actions' => array('update'),
                'roles' => array('update_goods'),
            ),
            array('allow',
                'actions' => array('delete'),
                'roles' => array('delete_goods'),
            ),
            array('allow',
                'actions' => array('create'),
                'roles' => array('create_goods'),
            ),
            array('allow',
                'actions' => array('saleOfGoods'),
                'roles' => array('sale_Of_Goods'),
            ),
            array('allow',
                'actions' => array('goodSupply'),
                'roles' => array('good_supply'),
            ),
            array('allow',
                'actions' => array('getGoodById'),
                'roles' => array('get_Good_By_Id'),
            ),
            array('deny'),
        );
    }

}