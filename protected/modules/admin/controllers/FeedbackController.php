<?php

class FeedbackController extends AdminController {

    public function actionIndex() {
        $model = $this->createSearchModel('Feedback');
        $model->unsetAttributes();
        $feedbackStatusId = Yii::app()->request->getParam('feedbackStatusId');
        if (isset($feedbackStatusId)) {
            $model->status_id = $feedbackStatusId;
        }
        $provider = $model->search();
        $provider->pagination = array(
            'pageSize' => $this->getStoredParam('feedbackPerPage', 10),
        );

        if (isset($_GET['ajax'])) {
            $this->renderPartial('_grid', array(
                'model' => $model,
                'provider' => $provider,
                'feedbackStatusId' => isset($feedbackStatusId) ? $feedbackStatusId : ''
            ));
        } else {
            $this->render('index', array(
                'model' => $model,
                'provider' => $provider,
                'feedbackStatusId' => isset($feedbackStatusId) ? $feedbackStatusId : ''
            ));
        }
    }

    public function getStatusCssClass($data) {
        return '{$data->status->id}';
    }

    public function actionExport() {
        Yii::import('application.extensions.ExportXLS.ExportXLS');
        $headerColumns = array();
        foreach (Feedback::model()->getAttributes() as $key=>$value){
            $headerColumns[]=$key;
        }
        $filter = new FeedbackExportFilter();
        if (isset($_GET['FeedbackExportFilter'])) {
            $filter->attributes = $_GET['FeedbackExportFilter'];
        }
        $criteria = $filter->createCriteria();
        $filename =  'FeedBacks.xls';
        $xls =  new ExportXLS($filename);
        $xls->addHeader($headerColumns);
        $FeedBacks = Feedback::model()->findAll($criteria);
        foreach ($FeedBacks as $FeedBack){
            $tmp = array_values($FeedBack->attributes);
            $tmp[1]=iconv('UTF-8','windows-1251',$FeedBack->status->name);
            $tmp[2]=iconv('UTF-8','windows-1251',$FeedBack->user->username);
            $tmp[13]=iconv('UTF-8','windows-1251',$FeedBack->client->name);
            $tmp[14]=iconv('UTF-8','windows-1251',$FeedBack->good->name);  
            $xls->addRow($tmp);
        }
        $xls->sendFile();
        $this->render('export');
       
    }

    public function actionCreate() {
        $currentUser  = User::getCurrentUser();
        $client = new Client();
        $novaPoshta = new NovaPoshtaApi2($currentUser->newPostKey);
        $newPostRecord = new NewPost();
        $feedbackModel = new Feedback('create');
        if(isset($_POST['Feedback'],$_POST['Client']))
        {
            $client->attributes=$_POST['Client'];
            $feedbackModel->attributes=$_POST['Feedback'];
             $params =  array(
                      'IntDocNumber'=>$feedbackModel->declarationNumber
                );
            $result = $novaPoshta->getDocumentList($params);
            $newPostRecord->arrivalDate = $result['data'][0]['PreferredDeliveryDate'];
            $result= $novaPoshta->documentsTracking($feedbackModel->declarationNumber);
            $newPostRecord->StateName= $result['data'][0]['StateName'];
            $newPostRecord->declarationNumber= $feedbackModel->declarationNumber;
            if($client->validate() && $feedbackModel->validate()&&$newPostRecord->validate())
            {
                $client->save(false);
                $feedbackModel->client_id = $client->id;
                // TODO: добавить на вьюху выпадающий список форм, с привязкой по товару
                $feedbackModel->form_id = 32;
                $feedbackModel->manager_id = Yii::app()->user->getId();
                $feedbackModel->user_id = Yii::app()->user->getId();
                $feedbackModel->save(false);
                $newPostRecord->feedback_id = $feedbackModel->id;
                $newPostRecord->save(false);
                $ShipmentToStatusId = Shipment::model()->findByAttributes(array('form_id'=>$feedbackModel->form->id,'to_status_id'=>$feedbackModel->status_id));
                if($ShipmentToStatusId)
                {
//                    $password = 'power1990';
//                    $login = 'biglapa';
//                    $sender = 'BigLapa.com';         
                    $sms = new SmsCurier($currentUser->turbosms_login,$currentUser->turbosms_password,$currentUser->turbosms_sender);    
                    $sms->sentDeliveredSms($feedbackModel->form->Shipment,$feedbackModel);
                }
                $this->redirect(array('view', 'id' => $feedbackModel->id));
            }    
        } 
        $this->render('create', array(
            'feedbackModel' => $feedbackModel,
            'client' => $client,
//            'newPostRecord'=>$newPostRecord,
        ));
    }

    public function actionUpdate($id) {
        $currentUser  = User::getCurrentUser();
        $novaPoshta = new NovaPoshtaApi2($currentUser->newPostKey);
        $newPostRecord = NewPost::model()->findByAttributes(array('feedback_id'=>$id));
        $feedBack = new Feedback('update');
        $currentUserId = $currentUser->id;
        $feedBack = Feedback::model()->with('client')->findByPk($id);    
        
        $oldStatus = $feedBack->status_id; 
        $oldCreateTime = $feedBack->create_time;
        $oldCreateTimeClient = $feedBack->client->create_time;
        $debugBag = $feedBack;
        
        if (isset($_POST['Feedback'], $_POST['Client'])) {
            $feedBack->attributes = $_POST['Feedback'];
            $feedBack->client->attributes = $_POST['Client'];
            $feedBack->create_time = $oldCreateTime;
            $feedBack->client->create_time = $oldCreateTimeClient;
            $feedBack->user_id = $currentUserId;
            $feedBack->manager_id = $currentUserId;
            $statusId = $_POST['Feedback']['status_id'];
            $goodId = $_POST['Feedback']['good_id'];
            $feedBack->status_id = $statusId;
            $feedBack->good_id = $goodId;
            $params =  array(
                      'IntDocNumber'=>$feedBack->declarationNumber
                );
            $result = $novaPoshta->getDocumentList($params);
            $newPostRecord->arrivalDate = $result['data'][0]['PreferredDeliveryDate'];
            $result= $novaPoshta->documentsTracking($feedBack->declarationNumber);
            $newPostRecord->StateName= $result['data'][0]['StateName'];
            $newPostRecord->declarationNumber= $feedBack->declarationNumber;
            if ($feedBack->validate() && $feedBack->client->validate() && $newPostRecord->validate()) {
                
                $feedBack->update();
                $feedBack->client->update();
                $newStatus = $feedBack->status_id;
                $newPostRecord->feedback_id = $feedBack->id;
                $newPostRecord->update();
                if($newStatus!==$oldStatus){
                    if($feedBack->status->Shipment->is_sms) {
//                        $password = 'power1990';
//                        $login = 'biglapa';
//                           $sms = new SmsCurier($login,$password,'BigLapa.com'); 
                        $sms =  new SmsCurier($currentUser->turbosms_login,$currentUser->turbosms_password,$currentUser->turbosms_sender);
                        $sms->auth();
                        $sms->sendSms('all for you',$feedBack->client->phone);
                    }
                    
                    if($feedBack->status->Shipment->is_email) {
                        $message = new Messages();
                        $message->response = 'please check your mail';
                        $message->subject = 'UpdateFeedback';
                        $message->save();
                        $mail = new Mail();
                        $mail->message = $message;
                        $to = User::getCurrentUser()->email;
                        $mail->isHtml = true;
                        $mail->debug = true;
                        $mail->render('application.components.mailBodyShipment');
                        $mail->send($to,'UpdateFeedback');
                    }
                }
               
                $return = array();                
                $return['charset'] = SmsCurier::getCharset($feedBack->form->Shipment->sms_text);
                echo CJSON::encode($return);
                $ShipmentToStatusId = Shipment::model()->findByAttributes(array('form_id'=>$feedBack->form->id,'to_status_id'=>$feedBack->status_id));
                if($ShipmentToStatusId)
                {
//                    $password = 'power1990';
//                    $login = 'biglapa';
//                    $sender = 'BigLapa.com';     
//                    $sms = new SmsCurier($login,$password,$sender);    
                    $sms =  new SmsCurier($currentUser->turbosms_login,$currentUser->turbosms_password,$currentUser->turbosms_sender);
                    $sms->sentDeliveredSms($feedBack->form->Shipment,$feedBack);
                }       
            }
        }
        $this->render('update', array(
            'feedBack' => $feedBack,
            'client' => $feedBack->client,
        ));
    }

    public function actionDelete($id) {
        $model = $this->loadModel($id, 'Feedback');
        $model->delete();
        if (!isset($_GET['ajax'])) {
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
        }
    }

    public function actionCreateFromForm() {
        header('Access-Control-Allow-Origin: *');
        $rsa = new Crypt_RSA();
        $feedbackModel = new Feedback();
        $binaryDecrypted = $this->decrypt($_POST['encryptedText']);
        $strPosPhone = strpos($binaryDecrypted,'phone');
//        $strPosAddress; 
//        $strPosEmail;
        $strPosName = strpos($binaryDecrypted,'name');
        $binaryDecrypted = $this->decrypt($_POST['encryptedText']);
        if(isset($_POST['encryptedText']))
        {
            $clientPhone = substr($binaryDecrypted,$strPosPhone+5);
            $lenght = (strpos($binaryDecrypted,' ',$strPosName)-5);
            $clientName = substr($binaryDecrypted,$strPosName+4,$lenght);
            if (strlen($clientPhone)>0) {
                $client = Client::model()->findByAttributes(array('phone' => $clientPhone));
            }
            if (!$client) {
                $client = new Client('insert');
                if (isset($_POST['fieldAddres'])) {
                    $client->city = $_POST['fieldAddres'];
                }
                if (isset($_POST['fieldEmail'])) {
                    $client->email = $_POST['fieldEmail'];
                }
                if (isset($_POST['fieldName'])) {
                    $client->name = $_POST['fieldName'];
                }
                if (isset($_POST['fieldPhone'])) {
                    $client->phone = $_POST['fieldPhone'];
                }
                $client->user_id = User::getCurrentUser()->id;
                if ($client->save()) {
                    $returnArray['success'] = 'success';
                }
                else {
                    $returnArray['error'] =  $client->getErrors() ;
                }
            } 
            else {
                if (isset($_POST['fieldAddres']) && (empty($client->city))) {
                    $client->city = $_POST['fieldAddres'];
                }
                if (isset($_POST['fieldEmail']) && (empty($client->email))) {
                    $client->email = $_POST['fieldEmail'];
                }
                if (strlen($clientName)>0) {
                    $client->name = $clientName;
                }
                $client->save() ? $returnArray['success'] = true : $returnArray['error'] = $client->getErrors() ; 
            }
            $defaultNewFeedbackStatus = 1;
            $feedbackModel->user_id = User::getCurrentUser()->id;
            $feedbackModel->status_id = $defaultNewFeedbackStatus;
            $feedbackModel->client_id = $client->id; 
            if (isset($_POST['good'])) {
                $feedbackModel->good = $_POST['good'];
            }
            $feedbackModel->save();
//            $rsa->loadKey('-----BEGIN RSA PUBLIC KEY-----'.$_POST['publicKey'].'-----BEGIN RSA PUBLIC KEY-----');
        
//            $binaryDecrypted=$this->decrypt("JDlK7L/nGodDJodhCj4uMw0/LW329HhO2EvxNXNUuhe+C/PFcJBE7Gp5GWZ835fNekJDbotsUFpLvP187AFAcNEfP7VAH1xLhhlB2a9Uj/z4Hulr4E2EPs6XgvmLBS3MwiHALX2fES5hSKY/sfSUssRH10nBHHO9wBLHw5mRaeg=");
            echo $binaryDecrypted;     
            echo $clientName;
            echo $clientPhone;
//            echo $lenght;
//            echo strpos($binaryDecrypted,' ',$strPosName);
        }
    }

    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id, 'Feedback'),
        ));
    }

    public function filters() {
        return array(
            'accessControl',
            'postOnly + delete',
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('create'),
                'roles' => array('create_feedback'),
            ),
            array('allow',
                'actions' => array('export'),
                'roles' => array('export_feedbacks'),
            ),
            array('allow',
                'actions' => array('CreateFromForm'),
                'roles' => array('get_ExternalData'),
            ),
            array('allow',
                'actions' => array('view', 'index', 'export'),
                'roles' => array('view_feedback'),
            ),
            array('allow',
                'actions' => array('update'),
                'roles' => array('update_feedback'),
            ),
            array('allow',
                'actions' => array('delete'),
                'roles' => array('delete_feedback'),
            ),
            array('deny'),
        );
    }
    private  function decrypt($strBase64CipherText)
    {
//        //CRYPT_RSA_MODE_INTERNAL is slow
//        //CRYPT_RSA_MODE_OPENSSL is fast, but requires openssl to be installed, configured and accessible.
//        define("CRYPT_RSA_MODE", CRYPT_RSA_MODE_INTERNAL);
//        $rsa=new Crypt_RSA();
//        //$strPrivateKey=file_get_contents("private.pem");
//        //This private key is for example purposes
//        //DO NOT REUSE
//        $strPrivateKey="-----BEGIN RSA PRIVATE KEY-----
//MIICXQIBAAKBgQDVd/gb2ORdLI7nTRHJR8C5EHs4RkRBcQuQdHkZ6eq0xnV2f0hk
//WC8h0mYH/bmelb5ribwulMwzFkuktXoufqzoft6Q6jLQRnkNJGRP6yA4bXqXfKYj
//1yeMusIPyIb3CTJT/gfZ40oli6szwu4DoFs66IZpJLv4qxU9hqu6NtJ+8QIDAQAB
//AoGADbnXFENP+8W/spO8Dws0EzJCGg46mVKhgbpbhxUJaHJSXzoz92/MKAqVUPI5
//mz7ZraR/mycqMia+2mpo3tB6YaKiOpjf9J6j+VGGO5sfRY/5VNGVEQ+JLiV0pUmM
//doq8n2ZhKdSd5hZ4ulb4MFygzV4bmH29aIMvogMqx2Gkp3kCQQDx0UvBoNByr5hO
//Rl0WmDiDMdWa9IkKD+EkUItR1XjpsfEQcwXet/3QlAqYf+FE/LBcnA79NdBGxoyJ
//XS+O/p4rAkEA4f0JMSnIgjl7Tm3TpNmbHb7tsAHggWIrPstCuHCbNclmROfMvcDE
//r560i1rbOtuvq5F/3BQs+QOnOIz1jLslUwJAbyEGNZfX87yqu94uTYHrBq/SQIH8
//sHkXuH6jaBo4lP1HkY2qtu3LYR2HuQmb1v5hdk3pvYgLjVsVntMKVibBPQJBAKd2
//Dj20LLTzS4BOuirKZbuhJBjtCyRVTp51mLd8Gke9Ol+NNZbXJejNvhQV+6ad7ItC
//gnDfMoRERMIPElZ6x6kCQQCP45DVojZduLRuhJtzBkQXJ4pCsGC8mrHXF3M+hJV+
//+LAYJbXrQa4mre59wR0skgb6CwGg1siMrDzJgu3lmBB0
//-----END RSA PRIVATE KEY-----";
//        $strPrivateKey=preg_replace("/[ \t]/", "", $strPrivateKey);//this won't be necessary when loading from PEM
//
//
//        $rsa->loadKey($strPrivateKey);
//
//        $binaryCiphertext=base64_decode($strBase64CipherText);
//
//        $rsa->setEncryptionMode(CRYPT_RSA_ENCRYPTION_PKCS1);
//        $strBase64DecryptedData=$rsa->decrypt($binaryCiphertext);
//
//        return base64_decode($strBase64DecryptedData);\
        
    //Load private key:
    $private = "-----BEGIN RSA PRIVATE KEY-----
    MIICXAIBAAKBgQDfmlc2EgrdhvakQApmLCDOgP0nNERInBheMh7J/r5aU8PUAIpG
    XET/8+kOGI1dSYjoux80AuHvkWp1EeHfMwC/SZ9t6rF4sYqV5Lj9t32ELbh2VNbE
    /7QEVZnXRi5GdhozBZtS1gJHM2/Q+iToyh5dfTaAU8bTnLEPMNC1h3qcUQIDAQAB
    AoGAcbh6UFqewgnpGKIlZ89bpAsANVckv1T8I7QT6qGvyBrABut7Z8t3oEE5r1yX
    UPGcOtkoRniM1h276ex9VtoGr09sUn7duoLiEsp8aip7p7SB3X6XXWJ9K733co6C
    dpXotfO0zMnv8l3O9h4pHrrBkmWDBEKbUeuE9Zz7uy6mFAECQQDygylLjzX+2rvm
    FYd5ejSaLEeK17AiuT29LNPRHWLu6a0zl923299FCyHLasFgbeuLRCW0LMCs2SKE
    Y+cIWMSRAkEA7AnzWjby8j8efjvUwIWh/L5YJyWlSgYKlR0zdgKxxUy9+i1MGRkn
    m81NLYza4JLvb8/qjUtvw92Zcppxb7E7wQJAIuQWC+X12c30nLzaOfMIIGpgfKxd
    jhFivZX2f66frkn2fmbKIorCy7c3TIH2gn4uFmJenlaV/ghbe/q3oa7L0QJAFP19
    ipRAXpKGX6tqbAR2N0emBzUt0btfzYrfPKtYq7b7XfgRQFogT5aeOmLARCBM8qCG
    tzHyKnTWZH6ff9M/AQJBAIToUPachXPhDyOpDBcBliRNsowZcw4Yln8CnLqgS9H5
    Ya8iBJilFm2UlcXfpUOk9bhBTbgFp+Bv6BZ2Alag7pY=
    -----END RSA PRIVATE KEY-----";
    $privateKey = openssl_pkey_get_private($private);
    if (!$privateKey) {die('Loading Private Key failed');}
    //Decrypt
    $decrypted_text = "";
    if (!openssl_private_decrypt(base64_decode($_POST['password']), $decrypted_text, $privateKey)) die('Failed to decrypt data');

    //Decrypted :) 

    //Free key
    openssl_free_key($privateKey);
     return  $decrypted_text;
    }

}
