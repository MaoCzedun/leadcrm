<?php
class UserPageController extends TestController
{
  public function actionIndex()
  {
      $this->render('index');
  }
  public  function actionTestExport()
  {
        header('Content-Type: application/vnd.ms-excel;charset=utf-8');
        Yii::import('application.extensions.ExportXLS.ExportXLS');

        $headerColumns = array();
        foreach (Feedback::model()->getAttributes() as $key=>$value){
            $headerColumns[]=$key;
        }        
        $filename =  'test.xls';
        $xls =  new ExportXLS($filename);
        $xls->addHeader($headerColumns);
        $FeedBacks = Feedback::model()->findAll();
        
        foreach ($FeedBacks as $FeedBack){
            $tmp = array_values($FeedBack->attributes);
            $tmp[1]=iconv('UTF-8','windows-1251',$FeedBack->status->name);
            $tmp[2]=iconv('UTF-8','windows-1251',$FeedBack->user->username);
            $tmp[13]=iconv('UTF-8','windows-1251',$FeedBack->client->name);
            $tmp[14]=iconv('UTF-8','windows-1251',$FeedBack->good->name);
            $xls->addRow($tmp);
        }
        $xls->sendFile();
        $this->render('testExportXLS',array(
            'provider'=>$FeedBacks,
            'data'=>$headerColumns,
          
      ));
  }
}

