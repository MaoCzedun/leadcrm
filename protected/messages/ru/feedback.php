<?php

return array(
	'City' => 'Город',
	'Comment' => 'Комментарии',
	'Course' => 'Курс',
	'Time Created' => 'Дата создания',
	'Email' => 'Email',
	'Manager' => 'Менеджер',
	'Name' => 'ФИО',
	'Phone Number' => 'Номер телефона',
	'Status' => 'Статус',
	'Time Updated' => 'Дата обновления',
);
