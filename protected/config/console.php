<?php

return array(
	'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
	'name' => 'MESI',
	'preload'=>array('log'),
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),
	'components'=>array(
		'db'=>array(
			'class' => 'CDbConnection',
			'connectionString' => 'mysql:host=localhost;dbname=onlinemesi',
			'username' => 'root',
			'password' => 'sax',
			'tablePrefix' => '',
			'charset' => 'utf8',
		),
	),
);
