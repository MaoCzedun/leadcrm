<?php

return array(
	/**
	 * User
	 */
	'view_user' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'View users',
		'bizRule' => null,
		'data' => null
	),
	'create_user' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'Create users',
		'bizRule' => null,
		'data' => null
	),
	'update_user' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'Update users',
		'bizRule' => null,
		'data' => null
	),
	'delete_user' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'Delete users',
		'bizRule' => null,
		'data' => null
	),
	'signup_user' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'register user',
		'bizRule' => null,
		'data' => null
	),
	'profile_user' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'register user',
		'bizRule' => null,
		'data' => null
	),
	'deleteRegister_user' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'register user',
		'bizRule' => null,
		'data' => null
	),
        
	/**
	 * Course 
	 */
	'view_course' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'View course',
		'bizRule' => null,
		'data' => null
	),
	'create_course' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'Create course',
		'bizRule' => null,
		'data' => null
	),
	'update_course' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'Edit course',
		'bizRule' => null,
		'data' => null
	),
	'delete_course' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'Delete course',
		'bizRule' => null,
		'data' => null
	),
	/**
	 * Feedback 
	 */
	'view_feedback' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'View feedback',
		'bizRule' => null,
		'data' => null
	),
	'create_feedback' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'Create feedback',
		'bizRule' => null,
		'data' => null
	),
	'update_feedback' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'Edit feedback',
		'bizRule' => null,
		'data' => null
	),
	'delete_feedback' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'Delete feedback',
		'bizRule' => null,
		'data' => null
	),
	'get_ExternalData' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'Get  External Data',
		'bizRule' => null,
		'data' => null
	),
	'export_feedbacks' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'Get  External Data',
		'bizRule' => null,
		'data' => null
	),
        
        
	/**
	 * FeedbackStatusOptions 
	 */
	'update_feedback_status_options' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'Edit feedback status options',
		'bizRule' => null,
		'data' => null
	),
        /**
	 * Good
	 */
        'view_goods' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'View goods',
		'bizRule' => null,
		'data' => null
	),
	'create_goods' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'Create goods',
		'bizRule' => null,
		'data' => null
	),
	'update_goods' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'Edit goods',
		'bizRule' => null,
		'data' => null
	),
	'delete_goods' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'Delete good',
		'bizRule' => null,
		'data' => null
	),
        'sale_Of_Goods'=>array(
                'type'=>  CAuthItem::TYPE_OPERATION,
                'description'=>'sales  goods',
                'bizrule'=>null,
                'data'=>null,
        ),
       'good_supply'=>array(
                'type'=>  CAuthItem::TYPE_OPERATION,
                'description'=>'good  supply',
                'bizrule'=>null,
                'data'=>null,
       ),
        'get_Good_By_Id'=>array(
                'type'=>  CAuthItem::TYPE_OPERATION,
                'description'=>'get  Good By Id ',
                'bizrule'=>null,
                'data'=>null,
        ),
    
    
	/**
	 * File
	 */
	'view_file' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'View files',
		'bizRule' => null,
		'data' => null
	),
	'create_file' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'Upload files',
		'bizRule' => null,
		'data' => null
	),
	'update_file' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'Edit files',
		'bizRule' => null,
		'data' => null
	),
	'delete_file' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'Delete files',
		'bizRule' => null,
		'data' => null
	),
	/**
	 * FileCategory
	 */
	'view_file_category' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'View file caegories',
		'bizRule' => null,
		'data' => null
	),
	'create_file_category' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'Create file caegories',
		'bizRule' => null,
		'data' => null
	),
	'update_file_category' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'Edit file caegories',
		'bizRule' => null,
		'data' => null
	),
	'delete_file_category' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'Delete file caegories',
		'bizRule' => null,
		'data' => null
	),
	/**
	 * MailOptions 
	 */
	'update_mail_options' => array(
		'type' => CAuthItem::TYPE_OPERATION,
		'description' => 'Edit mail options',
		'bizRule' => null,
		'data' => null
	),
	/**
	 * Basic roles
	 */
	'guest' => array(
		'type' => CAuthItem::TYPE_ROLE,
		'description' => 'Гость',
		'bizRule' => null,
		'data' => null,
                'children'=>array(
                    'signup_user',
                    'deleteRegister_user',
                    'get_ExternalData',
                ),
	),
	'user' => array(
		'type' => CAuthItem::TYPE_ROLE,
		'description' => 'Пользователь',
		'children' => array(
			'guest',
                    'profile_user',
                    'export_feedbacks',
		),
		'bizRule' => null,
		'data' => null,
	),
	'manager' => array(
		'type' => CAuthItem::TYPE_ROLE,
		'description' => 'Менеджер',
		'children' => array(
			//'view_course',
			//'create_course',
			//'update_course',
			//'delete_course',
			'view_feedback',
			'create_feedback',
			'update_feedback',
			'user',
                        'view_goods',
                        'create_goods',
                        'update_goods',
                        'delete_goods',
                        'good_supply',
                        'get_Good_By_Id',
                        'sale_Of_Goods',
		),
		'bizRule' => null,
		'data' => null
	),
	'admin' => array(
		'type' => CAuthItem::TYPE_ROLE,
		'description' => 'Администратор',
		'children' => array(
			'view_user',
			'create_user',
			'update_user',
			'delete_user',
			'view_file',
			'create_file',
			'update_file',
			'delete_file',
			'view_file_category',
			'create_file_category',
			'update_file_category',
			'delete_file_category',
                             
			'delete_feedback',
			'update_mail_options',
			'update_feedback_status_options',
			'manager',
		),
		'bizRule' => null,
		'data' => null
	),
);
