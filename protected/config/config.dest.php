<?php

return array (
    'preload' => array('log'),
    'components' => array(
        'urlManager' => array(
            'urlFormat' => 'path',
            'appendParams' => false,
            'showScriptName' => false,
            'rules' => array(
                '/<v:(aa|akc|car1|car2|car3)>' => '/site/index',
                '/' => '/site/index',
            ),
        ),
        'authManager' => array(
            'class' => 'PhpAuthManager',
            'defaultRoles' => array('guest'),
        ),
        'user' => array(
            'class' => 'WebUser',
            'loginUrl' => '/index.php/admin/login',
            'allowAutoLogin' => true,
        ),
        'db' => array(
            'class' => 'CDbConnection',
            'connectionString' => 'mysql:host=localhost;dbname=mesigr',
            'username' => 'root',
            'password' => '321632',
            'tablePrefix' => '',
            'charset' => 'utf8',
        ),
        'cache' => array(
            'class' => 'CFileCache',
        ),
        'errorHandler'=>array(
            'errorAction'=>'site/error',
        ),
        'format' => array(
            'class' => 'Formatter',
            'booleanFormat' => array('Нет', 'Да'),
            'datetimeFormat' => 'd/m/Y h:i',
        ),
    ),
    'modules' => array(
        'admin',
    ),
    'import' => array(
        'application.components.*',
        'application.models.*',
        'application.extensions.*',
    ),
    'layout' => 'default',
    'name' => 'Lead-CRM',
    'language' => 'ru',
);
