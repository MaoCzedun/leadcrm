<?php

class SiteController extends Controller
{
	public $success_page = false;
	
	public function actionRequest($short='no')
	{
		Yii::import('ext.devdetect.Mobile_Detect');
		$dev_detect = new Mobile_Detect();
		
		$model = new Feedback($short == 'yes' ? 'create-short' : 'create');

		if (isset($_POST['Feedback'])) {
			$model->attributes = $_POST['Feedback'];
			$model->setAttributes(TrafficDetect::get(false), false);
			$model->mobile_device = $dev_detect->isMobile() ? 1 : 0;
			$isAjax = Yii::app()->request->isAjaxRequest;
			if ($model->save()) {
				$redirect = empty($model->variant) ? '/success_osn' : sprintf('/%s/success_%s', $model->variant, $model->variant);
				if ($isAjax) {
					echo CJSON::encode(array(
						'status' => 'ok',
						'request_id' => $model->id,
						'redirect' => $redirect,
					));
					Yii::app()->end();
				} else {
					$this->redirect($redirect);
				}
			} elseif ($isAjax) {
				echo CJSON::encode(array(
					'status' => 'fail',
					'message' => CHtml::errorSummary($model),
				));
				Yii::app()->end();
			}
		}
		
		$this->redirect(Yii::app()->homeUrl);
	}
	
	public function actionComplete($id)
	{
		$model = Feedback::model()->findByPk($id);
		$model->setScenario('complete');

		if (isset($_POST['Feedback'])) {
			$model->attributes = $_POST['Feedback'];
			$isAjax = Yii::app()->request->isAjaxRequest;
			if ($model->save()) {
				$redirect = empty($model->variant) ? '/success_osn' : sprintf('/%s/success_%s', $model->variant, $model->variant);
				if ($isAjax) {
					echo CJSON::encode(array(
						'status' => 'ok',
						'redirect' => $redirect,
					));
					Yii::app()->end();
				}
			} elseif ($isAjax) {
				echo CJSON::encode(array(
					'status' => 'fail',
					'message' => CHtml::errorSummary($model),
				));
				Yii::app()->end();
			}
		}
		
		$this->renderPartial('complete', array(
			'model' => $model,
		));
	}
	
	public function actionConfirm($code)
	{
		$model = Feedback::model()->find('confirmation_code = ?', array($code));
		
		if (null !== $model) {
			$model->updateByPk($model->id, array(
				'confirmed' => 1,
			));
			$result = true;
		} else {
			$result = false;
		}
		
		$this->render('thanks', array(
			'result' => $result,
		));
	}
        
        public function actionError()
        {
            $error = Yii::app()->errorHandler->error;
//            VarDumper::dump($error);
//            echo '<script type="text/javascript">alert("' . $error . '");</script>';
//            $this->redirect(Yii::app()->homeUrl);
              $this->render('error'.$error['code']);
            
        }
        public function actionIndex($v='')
	{
                $userModel = new User();
		if ('car1' == $v || 'akc' == $v) {
			$this->layout = '//layouts/main-alt';
			$this->render('index-alt', array('var' => $v,'userModel'=>$userModel));
		} elseif ('car3' == $v) {
                        $this->layout = '//layouts/main-alt2';
                        $this->render('index-alt2', array('var' => $v,'userModel'=>$userModel));
                } else {
			$this->render('index', array('var' => $v,'userModel'=>$userModel,));
		}
	}
	
	public function actionSuccess($v='',$s='')
	{
		$this->success_page = true;
		
		if ('car1' == $v || 'akc' == $v) {
			$this->layout = '//layouts/main-alt';
			$this->render('index-alt', array(
				'var' => $v,
				'success' => 1,
			));
		} elseif ('car3' == $v) {
            $this->layout = '//layouts/main-alt2';
            $this->render('index-alt2', array(
                'var' => $v,
                'success' => 1,
            ));
        } else {
			$this->render('index', array(
				'var' => $v,
				'success' => 1,
			));
		}
	}
}
