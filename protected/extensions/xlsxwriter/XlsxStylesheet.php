<?php

class XlsxStylesheet
{
	protected $formats;
	protected $fonts;
	protected $fills;
	protected $borders;
	protected $styles;
	public $defaultStyle;
	
	public function __construct()
	{
		$this->init();
	}
	
	public function init()
	{
		$style = new XlsxStyle();
		$style->setFormat($this->addFormat(new XlsxStyleFormat('GENERAL', 164)));
		$style->setFont($this->addFont(new XlsxStyleFont('Arial')));
		$style->setFill($this->addFill(new XlsxStyleFill()));
		$style->setBorder($this->addBorder(new XlsxStyleBorder()));
		$this->defaultStyle = $this->addStyle($style);
	}
	
	public function addFormat(XlsxStyleFormat $value)
	{
		$this->formats[] = $value;
		return $value->code;
	}
	
	public function addFont(XlsxStyleFont $value)
	{
		$index = count($this->fonts);
		$this->fonts[] = $value;
		return $index;
	}
	
	public function addFill(XlsxStyleFill $value)
	{
		$index = count($this->fills);
		$this->fills[] = $value;
		return $index;
	}
	
	public function addBorder(XlsxStyleBorder $value)
	{
		$index = count($this->borders);
		$this->borders[] = $value;
		return $index;
	}
	
	public function addStyle(XlsxStyle $value)
	{
		$index = count($this->styles);
		$this->styles[] = $value;
		return $index;
	}
	
	public function toXml()
	{
		$xml  = XlsxWriter::startXmlTag('styleSheet', array(
			'xmlns' => 'http://schemas.openxmlformats.org/spreadsheetml/2006/main',
		));
		
		if (count($this->formats)) {
			$xml .= XlsxWriter::startXmlTag('numFmts', array(
				'count' => count($this->formats),
			));
			foreach ($this->formats as $format) {
				$xml .= $format->toXml();
			}
			$xml .= XlsxWriter::endXmlTag('numFmts');
		}
		
		if (count($this->fonts)) {
			$xml .= XlsxWriter::startXmlTag('fonts', array(
				'count' => count($this->fonts),
			));
			foreach ($this->fonts as $font) {
				$xml .= $font->toXml();
			}
			$xml .= XlsxWriter::endXmlTag('fonts');
		}
		
		if (count($this->fills)) {
			$xml .= XlsxWriter::startXmlTag('fills', array(
				'count' => count($this->fills),
			));
			foreach ($this->fills as $fill) {
				$xml .= $fill->toXml();
			}
			$xml .= XlsxWriter::endXmlTag('fills');
		}
		
		if (count($this->borders)) {
			$xml .= XlsxWriter::startXmlTag('borders', array(
				'count' => count($this->borders),
			));
			foreach ($this->borders as $border) {
				$xml .= $border->toXml();
			}
			$xml .= XlsxWriter::endXmlTag('borders');
		}
		
		// TODO rewrite
		$xml .= XlsxWriter::startXmlTag('cellStyleXfs', array(
			'count' => 1,
		));
		$xml .= XlsxWriter::startXmlTag('xf', array(
			'applyAlignment' => 'true',
			'applyBorder' => 'true',
			'applyFont' => 'true',
			'applyProtection' => 'true',
			'borderId' => 0,
			'fillId' => 0,
			'fontId' => 0,
			'numFmtId' => 164,
		));
		$xml .= XlsxWriter::xmlTag('alignment', array(
			'horizontal' => 'general',
			'indent' => 0,
			'shrinkToFit' => 'false',
			'textRotation' => 0,
			'vertical' => 'bottom',
			'wrapText' => 'false',
		));
		$xml .= XlsxWriter::xmlTag('protection', array(
			'hidden' => 'false',
			'locked' => 'false',
		));
		$xml .= XlsxWriter::endXmlTag('xf');
		$xml .= XlsxWriter::endXmlTag('cellStyleXfs');
		// -------------
		
		if (count($this->styles)) {
			$xml .= XlsxWriter::startXmlTag('cellXfs', array(
				'count' => count($this->styles),
			));
			foreach ($this->styles as $style) {
				$xml .= $style->toXml();
			}
			$xml .= XlsxWriter::endXmlTag('cellXfs');
		}
		
		$xml .= XlsxWriter::endXmlTag('styleSheet');
		return $xml;
	}
}
