<?php

class XlsxStrings
{
	protected $items = array();
	protected $count = 0;
	protected $uniqueCount = 0;

	public function toXml()
	{
		$xml  = XlsxWriter::startXmlTag('sst', array(
			'xmlns' => 'http://schemas.openxmlformats.org/spreadsheetml/2006/main',
			'count' => $this->count,
			'uniqueCount' => $this->uniqueCount,
		));
		foreach ($this->items as $str) {
			$xml .= XlsxWriter::startXmlTag('si');
			$xml .= XlsxWriter::xmlTag('t', null, $str);
			$xml .= XlsxWriter::endXmlTag('si');
		}
		$xml .= XlsxWriter::endXmlTag('sst');
		return $xml;
	}
	
	public function getStringIndex($string)
	{
		$this->count++;
		$i = array_search($string, $this->items);
		if ($i !== false) {
			return $i;
		}
		$this->items[] = $string;
		$i = $this->uniqueCount;
		$this->uniqueCount++;
		return $i;
	}
}
