<?php

class XlsxSheet
{
	public $name;
	protected $rows;
	protected $row;
	protected $r = 0;
	protected $c = 0;
	protected $writer;
	protected $maxc = 1;
	
	public function __construct($name, XlsxWriter $writer)
	{
		$this->name = $name;
		$this->writer = $writer;
	}
	
	public function addRow()
	{
		$row = new XlsxSheetRow();
		$this->rows[] = $row;
		$this->row = $row;
		$this->r++;
		$this->c = 0;
		$row->r = $this->r;
	}
	
	public function jump($r, $c)
	{
		$this->row = $this->rows[$r];
		$this->r = $r + 1;
		$this->c = $c;
	}
	
	public function addCell($value, $style=null, $isNumber=false)
	{
		$r = $this->getIndexLetter($this->c) . $this->r;
		if ($style === null) {
			$style = $this->writer->getStylesheet()->defaultStyle;
		}
		if ($isNumber) {
			$this->row->addCol(new XlsxSheetCol($value, $r, $style, 'n'));
		} else {
			$value = $this->writer->getStrings()->getStringIndex($value);
			$this->row->addCol(new XlsxSheetCol($value, $r, $style, 's'));
		}
		$this->c++;
	}
	
	protected function getIndexLetter($v)
	{
		$al = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$res = substr($al, $v % 26, 1);
		$v = intval($v / 26);
		while ($v > 0) {
			$res = substr($al, ($v - 1) % 26, 1) . $res;
			$v = intval($v / 26);
		}
		return $res;
	}
	
	public function toXml()
	{
		$xml  = XlsxWriter::startXmlTag('worksheet', array(
			'xmlns' => 'http://schemas.openxmlformats.org/spreadsheetml/2006/main',
			'xmlns:r' => 'http://schemas.openxmlformats.org/officeDocument/2006/relationships',
		));
		$xml .= XlsxWriter::startXmlTag('sheetPr', array(
			'filterMode' => 'false',
		));
		$xml .= XlsxWriter::xmlTag('pageSetUpPr', array(
			'fitToPage' => 'false',
		));
		$xml .= XlsxWriter::endXmlTag('sheetPr');
		
		// TODO rewrite
		$xml .= XlsxWriter::startXmlTag('cols');
		$xml .= XlsxWriter::xmlTag('col', array(
			'collapsed' => 'false',
			'hidden' => 'false',
			'max' => 1025,
			'min' => 1,
			'style' => '0',
			'width' => 12,
		));
		$xml .= XlsxWriter::endXmlTag('cols');
		// ----------
		
		$xml .= XlsxWriter::startXmlTag('sheetData');
		foreach ($this->rows as $row) {
			$xml .= $row->toXml();
		}
		$xml .= XlsxWriter::endXmlTag('sheetData');
		
		$xml .= XlsxWriter::endXmlTag('worksheet');
		return $xml;
	}
}
