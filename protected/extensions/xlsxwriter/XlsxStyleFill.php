<?php

class XlsxStyleFill
{
	public $patternType = 'none';
	public $fgColor;
	public $bgColor;
	
	public function toXml()
	{
		if ($this->fgColor || $this->bgColor) {
			$xml  = XlsxWriter::startXmlTag('patternFill', array(
				'patternType' => $this->patternType,
			));
			if ($this->fgColor) {
				$xml .= XlsxWriter::xmlTag('fgColor', array(
					'rgb' => $this->fgColor,
				));
			}
			if ($this->bgColor) {
				$xml .= XlsxWriter::xmlTag('bgColor', array(
					'rgb' => $this->bgColor,
				));
			}
			$xml .= XlsxWriter::endXmlTag('patternFill');
			return $xml;
		} else {
			return XlsxWriter::xmlTag('patternFill', array(
				'patternType' => $this->patternType,
			));
		}
	}
}
