<?php

class XlsxSheetRow
{
	public $collapsed = false;
	public $customFormat = false;
	public $customHeight = false;
	public $hidden = false;
	public $ht = 12.8;
	public $outlineLevel = 0;
	public $r = 1;
	
	protected $cols;
	
	public function addCol(XlsxSheetCol $col)
	{
		$this->cols[] = $col;
	}
	
	public function toXml()
	{
		$xml  = XlsxWriter::startXmlTag('row', array(
			'collapsed' => $this->collapsed ? 'true' : 'false',
			'customFormat' => $this->customFormat ? 'true' : 'false',
			'customHeight' => $this->customHeight ? 'true' : 'false',
			'hidden' => $this->hidden ? 'true' : 'false',
			'ht' => $this->ht,
			'outlineLevel' => $this->outlineLevel,
			'r' => $this->r,
		));
		foreach ($this->cols as $col) {
			$xml .= $col->toXml();
		}
		$xml .= XlsxWriter::endXmlTag('row');
		return $xml;
	}
}
