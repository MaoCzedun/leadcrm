<?php

class XlsxStyleFont
{
	public $name;
	public $family = 0;
	public $size = 10;
	
	public function __construct($name)
	{
		$this->name = $name;
	}
	
	public function toXml()
	{
		$xml  = XlsxWriter::startXmlTag('font');
		$xml .= XlsxWriter::xmlTag('name', array(
			'val' => $this->name,
		));
		$xml .= XlsxWriter::xmlTag('family', array(
			'val' => $this->family,
		));
		$xml .= XlsxWriter::xmlTag('size', array(
			'val' => $this->size,
		));
		$xml .= XlsxWriter::endXmlTag('font');
		return $xml;
	}
}
