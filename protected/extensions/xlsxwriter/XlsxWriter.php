<?php

class XlsxWriter
{
	protected $strings;
	protected $sheets;
	protected $stylesheet;
	protected $tmp_dir = '/tmp';
	protected $zip;
	
	public function __construct()
	{
		$this->strings = new XlsxStrings();
		$this->sheets = array();
		$this->stylesheet = new XlsxStylesheet();
	}
	
	public function setTempDir($value)
	{
		if (!is_dir($value) || !is_writable($value)) {
			throw new Exception("Can't set temporary directory to $value");
		}
		$this->tmp_dir = rtrim($value, '/');
	}
	
	public function addSheet($name)
	{
		$sheet = new XlsxSheet($name, $this);
		$this->sheets[] = $sheet;
		return $sheet;
	}
	
	public function getStrings()
	{
		return $this->strings;
	}
	
	public function getStylesheet()
	{
		return $this->stylesheet;
	}
	
	public function writeData()
	{
		$tmpname = $this->tmp_dir . '/'. uniqid() . '.zip';
		$this->zip = new ZipArchive();
		if (true !== $this->zip->open($tmpname, ZipArchive::CREATE)) {
			throw new Exception("Can't create zip archire $tmpname");
		}
		
		$this->addDir('docProps');
		$this->addDir('_rels');
		$this->addDir('xl');
		$this->addDir('xl/_rels');
		$this->addDir('xl/worksheets');
		
		$this->createDocProps();
		$this->createContentTypes();
		$this->createWorkbook();
		$this->createRels();
		$this->createStyles();
		$this->createStrings();
		$this->createSheets();
		
		$this->zip->close();
		
		readfile($tmpname);
		unlink($tmpname);
	}
	
	protected function addFile($name, $contents)
	{
		if (!$this->zip->addFromString($name, $contents)) {
			throw new Exception("Can't write to zip archire");
		}
	}
	
	protected function addDir($name)
	{
		if (!$this->zip->addEmptyDir($name)) {
			throw new Exception("Can't write to zip archire");
		}
	}
	
	protected function createDocProps()
	{
		ob_start();
		echo self::xmlDefinition();
		echo self::startXmlTag('Properties', array(
			'xmlns' => 'http://schemas.openxmlformats.org/officeDocument/2006/extended-properties',
			'xmlns:vt' => 'http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes',
		));
		echo self::xmlTag('TotalTime', null, 0);
		echo self::xmlTag('Application', null, 'PHP/XlsxWriter/1.0');
		echo self::endXmlTag('Properties');
		$this->addFile('docProps/app.xml', ob_get_clean());
		
		ob_start();
		echo self::xmlDefinition();
		echo self::startXmlTag('cp:coreProperties', array(
			'xmlns:cp' => 'http://schemas.openxmlformats.org/package/2006/metadata/core-properties',
			'xmlns:dc' => 'http://purl.org/dc/elements/1.1/',
			'xmlns:dcmitype' => 'http://purl.org/dc/dcmitype/',
			'xmlns:dcterms' => 'http://purl.org/dc/terms/',
			'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
		));
		echo self::xmlTag('dcterms:created', array(
			'xsi:type' => 'dcterms:W3CDTF',
		), '2013-09-10T10:34:29.00Z');
		echo self::xmlTag('cp:revision', null, 0);
		echo self::endXmlTag('cp:coreProperties');
		$this->addFile('docProps/core.xml', ob_get_clean());
	}
	
	protected function createContentTypes()
	{
		ob_start();
		echo self::xmlDefinition();
		echo self::startXmlTag('Types', array(
			'xmlns' => 'http://schemas.openxmlformats.org/package/2006/content-types',
		));
		echo self::xmlTag('Override', array(
			'PartName' => '/_rels/.rels',
			'ContentType' => 'application/vnd.openxmlformats-package.relationships+xml',
		));
		echo self::xmlTag('Override', array(
			'PartName' => '/xl/_rels/workbook.xml.rels',
			'ContentType' => 'application/vnd.openxmlformats-package.relationships+xml',
		));
		foreach ($this->sheets as $n => $sheet) {
			echo self::xmlTag('Override', array(
				'PartName' => sprintf('/xl/worksheets/sheet%d.xml', $n + 1),
				'ContentType' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.worksheet+xml',
			));
		}
		echo self::xmlTag('Override', array(
			'PartName' => '/xl/sharedStrings.xml',
			'ContentType' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sharedStrings+xml',
		));
		echo self::xmlTag('Override', array(
			'PartName' => '/xl/workbook.xml',
			'ContentType' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml',
		));
		echo self::xmlTag('Override', array(
			'PartName' => '/xl/styles.xml',
			'ContentType' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.styles+xml',
		));
		echo self::xmlTag('Override', array(
			'PartName' => '/docProps/app.xml',
			'ContentType' => 'application/vnd.openxmlformats-officedocument.extended-properties+xml',
		));
		echo self::xmlTag('Override', array(
			'PartName' => '/docProps/core.xml',
			'ContentType' => 'application/vnd.openxmlformats-package.core-properties+xml',
		));
		echo self::endXmlTag('Types');
		$this->addFile('[Content_Types].xml', ob_get_clean());
	}
	
	protected function createWorkbook()
	{
		ob_start();
		echo self::xmlDefinition();
		echo self::startXmlTag('workbook', array(
			'xmlns' => 'http://schemas.openxmlformats.org/spreadsheetml/2006/main',
			'xmlns:r' => 'http://schemas.openxmlformats.org/officeDocument/2006/relationships',
		));
		echo self::xmlTag('fileVersion', array(
			 'appName' => 'Calc',
		));
		echo self::xmlTag('workbookPr', array(
			 'backupFile' => 'false',
			 'showObjects' => 'all',
			 'date1904' => 'false',
		));
		echo self::xmlTag('workbookProtection');
		echo self::startXmlTag('sheets');
		foreach ($this->sheets as $n => $sheet) {
			echo self::xmlTag('sheet', array(
				'name' => $sheet->name,
				'sheetId' => $n + 1,
				'state' => 'visible',
				'r:id' => sprintf('rId%d', $n + 2),
			));
		}
		echo self::endXmlTag('sheets');
		echo self::xmlTag('calcPr', array(
			 'iterateCount' => '100',
			 'refMode' => 'A1',
			 'iterate' => 'false',
			 'iterateDelta' => '0.001',
		));
		echo self::endXmlTag('workbook');
		$this->addFile('xl/workbook.xml', ob_get_clean());
	}
	
	protected function createRels()
	{
		ob_start();
		echo self::xmlDefinition();
		echo self::startXmlTag('Relationships', array(
			'xmlns' => 'http://schemas.openxmlformats.org/package/2006/relationships',
		));
		echo self::xmlTag('Relationship', array(
			 'Id' => 'rId1',
			 'Type' => 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/officeDocument',
			 'Target' => 'xl/workbook.xml',
		));
		echo self::xmlTag('Relationship', array(
			 'Id' => 'rId2',
			 'Type' => 'http://schemas.openxmlformats.org/package/2006/relationships/metadata/core-properties',
			 'Target' => 'docProps/core.xml',
		));
		echo self::xmlTag('Relationship', array(
			 'Id' => 'rId3',
			 'Type' => 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/extended-properties',
			 'Target' => 'docProps/app.xml',
		));
		echo self::endXmlTag('Relationships');
		$this->addFile('_rels/.rels', ob_get_clean());
		
		ob_start();
		echo self::xmlDefinition();
		echo self::startXmlTag('Relationships', array(
			'xmlns' => 'http://schemas.openxmlformats.org/package/2006/relationships',
		));
		echo self::xmlTag('Relationship', array(
			 'Id' => 'rId1',
			 'Type' => 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles',
			 'Target' => 'styles.xml',
		));
		foreach ($this->sheets as $n => $sheet) {
			echo self::xmlTag('Relationship', array(
				'Id' => sprintf('rId%d', $n + 2),
				'Type' => 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/worksheet',
				'Target' => sprintf('worksheets/sheet%d.xml', $n + 1),
			));
		}
		echo self::xmlTag('Relationship', array(
			 'Id' => sprintf('rId%d', count($this->sheets) + 2),
			 'Type' => 'http://schemas.openxmlformats.org/officeDocument/2006/relationships/sharedStrings',
			 'Target' => 'sharedStrings.xml',
		));
		echo self::endXmlTag('Relationships');
		$this->addFile('xl/_rels/workbook.xml.rels', ob_get_clean());
	}
	
	protected function createStyles()
	{
		ob_start();
		echo self::xmlDefinition();
		echo $this->stylesheet->toXml();
		$this->addFile('xl/styles.xml', ob_get_clean());
	}
	
	protected function createStrings()
	{
		ob_start();
		echo self::xmlDefinition();
		echo $this->strings->toXml();
		$this->addFile('xl/sharedStrings.xml', ob_get_clean());
	}
	
	protected function createSheets()
	{
		foreach ($this->sheets as $n => $sheet) {
			ob_start();
			echo self::xmlDefinition();
			echo $sheet->toXml();
			$this->addFile(sprintf('xl/worksheets/sheet%d.xml', $n + 1), ob_get_clean());
		}
	}
	
	public static function xmlTag($tag, $attributes=null, $value=null)
	{
		$r = "<$tag";
		if (!empty($attributes)) {
			$r .= ' ' . self::xmlAttributes($attributes);
		}
		if (null !== $value) {
			$r .= ">";
			$r .= htmlspecialchars($value);
			$r .= "</$tag>";
		} else {
			$r .= "/>";
		}
		return $r;
	}
	
	public static function startXmlTag($tag, $attributes=null)
	{
		$r = "<$tag";
		if (!empty($attributes)) {
			$r .= ' ' . self::xmlAttributes($attributes);
		}
		$r .= '>';
		return $r;
	}
	
	public static function endXmlTag($tag)
	{
		return "</$tag>";
	}
	
	public static function xmlAttributes($attributes)
	{
		$r  = array();
		foreach ($attributes as $name => $value) {
			$r[] = sprintf('%s="%s"', $name, htmlspecialchars($value));
		}
		return implode(' ', $r);
	}
	
	public function xmlDefinition()
	{
		return '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'."\n";
	}
}
