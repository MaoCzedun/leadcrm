<?php

class XlsxStyleBorder
{
	public $diagonalDown = false;
	public $diagonalUp = false;
	public $left;
	public $right;
	public $top;
	public $bottom;
	public $diagonal;
	
	public function toXml()
	{
		$xml  = XlsxWriter::startXmlTag('border', array(
			'diagonalDown' => $this->diagonalDown ? 'true' : 'false',
			'diagonalUp' => $this->diagonalUp ? 'true' : 'false',
		));
		$xml .= XlsxWriter::xmlTag('left', $this->left ? array('style' => $this->left) : null);
		$xml .= XlsxWriter::xmlTag('right', $this->right ? array('style' => $this->right) : null);
		$xml .= XlsxWriter::xmlTag('top', $this->top ? array('style' => $this->top) : null);
		$xml .= XlsxWriter::xmlTag('bottom', $this->bottom ? array('style' => $this->bottom) : null);
		$xml .= XlsxWriter::xmlTag('diagonal', $this->diagonal ? array('style' => $this->diagonal) : null);
		$xml .= XlsxWriter::endXmlTag('border');
		return $xml;
	}
}
