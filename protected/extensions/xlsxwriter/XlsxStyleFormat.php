<?php

class XlsxStyleFormat
{
	public $name;
	public $code;
	
	public function __construct($name, $code = 0)
	{
		$this->name = $name;
		$this->code = $code;
	}
	
	public function toXml()
	{
		return XlsxWriter::xmlTag('numFmt', array(
			'formatCode' => $this->name,
			'numFmtId' => $this->code,
		));
	}
}
