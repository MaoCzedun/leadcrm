<?php

class XlsxSheetCol
{
	protected $value;
	protected $type;
	protected $style;
	protected $rel;
	
	public function __construct($value, $rel, $style, $type)
	{
		$this->value = $value;
		$this->type = $type;
		$this->style = $style;
		$this->rel = $rel;
	}
	
	public function toXml()
	{
		$xml  = XlsxWriter::startXmlTag('c', array(
			'r' => $this->rel,
			's' => $this->style,
			't' => $this->type,
		));
		$xml .= XlsxWriter::xmlTag('v', null, $this->value);
		$xml .= XlsxWriter::endXmlTag('c');
		return $xml;
	}
}
