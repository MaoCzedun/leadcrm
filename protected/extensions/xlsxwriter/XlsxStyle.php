<?php

class XlsxStyle
{
	public $applyAlignment = false;
	public $applyBorder = false;
	public $applyFont = false;
	public $applyProtection = false;
	public $borderId = 0;
	public $fillId = 0;
	public $fontId = 0;
	public $numFmtId = 0;
	
	public function setFont($value)
	{
		if (false !== $value) {
			$this->fontId = $value;
			$this->applyFont = true;
		} else {
			$this->fontId = 0;
			$this->applyFont = false;
		}
	}
	
	public function setBorder($value)
	{
		if (false !== $value) {
			$this->borderId = $value;
			$this->applyBorder = true;
		} else {
			$this->borderId = 0;
			$this->applyBorder = false;
		}
	}
	
	public function setFill($value)
	{
		$this->fillId = $value;
	}
	
	public function setFormat($value)
	{
		$this->numFmtId = $value;
	}
	
	public function toXml()
	{
		return XlsxWriter::xmlTag('xf', array(
			'applyAlignment' => $this->applyAlignment ? 'true' : 'false',
			'applyBorder' => $this->applyBorder ? 'true' : 'false',
			'applyFont' => $this->applyFont ? 'true' : 'false',
			'applyProtection' => 'false',
			'borderId' => $this->borderId,
			'fillId' => $this->fillId,
			'fontId' => $this->fontId,
			'numFmtId' => $this->numFmtId,
			'xfId' => 0,
		));
	}
}
