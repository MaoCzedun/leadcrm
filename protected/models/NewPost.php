<?php
class NewPost  extends CActiveRecord
{
    
    public static  function model($className = __CLASS__) {
       return  parent::model($className);
    }
    public function attributeLabels() {
        return array(
            'declarationNumber'=>'ИНН',
            'arrivalDate'=>'Даты прибытия',
           
        );
    }
    public function tableName() {
        return 'new_post_records';
    }

    public  function rules(){
        return array(
            
            array('declarationNumber','numerical','integerOnly'=>true),
            array('declarationNumber','length','min'=>3,'max'=>18,'allowEmpty'=>false),
            array('declarationNumber','unique'),
//            array('arrivalDate','date','format'=> 'yyyy-mm-dd','allowEmpty'=>true),
//            сравнение  даты   с сегодняшней 
//           имеем  ошибку 
//            array('arrivalDate','compare','compareAttribute'=>date('yyyy-mm-dd'),'operator'=>'>','message'=>'дата  прибытия не может быть  меньше текущей даты '),
        );
    }
    public function relations() {
        return array(
            'feedback'=>array(self::BELONGS_TO,'Feedback','feedback_id'),
        );
    }

}