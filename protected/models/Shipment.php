<?php 
class Shipment extends CActiveRecord
{
    static public function model($className = __CLASS__) {
      return  parent::model($className);
   }
   
   public function tableName() {
       return 'feedback_shipment';
   }
// проверка уникального   составного  ключа 
   public  function rules(){
       return array(
           array('form_id+to_status_id','application.extensions.uniqueMultiColumnValidator','message'=>'статус   для формы  не может дублироваться '),
       );
   }
   public function attributeLabels() {
       return array(
        'to_status_id'=>'Status',  
        'form_id'=>'Form',  
        'is_sms'=>'isSms',  
        'is_email'=>'isEmail',
       );
   }
   public function relations() {
       return array(
           'Status'=>array(self::BELONGS_TO,'FeedbackStatus','to_status_id'),
//           'Good'=>array(),
           'Form'=>array(self::BELONGS_TO,'Form','form_id'),
          
       );
   }    
}