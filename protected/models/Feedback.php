<?php

class Feedback extends CActiveRecord  
{
	public $short = 0;
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        public function tableName()
	{
		return '{{feedback}}';
	}
	
	public function attributeLabels()
	{
		return array(
			'id' => '№',
			'comment' => Yii::t('feedback', 'Comment'),
			'create_time' => Yii::t('feedback', 'Time Created'),
//			'email' => Yii::t('feedback', 'Email'),
			'manager_id' => Yii::t('feedback', 'Manager'),
			'manager' => Yii::t('feedback', 'Manager'),
                        'client.name'=>'Клиент',
			'status_id' => Yii::t('feedback', 'Status'),
//			'status' => Yii::t('feedback', 'Status'),
			'update_time' => Yii::t('feedback', 'Time Updated'),
			'utm_content' => 'UTM content',
			'utm_campaign' => 'UTM campaign',
			'utm_medium' => 'UTM medium',
			'utm_source' => 'UTM source',
			'utm_term' => 'UTM term',
			'referrer' => 'Источник',
			'confirmed' => 'Подтверждена',
			'mobile_device' => 'Моб. устройство',
			'variant' => 'Вариант',
			'short' => 'Короткая',
                        'good_id'=>'Товар',
                        'declarationNumber'=>'ИНН',
		);
	}
	
	public function rules()
	{
		return array(
//			array('	name,
//					phone',
//					'length', 'max' => 200),
//			array('	email,
//					name,
//					phone',
//					'required', 'on' => 'create, complete'),
//			array('	phone',
//					'required', 'on' => 'create-short'),
			//array('good_id','required','on'=>'create','message'=>'Выбирете товар  из списка'),
                        array('comment','length', 'max' => 16000),
			array('comment','required', 'on' => Yii::app()->user->checkAccess('admin') ? 'never' : 'update'),
//			array('create_time,update_time','date', 'format' => 'yyyy-MM-dd HH:mm:ss'),
//			array('email',
//			//					'length', 'max' => 100),
//			array('email',
//					'email'),
//                      array('	email,
//					manager_id,
//					name,
//					status_id',
//                'safe', 'on' => 'search'),
                        array('manager_id, variant','safe'),
                        array('status_id','safe'),
                        array('good_id','safe'),
                        array('declarationNumber','unique'),
                        array('declarationNumber','numerical','integerOnly'=>true),
                        array('declarationNumber','length','min'=>3,'max'=>18,'allowEmpty'=>false),
		);
	}
	
        
	public function relations()
	{
		return array(
                    'manager' => array(self::BELONGS_TO, 'User', 'manager_id'),
                    'client'=>array(self::BELONGS_TO, 'Client','client_id'),
                    'status' => array(self::BELONGS_TO, 'FeedbackStatus', 'status_id'),
//			'statuslog' => array(self::HAS_MANY, 'FeedbackStatusLog', 'feedback_id'),
                    'good'=>array(self::BELONGS_TO,'Good','good_id'),
                    'newPostRecord'=>array(self::HAS_ONE,'NewPost','feedback_id'),
                    'form'=>array(self::BELONGS_TO,'Form','form_id'),
                    'user'=>array(self::BELONGS_TO,'User','user_id'),
		);
	}
	
	public function behaviors()
	{
		return array(
			array(
				'class' => 'SendMailBehavior',
				'to' => '${mail.admin_email}',
				'subject' => '${mail.subject}',
			),
			array(
				'class' => 'StampBehavior',
				'create_time_attribute' => 'create_time',
				'update_time_attribute' => 'update_time',
				'updated_by_attribute' => 'manager',
//                                'updated_by_attribute' => 'status',
			),
			array(
				'class' => 'RelationBehavior',
				'attributes' => array(
                                    'manager',
                                    'client',
                                    'status',
                                    'good',
					'statuslog' => array(
						'cascadeDelete' => true,
						'quickDelete' => true,
					),
				),
			),
		);
	}
        
	protected function afterSave()
	{
		if ('complete' != $this->getScenario()) {
			$log = new FeedbackStatusLog();
			$log->status_id = $this->status_id;
			$log->user_id= $this->user_id;
                        $log->comment = $this->comment;
			$log->feedback_id = $this->id;
			$log->save(false);
			//if ($this->getIsNewRecord()) {
			//	$this->sendConfirmationEmail();
			//}
		}
		parent::afterSave();
	}
	
	public function statusLog($params=array())
	{
		$criteria = new CDbCriteria($params);
		$criteria->compare('t.feedback_id', $this->id);
		$criteria->with = array('manager');
		return new CActiveDataProvider('FeedbackStatusLog', array(
			'criteria' => $criteria,
			'sort' => array(
				'attributes' => array(
//					'manager' => array(
//						'asc' => 'manager.username ASC',
//						'desc' => 'manager.username DESC',
//					),
                                        'status.name' => array(
                                            'asc' => 'status.name ASC',
                                            'desc' => 'status.name DESC',
                                        ),
					'*',
				),
				'defaultOrder' => 'time DESC',
			),
		));
	}
	
	public function search($params=array(), $defaultAttribs=array())
	{
            $criteria = new CDbCriteria($params);
//                $criteria->addSearchCondition('user_id',Yii::app()->user->getId(),true);
            $criteria->with = array('status');
            $criteria->compare('status.id', $this->status_id, true);
            $criteria->compare('t.manager_id', $this->manager_id);
            $criteria->compare('t.status_id', $this->status_id);
            $criteria->addColumnCondition(array('t.user_id'=>Yii::app()->user->getId()));
		
            return new CActiveDataProvider($this, array(
                'criteria' => $criteria,
		'sort' => array(
                    'attributes' => array(
//			'manager' => array(
//                          'asc' => 'manager.username ASC',
//                          'desc' => 'manager.username DESC',
//			),
                        'status.name' => array(
                            'asc' => 'status.name ASC',
                            'desc' => 'status.name DESC',
                        ),
			'*',
                    ),
                    'defaultOrder' => 't.create_time DESC',
		),
            ));
	}

        public static function getStatusList()
        {
            $criteria = new CDbCriteria;
            $criteria->condition = 'user_id=0';

            $opts = CHtml::listData(FeedbackStatus::model()->findAll($criteria),'id','name');

            return $opts;
        }
	
	protected function sendConfirmationEmail()
	{
            $mailer = new Mail();
            $message = new Messages();
            $message->response = 'please check your mail';
            $message->subject = 'Confirm';
            $message->save();
            $mailer->message = $message;
            $mailer->debug = defined('DEBUG_MAIL') && DEBUG_MAIL;
//            $mailer->subject = 'Подтверждение заявки с сайта';
            $mailer->isHtml = true;
            $mailer->render('application.views.mail.confirmation', array(
                'model' => $this,
            ));
                
            return $mailer->send($this->email,'FeedBack');
	}
}
