<?php

class Client extends CActiveRecord
{
    public $short = 0;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function rules()
    {
        return array(
            array('email','email'),
            array('name,city','length','min'=>3,'max'=>50,'tooShort'=>'Поле слишком короткое','tooLong'=>'Поле слишком длинное'),
            array('name,phone','required'),
            array('phone','match','pattern'=>'/^\+\d{2}\(\d{3}\)\d{3}-\d{2}-\d{2}$/'),
            array('phone','unique'),
            
            array('name','match','pattern'=>'/[a-zA-Z]+$/s', 'message'=>'Поле  должны  содержать только текст '),
//            array('create_time','date', 'format' => 'yyyy-MM-dd HH:mm:ss'),
//            array('create_time','default', 'value' => date('Y-m-d H:i:s'),'setOnEmpty'=>false),
            
//             следует   также  добавить полу create_time и задать его по дефолту через функцию time или data
        );
    }
    public function tableName()
    {
        return '{{client}}';
    }
    public function relations()
    {
       return  array(
           //  получаем все   заявкт даного пользователя
            'feedbacks'=>array(self::HAS_MANY,'Feedback','client_id'),
            'user'=>array(self::BELONGS_TO,'User','user_id'),
        );
    }
    public function attributeLabels()
    {
        return array(
            'id' => 'Client №',
            'name'=>'Имя',
            'city'=>'Город',
            'email'=>' Электронный адрес',
            'phone'=>'телефон',
            
//            'comment' => Yii::t('feedback', 'Comment'),
//            'create_time' => Yii::t('feedback', 'Time Created'),
        );
    }
//                    ПРописать отношение  между таблицей FeedBack and Client
    public function search($params=array(), $defaultAttribs=array())
    {
        $criteria = new CDbCriteria($params);
        $criteria->compare('t.id', $this->id, true);


        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'attributes' => array(
//					'manager' => array(
//						'asc' => 'manager.username ASC',
//						'desc' => 'manager.username DESC',
//					),
                    '*',
                ),
                'defaultOrder' => 't.create_time DESC',
            ),
        ));
    }

}