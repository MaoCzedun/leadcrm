<?php
class RegisterUser extends CModel
{
    public $email;
    public static function model($className = __CLASS__) {
      return parent::model($className);
 }
 public function rules() {
     return array(
         array('email','required'),
         array('email','email'),
     );
 }
 public function attributeNames() {
     return array(
        'email'=>'Эллектронный адрес',
    );
 }
}

