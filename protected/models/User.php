<?php

class User extends CActiveRecord  
{
//         неактивирован                
	const STATUS_DISABLED = 0;
//	   активирован
        const STATUS_ENABLED = 1;
//	   заблокирован
        const STATUS_LOCKED = 2;
	const SCENARIO_SIGNUP = 'signup';
	public $password_plain;
	public $password_confirm;
        static private $currentAuthUser = null;
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
   
        public  function initRegister($registerUser)
        {
            $this->email = $registerUser->email;
            
        }
	public function tableName()
	{
		return 'user';
	}
	
	public function attributeLabels()
	{
		return array(
                        'first_Name'=>'Имя',
			'email' => Yii::t('user', 'Email'),
			'username' => Yii::t('user', 'Username'),
			'role' => Yii::t('user', 'Role'),
			'roleName' => Yii::t('user', 'Role'),
			'status' => Yii::t('user', 'Status'),
			'statusName' => Yii::t('user', 'Status'),
			'password_plain' => Yii::t('user', 'Password'),
			'password_confirm' => Yii::t('user', 'Confirm Password'),
                        'turbosms_login'=>'TurboSms имя  пользователя ',
                        'turbosms_password'=>'Пароль  TurboSms',
                        'turbosms_sender'=>'Отправитель',
                        'newPostKey'=>'Ключ новой  почты',
		);
	}
	public function  relations()
        {
            return array(
                'clients'=>array(self::HAS_MANY,'Client','user_id'),
                'goods'=>array(self::HAS_MANY,'Good','user_id'),
                'feedback'=>array(self::HAS_ONE,'Feedback','user_id'),
            );
        }

        public function rules()
	{
		return array(
//                        array('first_Name','length','min'=>3,'max'=>20,'allowEmpty'=>false),
			array('email','required','on'=>'signup'),
                        array('email','email'),
			array('email,role,username,status','required','on'=>'insert'),
			array('username','match', 'pattern' => '/^[a-z0-9 _.-]+$/i', 'message' => 'В логине можно использовать только латинские буквы, цыфры, пробелы, тире, точки и символ подчеркивания'),
			array('turbosms_login','match', 'pattern' => '/^[a-z0-9 _.-]+$/i', 'message' => 'В логине можно использовать только латинские буквы, цыфры, пробелы, тире, точки и символ подчеркивания'),
			array('turbosms_sender','match', 'pattern' => '/^[a-z0-9 _.-]+$/i', 'message' => 'В логине можно использовать только латинские буквы, цыфры, пробелы, тире, точки и символ подчеркивания'),
			array('turbosms_password','length', 'min' => 3, 'max' =>20 ,'allowEmpty'=>false),
			array('username','unique', 'on' => 'create'),
			array('username','unique', 'on' => 'update', 'criteria' => array('condition' => 'id != :id', 'params' => array(':id' => $this->id))),
			array('email','unique', 'on' => 'create'),
			array('email','unique', 'on' => 'update', 'criteria' => array('condition' => 'id != :id', 'params' => array(':id' => $this->id))),
			array('password_plain,password_confirm','required', 'on' => 'create'),
			array('password_plain','safe', 'on' => 'update'),
			array('password_confirm', 'compare', 'compareAttribute' => 'password_plain', 'on' => 'create, update'),
			array('email,username,role','safe', 'on' => 'search'),
			array('email,username,role','safe', 'on' => 'search'),
                        array('newPostKey','match','pattern'=>'^[a-zA-Z0-9]+$ '),
                    
		);
	}
	
//        стоит  переделать  данный  метод так как не  будет  хешировать пароль 
	protected function beforeSave()
	{
		if(parent::beforeSave()) {
			if($this->isNewRecord) {
//                              неправильное использхование  вместо time стоит  использовать date()
				$this->date_created = time();
			}
			if (! empty($this->password_plain)) {
				$this->salt = $this->generateSalt();
				$this->password = $this->hashPassword($this->password_plain, $this->salt);
			}
			return true;
		} else {
			return false;
		}
	}
	
	public function search($params=array())
	{
		$criteria = new CDbCriteria($params);
		$criteria->compare('t.username', $this->username, true);
		$criteria->compare('t.email', $this->email, true);
		$criteria->compare('t.role', $this->role, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
	
	public static function getList($params=array())
	{
		$criteria = new CDbCriteria($params);
		$criteria->order = 'username';
		return CHtml::listData(self::model()->findAll($criteria), 'id', 'username');
	}
	
	public function validatePassword($password)
	{
		return $this->hashPassword($password, $this->salt) === $this->password;
	}

	public function hashPassword($password, $salt)
	{
		return md5($salt . $password);
	}

	public function generateRandomString($len=10)
	{
		$alpha = '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$l = strlen($alpha);
		$str = '';
		for ($i = 0; $i < $len; $i++) {
			$str .= $alpha[rand(0, $l - 1)];
		}
		return $str;
	}
	
	public function generateSalt()
	{
		return $this->generateRandomString(15);
	}
	
	public function getStatusName()
	{
		switch ($this->status) {
			case self::STATUS_LOCKED:
				return Yii::t('user', 'Locked');
				
			case self::STATUS_ENABLED:
				return Yii::t('user', 'Active');
				
			case self::STATUS_DISABLED:
				return Yii::t('user', 'Inactive');
				
			default:
				return Yii::t('user', 'Undefined');
		}
	}
	
	public static function listRoles()
	{
		return Yii::app()->authManager->listRoles();
	}
	
	public function getRoleName()
	{
		return Yii::app()->authManager->getRoleDescription($this->role);
	}
	
	public function __toString()
	{
		return $this->getDisplayName();
	}
	
	public function getDisplayName()
	{
		return $this->username;
	}
        //слишком сложно 
        
        static   public  function getCurrentUser()
        {
            if (Yii::app()->user->isGuest) {
                return new self();
            }
            if (is_null(self::$currentAuthUser)) {
                self::$currentAuthUser = self::model()->findByPk( Yii::app()->user->getId());
            }
            return self::$currentAuthUser;
        }
}
