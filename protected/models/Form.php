<?php
class Form extends  CActiveRecord
{
   static public function model($className = __CLASS__) {
      return  parent::model($className);
   }
   public function tableName() {
       return 'feedback_form';
   }
   public function attributeLabels() {
       return array(
        'user_id'=>'User',  
        'upsale_url'=>'UpsaleUrl',  
        'code'=>'FormCode',  
        'good_id'=>'Good',
        'name'=>'NameForm',
           
       );
   }
   public function  rules()
   {
       return array(
            array('upsale_url','url'),
            array('name','unique'),  
       );
   }

   public function relations() {
       return array(
//           'User'=>array(),
//           'Good'=>array(),
           'Shipment'=>array(self::HAS_ONE,'Shipment','form_id'),
//           'Status'=>array(),
           'feedback'=>array(self::HAS_ONE,'feedback','form_id'),
       );
   }
}

