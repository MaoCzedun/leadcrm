<?php

class Good extends CActiveRecord
{
    public $numberSaledGoods;
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public static function instance($class=__CLASS__)
    {
        return parent::instance($class);
    }

    public function tableName()
    {
        return '{{good}}';
    }

    public function rules()
    {
        return array(
            array('name','length', 'max' => 50),
            array('img','length', 'max' => 200),
            array('price,amount,purchase_price','numerical'),
            array('name,description,price,amount','required','except'=>'update'),
            array('price,amount','compare','compareValue'=>0,'operator'=>'>'),
            array('purchase_price','compare','compareValue'=>0,'operator'=>'>','allowEmpty'=>true),
            array('date_added','date','format'=>'yyyy-MM-dd'),  
            array('numberSaledGoods','compare','compareAttribute'=>'amount','operator'=>'<','except'=>'supply'),
        );
    }

    public function relations()
    {
        return array(
            'user'=>array(self::BELONGS_TO,'User','user_id'),
            'feedBack'=>array(self::HAS_ONE,'FeedBack','good_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'id',
            'name' => 'Название товара',
            'description' => 'Описание',
            'price' => 'Цена',
            'amount' => 'Количество',
            'img' => 'Изображение',
            'purchase_price'=>'закупочная цена',
        );
    }

    public function search($params=array())
    {
        $criteria = new CDbCriteria($params);
        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('t.description', $this->description, true);
        $criteria->compare('t.price', $this->price, true);
        $criteria->compare('t.amount', $this->amount, true);
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
    public static function getListGood()
    {
        $array =    User::model()->findByPk(Yii::app()->user->id)->goods;
        $list = CHtml::listData($array,'id','name');
        return $list;         
    }
}