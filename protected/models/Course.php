<?php

class Course extends CActiveRecord  
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function tableName()
	{
		return '{{course}}';
	}
	
	public function attributeLabels()
	{
		return array(
			'name' => Yii::t('course', 'Name'),
		);
	}
	
	public function rules()
	{
		return array(
			array('	name', 
					'length', 'max' => 200),
			array('	name', 
					'required'),
			array('	name', 
					'safe', 'on' => 'search'),
		);
	}
	
	public function relations()
	{
		return array(
		);
	}
	
	public function behaviors()
	{
		return array(
		);
	}
	
	public function search($params=array(), $defaultAttribs=array())
	{
		$criteria = new CDbCriteria($params);
		$criteria->compare('t.name', $this->name, true);
		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}
	
	public function __toString()
	{
		return $this->getDisplayName();
	}
	
	public function getDisplayName()
	{
		return $this->name;
	}
	
	public static function getList()
	{
		$criteria = new CDbCriteria();
		return CHtml::listData(self::model()->findAll($criteria), 'id', 'displayName');
	}
}
