<?php

class FeedbackStatus extends CActiveRecord {

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public static function instance($class=__CLASS__)
    {
        return parent::instance($class);
    }

    public function tableName()
    {
        return '{{feedback_status}}';
    }

    public function rules()
    {
        return array(
            array('name','length', 'max' => 50),
            array('name','required'),
            array('color','required', 'on' => 'create'),
        );
    }

    public function relations()
    {
        return array(
            'feedback'=> array(self::HAS_MANY, 'Feedback', 'status_id'),
            'Shipment'=>array(self::HAS_ONE,'Shipment','to_status_id'),
        );
    }
    public function attributeLabels()
    {
        return array(
            'id' => 'id',
            'name' => 'Cтатус',
            'color' => 'Цвет',
            'user_id' => 'User Id'
        );
    }

    public function search($params=array())
    {
        $criteria = new CDbCriteria($params);
        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('t.color', $this->color, true);
        $criteria->compare('t.user_id', $this->user_id, true);
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
   
}
