<?php
class  Messages extends CActiveRecord
{
    public $response; 
    public $subject;
    const CHARSET="utf-8";
    public static  function model($className = __CLASS__) {
        return parent::model($className);
    }
    public function attributeLabels() {
        return array(
            'subject'=>'тема письма',
            'response'=>'ответ письма',
            
        );
    }
    public function tableName() {
        return 'messages';
    }
}
