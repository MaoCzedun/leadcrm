<?php

class MailOptions extends OptionRecord 
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public static function instance($class=__CLASS__)
	{
		return parent::instance($class);
	}
	
	public function attributeLabels()
	{
		return array(
			'admin_email' => 'alexmaoczedun@gmail.com',
			'method' => Yii::t('admin.mailOptions', 'Method'),
			'smtp_host' =>  'smtp.gmail.com',
			'smtp_login' => 'alexmaoczedun@gmail.com',
			'smtp_pass' => 'killmeplease',
			'smtp_port' => 587,
			'smtp_ssl' => true,
		);
	}
	
	public function rules()
	{
		return array(
			array('	admin_email,
					smtp_host,
					smtp_login,
					smtp_pass', 
					'length', 'max' => 100),
			array('	admin_email', 
					'email'),
			array('	method', 
					'in', 'range' => array('mail', 'smtp')),
			array('	method', 
					'required'),
			array('	smtp_port', 
					'numerical', 'integerOnly' => true),
			array('	smtp_ssl', 
					'boolean'),
			array('	subject', 
					'length', 'max' => 200),
		);
	}
	
	public function relations()
	{
		return CMap::mergeArray(parent::relations(), array(
		));
	}
	
	protected function metaFields()
	{
		return array(
			'admin_email',
			'method',
			'smtp_host',
			'smtp_login',
			'smtp_pass',
			'smtp_port',
			'smtp_ssl',
			'subject',
		);
	}
}
