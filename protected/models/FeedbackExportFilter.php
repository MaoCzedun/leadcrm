<?php

class FeedbackExportFilter extends CFormModel
{
	public $date_start;
	public $date_end;
	public $id_start;
	public $id_end;
	
	public function attributeLabels()
	{
		return array(
			'date_start' => 'Созданы с',
			'date_end' => 'Созданы по',
			'id_start' => 'Номер с',
			'id_end' => 'Номер по',
		);
	}
	
	public function rules()
	{
		return array(
			array('	date_start,
					date_end',
					'date', 'format' => 'yyyy-MM-dd'),
			array('	id_start,
					id_end',
					'numerical', 'integerOnly' => true),
		);
	}
	
	public function createCriteria()
	{
		$criteria = new CDbCriteria();
		
		if (!empty($this->date_start)) {
			$criteria->addCondition('DATE(t.create_time) >= :start_date');
			$criteria->params[':start_date'] = $this->date_start;
		}
		if (!empty($this->date_end)) {
			$criteria->addCondition('DATE(t.create_time) <= :date_end');
			$criteria->params[':date_end'] = $this->date_end;
		}
		if (!empty($this->id_start)) {
			$criteria->addCondition('t.id >= :id_start');
			$criteria->params[':id_start'] = $this->id_start;
		}
		if (!empty($this->id_end)) {
			$criteria->addCondition('t.id <= :id_end');
			$criteria->params[':id_end'] = $this->id_end;
		}
		
		return $criteria;
	}
}
