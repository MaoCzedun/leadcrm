<?php

class FeedbackStatusLog extends CActiveRecord  
{
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function tableName()
	{
		return '{{feedback_status_log}}';
	}
	
	public function attributeLabels()
	{
		return array(
			'id' => '#',
			'manager_id' => Yii::t('feedback', 'Manager'),
			'manager' => Yii::t('feedback', 'Manager'),
			'status_id' => Yii::t('feedback', 'Status'),
			'comment' => 'Комментарий',
			'time' => Yii::t('feedback', 'Time Updated'),
		);
	}
	
	public function relations()
	{
		return array(
			'manager' => array(self::BELONGS_TO, 'User', 'manager_id'),
		);
	}
	
	public function behaviors()
	{
		return array(
			array(
				'class' => 'StampBehavior',
				'create_time_attribute' => 'time',
				'created_by_attribute' => 'manager',
			),
			array(
				'class' => 'RelationBehavior',
				'attributes' => array(
					'manager',
				),
			),
		);
	}
}
