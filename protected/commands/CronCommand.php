<?php

class CronCommand extends CConsoleCommand
{
	const EXPIRE_HOURS = 48;
	
    public function actionExpireFeedback()
    {
		$new_status = FeedbackStatusOptions::instance()->new_status;
		$exp_status = FeedbackStatusOptions::instance()->expired_status;
		
		$criteria = new CDbCriteria();
		$criteria->compare('status', $new_status);
		$criteria->addCondition('DATE_ADD(create_time, INTERVAL :h HOUR) < NOW()');
		$criteria->params[':h'] = self::EXPIRE_HOURS;
		
		Feedback::model()->updateAll(array(
			'status' => $exp_status,
		), $criteria);
	}
}
