<div class="mfp-hide white-popup-block " id="registerForm">
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/rc/js/plugins/magpoup/magnific-popup.css');?>
    <?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/rc/css/admin.css');?>
    <?php // Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/rc/dashboard/css/admin.css');?>
    <!-- стоит  настроить правильный путь  -->
    
    <?php Yii::app()->controller->renderPartial('_signup',array('registerUserModel'=>$userModel))?>
</div>
<nav>
    <div class="wrapper">
        <div class="logo">
            <img src="img/logo.png" alt="">
            <span>CRM</span>
        </div>
        <ul>
            <li><a class="scroll" href="#functional">Функционал</a></li>
            <li><a class="scroll" href="#detail">Обзор</a></li>
            <li><a class="scroll" href="#reasons">Почему мы?</a></li>
            <li><a class="scroll" href="#">Контакты</a></li>
        </ul>
        <?php // VarDumper::dump('application.modules.views.user.signup');?>
        <?php echo CHtml::link(CHtml::htmlButton('Enter',array('class'=>'log_in')), Yii::app()->controller->createUrl('admin/default/login'));?>
        <?php echo CHtml::link(CHtml::htmlButton('Register',array('class'=>'register')),'#registerForm',array('class'=>'popup-with-form'));?>
       <!--<a class="popup-with-form" href="#registerForm">Open form</a>--> 
        
    </div>
</nav>

  
<section class="main">
    <div class="wrapper">
        <h1> Удобная, функциональная, простая CRM система для продажи товаров через одностраничные сайты</h1>
        <h2>Мы создали эту СRM-систему, чтобы упростить и на 97% автоматизировать работу нашего бизнеса.</h2>
        <span>Теперь получите бесплатный доступ и вы.</span>
        <span class="sec">это займет  15 секунд</span>
        <span class="free">Это абсолютно  бесплатно</span>
        <button type="button">Получить доступ</button>
    </div>
</section>
<section class="functional" id="functional">
    <div class="wrapper">
        <h3>Наш функционал</h3>
        <ul>
            <li>
                <img src="img/d_b.png" alt="">
                <span>База лидов с понятными статусами</span>
                <p>Для сбора всей информации по заявкам и группировки по статусам</p>
            </li>
            <li>
                <img src="img/storage.png" alt="">
                <span>Функция склада</span>
                <p>Для сбора всей информации по заявкам и группировки по статусам</p>
            </li>
            <li>
                <img src="img/client.png" alt="">
                <span>Работа с базой клиентов</span>
                <p>Удобная работа с карточкой клиента, где можно изменять его данные, менять статусы, оставлять заметки, отправлять сообщения + возможность экспорта/импорта клиентов</p>
            </li>
            <li>
                <img src="img/post.png" alt="">
                <span>Интеграция с Почтой России и Новой Почтой</span>
                <p>Для печати бланков (накладных) и отслеживания статуса отправки внутри CRM</p>
            </li>
            <li>
                <img src="img/sms.png" alt="">
                <span>СМС-рассылка</span>
                <p>Для информирования клиентов о отправках, акциях и т.п.</p>
            </li>
            <li>
                <img src="img/generator.png" alt="">
                <span>Генератор форм заказа</span>
                <p>Для интеграции Вашего сайта с CRM-системой</p>
            </li>
            <li>
                <img src="img/utm.png" alt="">
                <span>Генератор UTM-меток</span>
                <p>Для подробной аналитики рекламных компаний + отображение в входящей заявке всех UTM параметровэкспорта/импорта клиентов</p>
            </li>
            <li>
                <img src="img/statis.png" alt="">
                <span>Статистика продаж</span>
                <p>Для учета количества принятых заявок, оформленных заявок, отправок, продаж, возвратов в общем и по каждому товару</p>
            </li>
            <li>
                <img src="img/money.png" alt="">
                <span>Финансовый учет</span>
                <p>Для ведения учета по доходам и издержкам, чистой прибили и оценке эффективности бизнеса</p>
            </li>
            <li>
                <img src="img/client.png" alt="">
                <span>Добавление сотрудников и управление доступом</span>
                <p>Для распределения обязанностей и уровня доступа сотрудников к данным</p>
            </li>
            <li>
                <img src="img/sales.png" alt="">
                <span>Воронка продаж</span>
                <p>Для определения эффективности работы на каждом уровне бизнес-процессов</p>
            </li>
            <li>
                <img src="img/upsel.png" alt="">
                <span>Функция «UPSELL»</span>
                <p>Для автоматизации внедрения допродаж в Ваш бизнес</p>
            </li>
        </ul>
    </div>
</section>
<section class="detail" id="detail">
    <div class="wrapper">
        <img src="img/zoom.png" alt="">
    </div>
</section>
<section class="reasons" id="reasons">
    <div class="wrapper">
        <h3>3 причины почему Вам это надо?</h3>
        <ul>
            <li>
                <span>1</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed convallis, odio nec vulputate vulputate, nulla est condimentum ex, sit amet sollicitudin metus ex vel eros.</p>
            </li>
            <li>
                <span>2</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed convallis, odio nec vulputate vulputate, nulla est condimentum ex, sit amet sollicitudin metus ex vel eros.</p>
            </li>
            <li>
                <span>3</span>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed convallis, odio nec vulputate vulputate, nulla est condimentum ex, sit amet sollicitudin metus ex vel eros.</p>
            </li>
        </ul>
        <button type="button">Получить доступ</button>
    </div>
</section>
<footer>
    <div class="wrapper">
        <div class="right_column">
            <span>Остались вопросы?</span>
            <button type="button">Задать вопрос</button>
            <button type="button">Партнерская програма</button>
            <ul class="cards">
                <li><a href="#"><img src="img/card.png" alt=""></a></li>
                <li><a href="#"><img src="img/card.png" alt=""></a></li>
                <li><a href="#"><img src="img/card.png" alt=""></a></li>
            </ul>
        </div>
        <div class="left_column">
            <span class="questions">Остались вопроси ? Lorem ipsum dolor sit amet</span>
            <ul class="social">
                <li><a href="#"><img src="img/v_k.png" alt=""></a></li>
                <li><a href="#"><img src="img/f_b.png" alt=""></a></li>
                <li><a href="#"><img src="img/twitt.png" alt=""></a></li>
                <li><a href="#"><img src="img/google.png" alt=""></a></li>
            </ul>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed convallis, odio nec vulputate vulputate,</p>
            <span class="copyright">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</span>
        </div>
    </div>
</footer>
