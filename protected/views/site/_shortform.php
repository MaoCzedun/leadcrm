<?php 
	$uid = isset($uid) ? $uid : '';
	$placeholder = isset($placeholder) ? $placeholder : 'Ваш телефон';
	$buttontext = isset($buttontext) ? $buttontext : 'Заказать <span>стильную</span> рамку';
	$form = $this->beginWidget('CActiveForm', array(
		'action' => Yii::app()->createUrl('site/request', array('short' => 'yes')),
		'id' => 'short'.$uid,
		'enableClientValidation' => true,
		'clientOptions' => array(
			'validateOnSubmit' => true,
            'validateOnChange'=>false,
			'afterValidate' => 'js:submitFormAjax',
		),
	)); ?>
	
	<?php $model->variant = $var; ?>
	
	<?php echo $form->hiddenField($model, 'variant'); ?>

	<div id="shortform">
		<div class="input">
			<?php $this->widget('CMaskedTextField', array(
				'model' => $model,
				'attribute' => 'phone',
				'mask' => '+7 999 999 9999',
				'htmlOptions' => array(
					'placeholder' => $placeholder,
					'id' => 'short_phone'.$uid,
				),
			)); ?>
		</div>
		<div class="submitbtn">
			<button type="submit" class="button shortformbtn"><?php echo $buttontext; ?></button>
		</div>
		<?php echo $form->error($model, 'phone', array(
			'inputID' => 'short_phone'.$uid,
		)); ?>
	</div>
	
<?php $this->endWidget(); ?>
