
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/rc/dashboard/css/bootstrap.min.css');?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/rc/dashboard/css/admin.css');?>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl.'/rc/dashboard/css/font-awesome.css');?>
<div class="login_page">
    <div class="error500">
         <i class="fa fa-exclamation-triangle"></i>
         <div class="">500</div>
         <h2>Page not found!</h2>
    </div>
</div>