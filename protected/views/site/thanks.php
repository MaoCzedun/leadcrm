<section id="apply_result">
	<div class="cover"></div>
	<div class="wrapper">
		<div class="thanks_message">
			<?php if ($result > 0): ?>
				<h2>
					Ваша заявка подтверждена!<br />
					Спасибо!
				</h2>
			<?php else: ?>
				<h2>
					Не удалось найти заявку!<br />
					<a href='/'>Попробуйте снова</a>
				</h2>
			<?php endif; ?>
		</div>
	</div>
</section>

