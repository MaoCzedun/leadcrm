<?php 
	$form = $this->beginWidget('CActiveForm', array(
		'action' => $model->getIsNewRecord() ? 
			Yii::app()->createUrl('site/request') :
			Yii::app()->createUrl('site/complete', array('id' => $model->id)),
		'id' => $id,
		'enableClientValidation' => true,
		'clientOptions' => array(
			'validateOnSubmit' => true,
			'afterValidate' => 'js:submitFormAjax',
		),
		'htmlOptions' => array(
			'class' => 'form',
		),
)); ?>
<?php $model->variant = $var; ?>

<?php echo $form->hiddenField($model, 'variant'); ?>
	<p>
		<?php echo $form->textField($model, 'name', array(
			'placeholder' => 'Ваше имя',
			'id' => "{$id}_name",
		)); ?>
		<?php echo $form->error($model, 'name', array(
			'inputID' => "{$id}_name",
		)); ?>
	</p>
    <p>
        <?php $this->widget('CMaskedTextField', array(
            'model' => $model,
            'attribute' => 'phone',
            'mask' => '+7 999 999 9999',
            'htmlOptions' => array(
                'placeholder' => 'Ваш телефон',
                'id' => "{$id}_phone",
            ),
        )); ?>
        <?php echo $form->error($model, 'phone', array(
            'inputID' => "{$id}_phone",
        )); ?>
    </p>
	<p>
		<?php echo $form->textField($model, 'email', array(
			'placeholder' => 'Ваш email',
			'id' => "{$id}_email",
		)); ?>
		<?php echo $form->error($model, 'email', array(
			'inputID' => "{$id}_email",
		)); ?>
	</p>

	<p>
		<input type="submit" class="button" value="Перезвоните мне" />
	</p>
<?php $this->endWidget(); ?>
