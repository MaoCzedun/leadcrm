<div id="callbackcomplete">
	<p class="title">Заказ обратного звонка</p>

	<?php $this->renderPartial('_form', array(
		'id' => 'formcallbackcomplete',
		'var' => $model->variant,
		'model' => $model,
	)); ?>

	<p class="policy">
		<a target="_blank" href="rcs_policy.pdf">Политика конфиденциальности</a>
	</p>
</div>
