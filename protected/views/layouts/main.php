<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>CRM</title>
    <link href='http://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700&subset=cyrillic,latin' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Bad+Script&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery.fancybox.css" media="screen" />
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/img/favicon.ico" rel="shortcut icon" />
    <!--[if lt IE 9]>
    <script>
        document.createElement('header');
        document.createElement('nav');
        document.createElement('section');
        document.createElement('article');
        document.createElement('aside');
        document.createElement('footer');
    </script>
    <![endif]-->

    <?php
    $cs = Yii::app()->clientScript;
    $cs->registerScriptFile('/rc/js/jquery-2.1.0.js');
//      $cs->registerScriptFile('/rc/bootstrap/js/bootstrap.min.js');
    $cs->registerScriptFile('/js/jquery.scrollTo-1.4.3.1-min.js');
    $cs->registerScriptFile('/js/jquery.fancybox.js');
    $cs->registerScriptFile('/js/jquery.magnific-popup.js');
    
    $cs->scriptMap=array(
        'jquery.magnific-popup.js'=>'/rc/js/plugins/magpoup/jquery.magnific-popup.js',
    );
    $cs->registerScriptFile('/js/main.js');
    ?>
    

    <script type="text/javascript">
        $(document).ready(function() {
            $('.fancybox').fancybox({
                'helpers': {
                    'title': {
                        'type': 'inside'
                    }
                },
                afterShow: function() {
                    document.getElementById('formcallback_name').focus();
                }
            });
            document.getElementById('short_phone').focus();
            simpleSlider('.slider');
        });
    </script>
</head>
<body>
    <?php echo $content; ?>
    <?php $this->renderPartial('counter');?>
</body>
</html>