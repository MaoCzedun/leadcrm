<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	</head>
	<body>
	<div style="background: #3AA3DE; height: 100%;">
	<table CELLSPACING="0" CELLPADDING="0" BORDER="0" width="100%" align="center">
		<tr>
			<td></td>
			<td width="660" align="center" valign="top" style="width:660px;">
				<table CELLSPACING="0" CELLPADDING="0" BORDER="0" width="660px">
					<tr>
						<td width="400" height="75" valign="top" style="background: #fff;">
							<img style="display: block;" src="http://online-mesi.performlp.com/i/logo_mail.jpg" width="400" height="75" />
						</td>
						<td width="260" height="75" valign="top" style="background: #fff;">
							<img style="display: block;" width="260" height="75" src="http://online-mesi.performlp.com/i/mail_bg.gif" width="260" height="75" />
						</td>
					</tr>
					<tr>
						<td colspan='2'  style="background: #fff; height:5px;"></td>
					</tr>
				</table>
				<table CELLSPACING="0" CELLPADDING="0" BORDER="0" width="660px">
					<tr>
						<td colspan='3'  style="background: #EFEFEF; height:30px;"></td>
					</tr>
					<tr>
						<td style="background: #EFEFEF; width: 30px; text-align: center;" width="30"></td>
						<td style="background: #EFEFEF;">
							<p style="color: #000000; font-size: 21px; font-family: Arial; margin-top: 0;">
								Уважаем(ый)ая <?php echo CHtml::encode($model->name); ?>!
							</p>
							<p style="color: #000000; font-size: 15px; font-family: Arial; margin-top: 0;">
								 Благодарим вас за интерес, проявленный к образовательным услугам МЭСИ. Чтобы подтвердить свою заявку, пройдите, пожалуйста, по ссылке:
							</p>
							<p style="font-size: 21px; font-family: Arial; margin-top: 0;">
								 <a style="color: #70B800" href="<?php echo Yii::app()->createAbsoluteUrl('site/confirm', array('code' => $model->confirmation_code)); ?>">
								 	<?php echo Yii::app()->createAbsoluteUrl('site/confirm', array('code' => $model->confirmation_code)); ?>
								 </a>
							</p>
						</td>
						<td style="background: #EFEFEF; width: 30px;" width="30"></td>
					</tr>
					<tr>
						<td colspan='3'  style="background: #EFEFEF; height:30px;"></td>
					</tr>
					<tr>
						<td colspan='3'  style="background: #EFEFEF; height:1px;"></td>
					</tr>
					<tr>
						<td style="background: #ffffff; width: 30px; text-align: center;" width="30"></td>
						<td  style="background: #ffffff; height:30px;" valign="middle">
							<p style="color: #999; font-size: 10px; font-family: Arial;">
								© 2001-2014 Московский государственный университет экономики, статистики и информатики
							</p>
						</td>
						<td style="background: #ffffff; width: 30px; text-align: center;" width="30"></td>
					</tr>
					<tr>
						<td colspan="3" height="1" style="height: 1px; background: #2891C9;"></td>
					</tr>
				</table>
			</td>
			<td></td>
		</tr>
		<tr>
			<td colspan="3" height="40" style="height: 40px;"></td>
		</tr>
	</table>
</div>

	</body>
</html>
