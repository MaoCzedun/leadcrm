<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	</head>
	<body>
		<p><?php echo Yii::app()->format->formatDatetime($model->create_time); ?>, поступила новая 
		заявка на обратный звонок.</p>
		<dl>
			<?php $this->widget('zii.widgets.CDetailView', array(
				'data' => $model,
				'tagName' => 'dl',
				'itemTemplate' => '<dt><b>{label}:</b></dt><dd>{value}</dd>',
				'attributes' => array(
					'name',
					'email',
					'phone',
				),
			)); ?>
		</dl>
		<a href="<?php echo Yii::app()->createAbsoluteUrl('admin/feedback/view', array('id' => $model->id)); ?>">Перейти к заявке</a>
	</body>
</html>
