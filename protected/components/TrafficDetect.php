<?php

/**
 * http://domain.com/?utm_source=campain&utm_medium=medium&utm_term=term&utm_content=content&utm_campaign=name&ad=ad&adid=adid&keyword=keyword
 * http://domain.com/?vk_sid=sid&vk_lead_id=lead_id&vk_uid=uid&vk_hash=hash
 */ 

class TrafficDetect
{
	const COOKIES_VAR = 'trafic';
	
	protected static $utm_vars = array(
		'utm_source',
		'utm_medium',
		'utm_term',
		'utm_content',
		'utm_campaign',
		'ad',
		'adid',
		'keyword',
	);
	
	protected static $vk_offer_vars = array(
		'vk_sid',
		'vk_lead_id',
		'vk_uid',
		'vk_hash',
	);
	
	public static function get($remember=true, &$is_from_cookie=false)
	{
		$result = self::getUtm();
		$result = array_merge($result, self::getVkOffer());
		$result = array_merge($result, self::getSearch());
		$result = array_merge($result, self::getReferef());
		
		if (empty($result)) {
			$cookies = Yii::app()->request->cookies;
			if (isset($cookies[self::COOKIES_VAR])) {
				$eval = $cookies[self::COOKIES_VAR]->value;
				$dval = base64_decode($eval);
				parse_str($dval, $result);
				$is_from_cookie = true;
				return $result;
			}
			return false;
		}
		
		if ($remember) {
			$cookies = Yii::app()->request->cookies;
			$eval = base64_encode(http_build_query($result));
			$cookie = new CHttpCookie(self::COOKIES_VAR, $eval);
			$cookie->expire = time() + 2592000; // 60*60*24*30 = 1 month
			$cookies->add($cookie->name, $cookie);
		}
		
		return $result;
	}
	
	public static function getUtm()
	{
		$result = array();
		$request = Yii::app()->request;
		foreach (self::$utm_vars as $var) {
			$qvar = $request->getQuery($var);
			if (null !== $qvar) {
				$result[$var] = $qvar;
			}
		}
		return $result;
	}
	
	public static function getVkOffer()
	{
		$result = array();
		$request = Yii::app()->request;
		foreach (self::$vk_offer_vars as $var) {
			$qvar = $request->getQuery($var);
			if (null !== $qvar) {
				$result[$var] = $qvar;
			}
		}
		return $result;
	}
	
	public static function getSearch()
	{
		if (! empty($_SERVER['HTTP_REFERRER'])) {
			
			$ref = $_SERVER['HTTP_REFERRER'];
			
			if ($res = self::checkGoogleReferrer($ref)) {
				return $res;
			}
			
			if ($res = self::checkYandexReferrer($ref)) {
				return $res;
			}
			
			if ($res = self::checkYahooReferrer($ref)) {
				return $res;
			}
			
			if ($res = self::checkBingReferrer($ref)) {
				return $res;
			}
		}
		
		return array();
	}
	
	protected static function checkGoogleReferrer($ref)
	{
		if (self::checkDomain($ref, 'google')) {
			return array(
				'previuos' => $ref,
				'search_query' => self::getQueryParam($ref, 'q'),
				'search_engine' => 'google',
			);
		}
		return false;
	}
	
	protected static function checkYandexReferrer($ref)
	{
		if (self::checkDomain($ref, 'yandex')) {
			return array(
				'previuos' => $ref,
				'search_query' => self::getQueryParam($ref, 'text'),
				'search_engine' => 'yandex',
			);
		}
		return false;
	}
	
	protected static function checkYahooReferrer($ref)
	{
		if (self::checkDomain($ref, 'yahoo')) {
			return array(
				'previuos' => $ref,
				'search_query' => self::getQueryParam($ref, 'p'),
				'search_engine' => 'yahoo',
			);
		}
		return false;
	}
	
	protected static function checkBingReferrer($ref)
	{
		if (self::checkDomain($ref, 'bing')) {
			return array(
				'previuos' => $ref,
				'search_query' => self::getQueryParam($ref, 'q'),
				'search_engine' => 'bing',
			);
		}
		return false;
	}
	
	protected static function checkDomain($url, $domain)
	{
		return (strpos(parse_url($url, PHP_URL_HOST), $domain) !== false);
	}
	
	protected static function getQueryParam($url, $param)
	{
		parse_str(parse_url($url, PHP_URL_QUERY), $result);
		return isset($result[$param]) ? $result[$param] : '';
	}
	
	protected static function getReferef()
	{
		$result = array();
		if (! empty($_SERVER['HTTP_REFERRER'])) {
			$refhost = parse_url($_SERVER['HTTP_REFERRER'], PHP_URL_HOST);
			$thishost = $_SERVER['HTTP_HOST'];
			if ($refhost != $thishost) {
				$result['referrer'] = $_SERVER['HTTP_REFERRER'];
			}
		}
		return $result;
	}
}
