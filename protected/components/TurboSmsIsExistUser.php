<?php

class TurboSmsIsExistUser extends CValidator
{
    public $authArray = array();
    protected function validateAttribute($object, $attribute) {
        $soapClient = new SoapClient('http://turbosms.in.ua/api/wsdl.html');
        $result =  $soapClient->Auth();
        if($result!=='Вы успешно авторизировались'){
            $this->addError($object, $attribute, $result);
        }
    }
}
