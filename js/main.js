$(document).ready(function(){
    $('.scroll').click(function(){
        var idscroll = $(this).attr('href');
        $.scrollTo(idscroll, 1000);
        return false;
    });
    
    $('.popup-with-form').magnificPopup({
         type:'inline',
         preloader:false,
         modal: true
     });
});


function submitFormAjax(form, data, hasErrors) {
    if (!hasErrors) {
        var b = form.find('[type=submit]');
        b.attr('disabled', 'disabled');
        
        if (b.is('.shortformbtn')) {
			b.html('Отправка...');
			$.post(form.get(0).action, form.serialize(), function(r) {
				if (r.status == 'ok') {
					$.fancybox.open({
						type : 'ajax', 
						href : '/site/complete?id=' + r.request_id,
						afterShow : function () {
							
							jQuery("#formcallbackcomplete_phone").mask("+7 999 999 9999");
							jQuery('#formcallbackcomplete').yiiactiveform({'validateOnSubmit':true,'afterValidate':submitFormAjax,'attributes':[{'id':'Feedback_name','inputID':'formcallbackcomplete_name','errorID':'formcallbackcomplete_name_em_','model':'Feedback','name':'name','enableAjaxValidation':false,'clientValidation':function(value, messages, attribute) {

							if(jQuery.trim(value)!='') {
								
							if(value.length>200) {
								messages.push("\u0424\u0418\u041e \u0441\u043b\u0438\u0448\u043a\u043e\u043c \u0434\u043b\u0438\u043d\u043d\u044b\u0439 (\u041c\u0430\u043a\u0441\u0438\u043c\u0443\u043c: 200 \u0441\u0438\u043c\u0432.).");
							}

							}


							if(jQuery.trim(value)=='') {
								messages.push("\u041d\u0435\u043e\u0431\u0445\u043e\u0434\u0438\u043c\u043e \u0437\u0430\u043f\u043e\u043b\u043d\u0438\u0442\u044c \u043f\u043e\u043b\u0435 \u00ab\u0424\u0418\u041e\u00bb.");
							}

							}},{'id':'Feedback_phone','inputID':'formcallbackcomplete_phone','errorID':'formcallbackcomplete_phone_em_','model':'Feedback','name':'phone','enableAjaxValidation':false,'clientValidation':function(value, messages, attribute) {

							if(jQuery.trim(value)!='') {
								
							if(value.length>200) {
								messages.push("\u041d\u043e\u043c\u0435\u0440 \u0442\u0435\u043b\u0435\u0444\u043e\u043d\u0430 \u0441\u043b\u0438\u0448\u043a\u043e\u043c \u0434\u043b\u0438\u043d\u043d\u044b\u0439 (\u041c\u0430\u043a\u0441\u0438\u043c\u0443\u043c: 200 \u0441\u0438\u043c\u0432.).");
							}

							}


							if(jQuery.trim(value)=='') {
								messages.push("\u041d\u0435\u043e\u0431\u0445\u043e\u0434\u0438\u043c\u043e \u0437\u0430\u043f\u043e\u043b\u043d\u0438\u0442\u044c \u043f\u043e\u043b\u0435 \u00ab\u041d\u043e\u043c\u0435\u0440 \u0442\u0435\u043b\u0435\u0444\u043e\u043d\u0430\u00bb.");
							}

							}},{'id':'Feedback_email','inputID':'formcallbackcomplete_email','errorID':'formcallbackcomplete_email_em_','model':'Feedback','name':'email','enableAjaxValidation':false,'clientValidation':function(value, messages, attribute) {

							if(jQuery.trim(value)=='') {
								messages.push("\u041d\u0435\u043e\u0431\u0445\u043e\u0434\u0438\u043c\u043e \u0437\u0430\u043f\u043e\u043b\u043d\u0438\u0442\u044c \u043f\u043e\u043b\u0435 \u00abEmail\u00bb.");
							}


							if(jQuery.trim(value)!='') {
								
							if(value.length>100) {
								messages.push("Email \u0441\u043b\u0438\u0448\u043a\u043e\u043c \u0434\u043b\u0438\u043d\u043d\u044b\u0439 (\u041c\u0430\u043a\u0441\u0438\u043c\u0443\u043c: 100 \u0441\u0438\u043c\u0432.).");
							}

							}



							if(jQuery.trim(value)!='' && !value.match(/^[a-zA-Z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/)) {
								messages.push("Email \u043d\u0435 \u044f\u0432\u043b\u044f\u0435\u0442\u0441\u044f \u043f\u0440\u0430\u0432\u0438\u043b\u044c\u043d\u044b\u043c E-Mail \u0430\u0434\u0440\u0435\u0441\u043e\u043c.");
							}

							}}],'errorCss':'error'});
						}
					});
				} else {
					b.html('Ошибка :(');
				}
				setTimeout(function() {
					b.removeAttr('disabled');
					b.html('Заказать <span>стильную</span> рамку');
				}, 3000);
			}, 'json');
			
		} else {
			
			ga('send', 'event', 'Form', 'Sent');
			yaCounter25575926.reachGoal('SentForm');
        
			b.val('Отправка...');
			$.post(form.get(0).action, form.serialize(), function(r) {
				if (r.status == 'ok') {
					if ('redirect' in r) {
						location.href = r.redirect;
					} else {
						form.find('input[type=text]').val('');
						b.val('Отправлено');
						form.hide();
						$(r.message).insertAfter(form);
					}
				} else {
					b.val('Ошибка :(');
				}
				setTimeout(function() {
					b.removeAttr('disabled');
					b.val('Перезвоните мне');
				}, 3000);
			}, 'json');
			
		}
		
        return false;
    }
    return true;
}

function simpleSlider(selector) {
	$(selector).each(function() {
		var i = $(this).find('.slider-item');
		var w = i.eq(0).width();
		var vp = $(this).find('.slider-view-port');
		var maxoffset = (i.length * w) - vp.width();
		var offset = 0;
		var navp = $(this).find('.slider-prev');
		var navn = $(this).find('.slider-next');
		if (maxoffset <= 0) {
			navp.hide();
			navn.hide();
		} else {
			navp.hide();
			navp.click(function() {
				if (offset > 0) {
					offset = Math.max(0, offset - w);
					navn.show();
					if (offset == 0) {
						navp.hide();
					}
					vp.animate({
						'scrollLeft': offset
					});
				}
				return false;
			});
			navn.click(function() {
				if (offset < maxoffset) {
					offset = Math.min(maxoffset, offset + w);
					navp.show();
					if (offset == maxoffset) {
						navn.hide();
					}
					vp.animate({
						'scrollLeft': offset
					});
				}
				return false;
			});
		}
	});
}
