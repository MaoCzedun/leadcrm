$(document).ready(function(){
    var formOptions = {
            productId: 0,
            upsaleUrl: '',
            isFieldName: true,
            isFieldPhone: true,
            isFieldEmail: false,
            isFieldAddress: false,
            isInnerText: false,
            shadowTypeCssClass: 'boxNoneShadow',
            textureType: 'ffffff',
            textFieldWidth: '500px',
            textFieldBorderRadius: '0px',
            textFieldBackgroundColor: '#ffffff',
            textFieldColor: '#000',
            textFieldLabelColor: '#000',
            buttonWidth: '100px',
            buttonHeight: '30px',
            buttonBorderRadius: '0px',
            buttonText: 'Заказать',
            buttonTextColor: '#fff',
            buttonBackgroundColor: '#f21b1b',
            buttonMarginTop: '20px',
            buttonMarginRight: '20px',
            buttonMarginBottom: '20px',
            buttonMarginLeft: '20px',
            blockBorderRadius: '0px',
            blockBackgroundColor: '#ffffff',
            blockPaddingTop: '20px',
            blockPaddingRight: '20px',
            blockPaddingBottom: '20px',
            blockPaddingLeft: '20px',
            blockOpacity: '1'
        };

        $(function () {
            updateForm(formOptions);
            
            $('.part h3').click(function () {
                $(this).next('.innerBlock').slideToggle('slow');
            });
            
            $("#blockBorderRadius").slider({
                range: "min",
                value: 0,
                min: 0,
                max: 50,
                slide: function (event, ui) {
                    $("#blockBorderRadiusText").text(ui.value + " px");
                    formOptions.blockBorderRadius = ui.value + 'px';
                    updateForm(formOptions);
                }
            });

            $("#textFieldWidth").slider({
                range: "min",
                value: 0,
                min: 200,
                max: 500,
                slide: function (event, ui) {
                    $("#textFieldWidthText").text(ui.value + " px");
                    formOptions.textFieldWidth = ui.value + 'px';
                    updateForm(formOptions);
                }
            });

            $("#textFieldBorderRadius").slider({
                range: "min",
                value: 0,
                min: 0,
                max: 15,
                slide: function (event, ui) {
                    $("#textFieldBorderRadiusText").text(ui.value + " px");
                    formOptions.textFieldBorderRadius = ui.value + 'px';
                    updateForm(formOptions);
                }
            });
            
            $("#buttonWidth").slider({
                range: "min",
                value: 100,
                min: 50,
                max: 500,
                slide: function (event, ui) {
                    $("#buttonWidthText").text(ui.value + " px");
                    formOptions.buttonWidth = ui.value + 'px';
                    updateForm(formOptions);
                }
            });
            
            $("#buttonHeight").slider({
                range: "min",
                value: 30,
                min: 0,
                max: 100,
                slide: function (event, ui) {
                    $("#buttonHeightText").text(ui.value + " px");
                    formOptions.buttonHeight = ui.value + 'px';
                    updateForm(formOptions);
                }
            });
            
            $("#blockOpacity").slider({
                range: "min",
                value: 100,
                min: 0,
                max: 100,
                step: 10,
                slide: function (event, ui) {
                    $("#blockOpacityText").text(ui.value / 100);
                    formOptions.blockOpacity = ui.value / 100;
                    var rgbColor = hexToRgb('#' + $('#blockBackgroundColor').val());
                    formOptions.blockBackgroundColor = 'rgba(' + rgbColor.r + ', ' + rgbColor.g + ', '+ rgbColor.b + ', ' + formOptions.blockOpacity + ')';
                    updateForm(formOptions);
                }
            });
            
            $("#buttonBorderRadius").slider({
                range: "min",
                value: 0,
                min: 0,
                max: 15,
                slide: function (event, ui) {
                    $("#buttonBorderRadiusText").text(ui.value + "px");
                    formOptions.buttonBorderRadius = ui.value + "px";
                    updateForm(formOptions);
                }
            });
            
            $("#orderButtonText").keyup(function(){
                formOptions.buttonText = $(this).val();
                updateForm(formOptions);
            });
            
            $("#buttonMarginTop").change(function(){
                formOptions.buttonMarginTop = $(this).val() + 'px';
                updateForm(formOptions);
            });
            
            $("#buttonMarginRight").change(function(){
                formOptions.buttonMarginRight = $(this).val() + 'px';
                updateForm(formOptions);
            });
            
            $("#buttonMarginBottom").change(function(){
                formOptions.buttonMarginBottom = $(this).val() + 'px';
                updateForm(formOptions);
            });
            
            $("#buttonMarginLeft").change(function(){
                formOptions.buttonMarginLeft = $(this).val() + 'px';
                updateForm(formOptions);
            });
            
            $('#optionFiledEmailCheckbox').change(function() {
                formOptions.isFieldEmail = $(this).prop("checked");
                updateForm(formOptions);
            });
            
            $("#blockPaddingTop").change(function(){
                formOptions.blockPaddingTop = $(this).val() + 'px';
                updateForm(formOptions);
            });
            
            $("#blockPaddingRight").change(function(){
                formOptions.blockPaddingRight = $(this).val() + 'px';
                updateForm(formOptions);
            });
            
            $("#blockPaddingBottom").change(function(){
                formOptions.blockPaddingBottom = $(this).val() + 'px';
                updateForm(formOptions);
            });
            
            $("#blockPaddingLeft").change(function(){
                formOptions.blockPaddingLeft = $(this).val() + 'px';
                updateForm(formOptions);
            });
            
            $('#optionFiledEmailCheckbox').change(function() {
                formOptions.isFieldEmail = $(this).prop("checked");
                updateForm(formOptions);
            });
            
            $('#optionFiledAddressCheckbox').change(function() {
                formOptions.isFieldAddress = $(this).prop("checked");
                updateForm(formOptions);
            });
            
            $('.picker').colpick({
                layout:'hex',
                submit:0,
                colorScheme:'dark',
                onChange:function(hsb,hex,rgb,el,bySetColor) {
                    $(el).css('border-color','#'+hex);
                    if(!bySetColor) $(el).val(hex);
                    
                    if($(el).is("#buttonBackgroundColor")) {
                        formOptions.buttonBackgroundColor = '#' + hex;
                        updateForm(formOptions);
                    }
                    
                    if($(el).is("#blockBackgroundColor")) {
                        var rgbColor = hexToRgb('#' + hex);
                        formOptions.blockOpacity = $('#blockOpacityText').text();
                        formOptions.blockBackgroundColor = 'rgba(' + rgbColor.r + ', ' + rgbColor.g + ', '+ rgbColor.b + ', ' + formOptions.blockOpacity + ')';
                        updateForm(formOptions);
                    }
                    
                    if($(el).is("#textFieldBackgroundColor")) {
                        formOptions.textFieldBackgroundColor = '#' + hex;
                        updateForm(formOptions);
                    }
                    
                    if($(el).is("#textFieldColor")) {
                        formOptions.textFieldColor = '#' + hex;
                        updateForm(formOptions);
                    }
                    
                    if($(el).is("#textFieldLabelColor")) {
                        formOptions.textFieldLabelColor = '#' + hex;
                        updateForm(formOptions);
                    }
                    
                    if($(el).is("#buttonTextColor")) {
                        formOptions.buttonTextColor = '#' + hex;
                        console.log(formOptions.buttonTextColor);
                        updateForm(formOptions);
                    }
                    
                    
                }
            }).keyup(function(){
                $(this).colpickSetColor(this.value);
            });
            
            
        });

        function updateForm(options) {
            if(typeof options !== 'undefined'){
                
                if(typeof options.blockPaddingTop === 'undefined' || options.blockPaddingTop === '') options.blockPaddingTop = '0px';
                if(typeof options.blockPaddingRight === 'undefined' || options.blockPaddingRight === '') options.blockPaddingRight = '0px';
                if(typeof options.blockPaddingBottom === 'undefined' || options.blockPaddingBottom === '') options.blockPaddingBottom = '0px';
                if(typeof options.blockPaddingLeft === 'undefined' || options.blockPaddingLeft === '') options.blockPaddingLeft = '0px';
                
                var templateFn = doT.template($('#formTemplate').html());
                var formTemplateResult = templateFn(options);

                $('#formResult').html(formTemplateResult);
                 var forCreate = $('#formResult').html();
                 $('#Form_code').val("'"+forCreate+"'");
                 $('#codeForm').val("'"+forCreate+"'");
//                console.log(formTemplateResult);
                console.log($('#Form_code').val());
            }
        }
    
        function hexToRgb(hex) {
            var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
            return result ? {
                r: parseInt(result[1], 16),
                g: parseInt(result[2], 16),
                b: parseInt(result[3], 16)
            } : null;
        }
			
});
