function submitFormAjax(form, data, hasErrors) {
    if (!hasErrors) {
        var b = form.find('input[type=submit]');
        b.attr('disabled', 'disabled');
        b.val('Отправка...');
        $.post(form.get(0).action, form.serialize(), function(r) {
            if (r.status == 'ok') {
                b.val('Отправлено!');
                form.get(0).reset();
            } else {
                b.val('Ошибка :(');
            }
            setTimeout(function() {
				b.removeAttr('disabled');
				b.val('Отправить');
			}, 3000);
        }, 'json');
        return false;
    }
    return true;
}
