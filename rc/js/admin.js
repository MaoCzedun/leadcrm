$(function() {

    $.fn.tooltip = $.fn.tooltip.noConflict();
    $(document.body).tooltip({
        selector: "[data-toggle=tooltip]",
        placement: 'bottom'
    });

    $(document.body).on('click', '[data-toggle=search-form]', function() {
        $('.search-form').toggle();
        $('.export-form').hide();
        return false;
    });

    $(document.body).on('click', '[data-toggle=export-form]', function() {
        if ($('.export-form').is(':hidden')) {
            var target = $('[role=search-form]').attr('data-target');
            $.fn.yiiGridView.update(target, {
                url: location.href
            });
        }
        $('.export-form').toggle();
        $('.search-form').hide();
        return false;
    });

    $(document.body).on('submit', '[role=search-form]', function() {
        var target = $(this).attr('data-target');
        $.fn.yiiGridView.update(target, {
            data: $(this).serialize()
        });
        return false;
    });



});

function editable(){
    $('.edit2').each(function() {
        var $this = $(this);
        $this.editable(submitEdit, {
            cssclass  : 'editable'

        });
    });

    $('.color').each(function() {
        var $this = $(this);
        $this.editable(submitEdit, {
            cssclass  : 'editable'

        });
    });
}

function submitEdit(value, settings){
    //var url="<?php echo $this->createUrl('/feedbackStatus/update');?>";

    var id = $(this).prev('td').text();
    console.log(id);
    var url="update?id=" + id;
    var fieldName = $(this).attr('data-name');
    console.log(fieldName);
    var result = value;
    var returned = $.ajax({
        url: url,
        type: "POST",
        data : {id : id, field: fieldName, value : result },
        dataType : "json",
        success : function (e)
        {
            if (e.status=="success"){
                notification(e.message);
            } else {
                notification(e.message);
            }
        }
    });
    return(result);
}
$(document).ready(function() {
    editable();
});



