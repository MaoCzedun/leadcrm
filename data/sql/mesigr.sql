-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Фев 18 2015 г., 13:57
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `mesigr`
--

-- --------------------------------------------------------

--
-- Структура таблицы `client`
--

CREATE TABLE IF NOT EXISTS `client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `create_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `course`
--

CREATE TABLE IF NOT EXISTS `course` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Name',
  PRIMARY KEY (`id`),
  KEY `idx_name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `course`
--

INSERT INTO `course` (`id`, `name`) VALUES
(1, 'Менеджмент'),
(2, 'Прикладная информатика'),
(3, 'Экономика'),
(4, 'Бизнес-информатика'),
(5, 'Юриспруденция');

-- --------------------------------------------------------

--
-- Структура таблицы `feedback`
--

CREATE TABLE IF NOT EXISTS `feedback` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comment` text COLLATE utf8_unicode_ci COMMENT 'Comment',
  `course_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Course, many-to-one relation, foreign key refers to Course',
  `create_time` datetime DEFAULT NULL COMMENT 'Time Created',
  `manager_id` int(10) unsigned DEFAULT NULL COMMENT 'Manager, many-to-one relation, foreign key refers to User',
  `state_id` int(11) NOT NULL,
  `update_time` datetime DEFAULT NULL COMMENT 'Time Updated',
  `utm_content` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `utm_campaign` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `utm_medium` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `utm_source` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `utm_term` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_course_id` (`course_id`),
  KEY `idx_manager_id` (`manager_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=83 ;

--
-- Дамп данных таблицы `feedback`
--

INSERT INTO `feedback` (`id`, `status_id`, `user_id`, `comment`, `course_id`, `create_time`, `manager_id`, `state_id`, `update_time`, `utm_content`, `utm_campaign`, `utm_medium`, `utm_source`, `utm_term`) VALUES
(3, 1, 1, '', 2, '2014-06-17 16:42:18', 1, 0, '2014-06-20 16:50:29', NULL, NULL, NULL, NULL, NULL),
(4, 1, 1, '', 3, '2014-06-17 16:49:32', 1, 0, '2014-06-19 14:52:57', 'content', 'name', 'medium', 'campain', 'term'),
(5, 1, 1, '', 1, '2014-06-18 18:57:25', 1, 0, '2014-06-19 14:33:36', 'content', 'name', 'medium', 'campain', 'term'),
(6, 1, 1, NULL, 3, '2014-06-19 14:48:54', NULL, 0, NULL, 'content', 'name', 'medium', 'campain', 'term'),
(7, 1, 1, NULL, 3, '2014-06-19 14:49:54', NULL, 0, NULL, 'content', 'name', 'medium', 'campain', 'term'),
(8, 1, 1, NULL, 2, '2014-06-19 14:50:29', NULL, 0, NULL, 'content', 'name', 'medium', 'campain', 'term'),
(9, 2, 1, NULL, 4, '2014-06-19 14:50:42', NULL, 0, NULL, 'content', 'name', 'medium', 'campain', 'term'),
(10, 2, 1, NULL, 4, '2014-06-19 14:51:15', NULL, 0, NULL, 'content', 'name', 'medium', 'campain', 'term'),
(11, 2, 1, NULL, 2, '2014-06-19 14:51:28', NULL, 0, NULL, 'content', 'name', 'medium', 'campain', 'term'),
(12, 2, 1, NULL, 5, '2014-06-19 14:51:48', NULL, 0, NULL, 'content', 'name', 'medium', 'campain', 'term'),
(13, 2, 1, NULL, 3, '2014-06-19 14:52:02', NULL, 0, NULL, 'content', 'name', 'medium', 'campain', 'term'),
(14, 3, 1, '', 5, '2014-06-19 14:52:15', 1, 0, '2014-06-19 17:19:45', 'content', 'name', 'medium', 'campain', 'term'),
(15, 3, 1, '', 5, '2014-06-19 15:18:02', 1, 0, '2014-06-19 17:19:29', 'content', 'name', 'medium', 'campain', 'term'),
(16, 3, 1, '', 2, '2014-06-19 15:18:27', 1, 0, '2014-06-19 17:19:21', 'content', 'name', 'medium', 'campain', 'term'),
(17, 1, 1, 'сказал туфта, а не товар', 3, '2014-06-19 15:18:41', 1, 0, '2015-01-24 13:03:41', 'content', 'name', 'medium', 'campain', 'term'),
(18, 0, 1, NULL, 2, '2014-06-20 14:35:12', NULL, 0, NULL, 'content', 'name', 'medium', 'campain', 'term'),
(19, 0, 1, NULL, 2, '2014-06-20 15:27:09', NULL, 0, NULL, 'content', 'name', 'medium', 'campain', 'term'),
(20, 0, 1, NULL, 3, '2014-06-20 15:27:50', NULL, 0, NULL, 'content', 'name', 'medium', 'campain', 'term'),
(21, 0, 1, NULL, 5, '2014-06-20 15:28:38', NULL, 0, NULL, 'content', 'name', 'medium', 'campain', 'term'),
(22, 0, 1, NULL, 4, '2014-06-20 15:29:51', NULL, 0, NULL, 'content', 'name', 'medium', 'campain', 'term'),
(23, 0, 1, 'asd', 3, '2014-06-20 15:36:25', 2, 0, '2014-06-26 15:20:15', 'content', 'name', 'medium', 'campain', 'term'),
(24, 0, 1, NULL, 2, '2014-06-20 15:38:04', NULL, 0, NULL, 'content', 'name', 'medium', 'campain', 'term'),
(25, 0, 1, NULL, 2, '2014-06-20 15:39:05', NULL, 0, NULL, 'content', 'name', 'medium', 'campain', 'term'),
(26, 0, 1, NULL, 3, '2014-06-20 15:39:42', NULL, 0, NULL, 'content', 'name', 'medium', 'campain', 'term'),
(27, 0, 1, NULL, 3, '2014-06-20 15:42:46', NULL, 0, NULL, 'content', 'name', 'medium', 'campain', 'term'),
(28, 0, 1, NULL, 4, '2014-06-20 15:52:44', NULL, 0, NULL, 'content', 'name', 'medium', 'campain', 'term'),
(29, 0, 1, NULL, 2, '2014-06-20 15:53:45', NULL, 0, NULL, 'content', 'name', 'medium', 'campain', 'term'),
(30, 0, 1, NULL, 2, '2014-06-20 15:54:24', NULL, 0, NULL, 'content', 'name', 'medium', 'campain', 'term'),
(31, 0, 1, '', 2, '2014-06-20 15:57:07', 1, 0, '2014-06-20 16:55:23', 'content', 'name', 'medium', 'campain', 'term'),
(32, 0, 1, NULL, 3, '2014-06-23 18:27:41', NULL, 0, NULL, 'content', 'name', 'medium', 'campain', 'term'),
(33, 0, 0, 'Лалала!', 2, '2014-06-23 18:28:32', 1, 0, '2014-06-26 15:05:59', 'content', 'name', 'medium', 'campain', 'term'),
(34, 0, 0, NULL, 2, '2014-06-23 18:30:32', NULL, 0, NULL, 'content', 'name', 'medium', 'campain', 'term'),
(36, 0, 0, NULL, 3, '2014-06-26 16:03:58', NULL, 0, NULL, 'content', 'name', 'medium', 'campain', 'term'),
(37, 0, 0, NULL, 5, '2014-06-26 16:05:41', NULL, 0, NULL, 'content', 'name', 'medium', 'campain', 'term'),
(38, 0, 0, NULL, 2, '2014-06-26 16:06:21', NULL, 0, NULL, 'content', 'name', 'medium', 'campain', 'term'),
(39, 0, 0, NULL, 2, '2014-06-26 16:08:13', NULL, 0, NULL, 'content', 'name', 'medium', 'campain', 'term'),
(40, 0, 0, NULL, 2, '2014-06-26 16:09:36', NULL, 0, NULL, 'content', 'name', 'medium', 'campain', 'term'),
(41, 0, 0, NULL, 1, '2014-07-03 10:12:09', NULL, 0, NULL, 'content', 'name', 'medium', 'campain', 'term'),
(42, 0, 0, NULL, 2, '2014-07-03 14:51:26', NULL, 0, NULL, 'content', 'name', 'medium', 'campain', 'term'),
(43, 0, 0, NULL, 3, '2014-07-03 15:23:10', NULL, 0, NULL, 'content', 'name', 'medium', 'campain', 'term'),
(44, 0, 0, NULL, 4, '2014-07-03 16:01:04', NULL, 0, NULL, 'content', 'name', 'medium', 'campain', 'term'),
(45, 0, 0, NULL, 3, '2014-07-03 16:12:18', NULL, 0, NULL, 'content', 'name', 'medium', 'campain', 'term'),
(46, 0, 0, NULL, 2, '2014-07-04 15:15:57', NULL, 0, NULL, 'content', 'name', 'medium', 'campain', 'term'),
(47, 0, 0, NULL, 0, '2014-07-08 17:33:42', NULL, 0, NULL, 'content', 'name', 'medium', 'campain', 'term'),
(48, 0, 0, NULL, 2, '2014-07-08 17:35:56', NULL, 0, NULL, 'content', 'name', 'medium', 'campain', 'term'),
(49, 0, 0, NULL, 0, '2014-07-17 16:31:44', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(50, 0, 0, NULL, 0, '2014-07-17 16:32:01', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(51, 0, 0, NULL, 0, '2014-07-17 16:32:35', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(52, 0, 0, NULL, 0, '2014-08-15 13:24:44', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(53, 0, 0, NULL, 0, '2014-08-15 13:25:01', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(54, 0, 0, NULL, 0, '2014-08-15 13:25:43', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(55, 0, 0, NULL, 0, '2014-08-15 14:40:47', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(56, 0, 0, NULL, 0, '2014-08-15 14:45:19', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(57, 0, 0, NULL, 0, '2014-08-15 14:45:36', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(58, 0, 0, NULL, 0, '2014-08-15 14:46:26', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(59, 0, 0, NULL, 0, '2014-08-15 14:46:44', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(60, 0, 0, NULL, 0, '2014-08-15 14:47:49', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(61, 0, 0, NULL, 0, '2014-08-15 14:48:06', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(62, 0, 0, NULL, 0, '2014-08-15 14:48:23', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(63, 0, 0, NULL, 0, '2014-08-15 14:48:33', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(64, 0, 0, NULL, 0, '2014-08-15 14:49:13', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(65, 0, 0, NULL, 0, '2014-08-15 14:49:24', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(66, 0, 0, NULL, 0, '2014-08-15 14:50:18', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(67, 0, 0, NULL, 0, '2014-08-15 14:54:05', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(68, 0, 0, NULL, 0, '2014-08-15 14:54:28', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(69, 0, 0, NULL, 0, '2014-08-15 14:54:44', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(70, 0, 0, NULL, 0, '2014-08-15 14:57:46', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(71, 0, 0, NULL, 0, '2014-08-15 15:40:41', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(72, 0, 0, NULL, 0, '2014-08-15 15:40:52', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(73, 0, 0, NULL, 0, '2014-08-15 15:41:34', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(74, 0, 0, NULL, 0, '2014-08-15 15:50:08', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(75, 0, 0, NULL, 0, '2014-08-15 19:02:06', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(76, 0, 0, NULL, 0, '2014-08-15 19:36:38', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(77, 0, 0, NULL, 0, '2014-08-18 15:36:14', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(78, 0, 0, NULL, 0, '2014-08-18 15:54:37', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(79, 0, 0, NULL, 0, '2014-08-18 15:59:39', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(80, 1, 0, '', 0, '2014-08-18 16:02:40', NULL, 0, '2015-01-27 18:02:40', NULL, NULL, NULL, NULL, NULL),
(81, 0, 0, '', 0, '2014-08-18 16:03:41', 1, 0, '2015-01-20 14:19:00', NULL, NULL, NULL, NULL, NULL),
(82, 1, 0, '', 0, '2014-08-18 16:06:15', NULL, 0, '2015-01-27 18:02:13', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `feedback_form`
--

CREATE TABLE IF NOT EXISTS `feedback_form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `upsale_url` varchar(200) NOT NULL,
  `code` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `feedback_status`
--

CREATE TABLE IF NOT EXISTS `feedback_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `color` varchar(10) NOT NULL,
  `user_id` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Дамп данных таблицы `feedback_status`
--

INSERT INTO `feedback_status` (`id`, `name`, `color`, `user_id`) VALUES
(0, 'Без статуса', 'ffffff', 0),
(1, 'Новый заказ', 'd4f09c', 0),
(2, 'Подтвержденный заказ', 'adf041', 0),
(3, 'Отмененный заказ', 'F75C54', 0),
(4, 'Возврат товара', 'F79E54', 0),
(5, 'Недозвон (не взяли трубку)', 'F754D9', 0),
(6, 'Ждем оплату', '80DB5E', 0),
(7, 'Недозвон (абонент недоступен)', '56b3f5', 0),
(8, 'Недозвон (номер не существует)', '000000', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `feedback_status_log`
--

CREATE TABLE IF NOT EXISTS `feedback_status_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `feedback_id` int(10) unsigned NOT NULL,
  `status` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `time` datetime NOT NULL,
  `manager_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=102 ;

--
-- Дамп данных таблицы `feedback_status_log`
--

INSERT INTO `feedback_status_log` (`id`, `feedback_id`, `status`, `comment`, `time`, `manager_id`) VALUES
(2, 3, 'новая', NULL, '2014-06-17 16:42:18', 1),
(3, 3, 'test', NULL, '2014-06-17 16:44:29', 1),
(4, 4, 'новая', NULL, '2014-06-17 16:49:32', 1),
(5, 4, 'новая', NULL, '2014-06-17 17:09:15', 1),
(6, 4, 'новая', NULL, '2014-06-17 17:09:56', 1),
(7, 5, 'новая', NULL, '2014-06-18 18:57:25', 1),
(8, 5, 'Подписание договора', 'Там там тарарарарам таратарам!', '2014-06-19 14:31:40', 1),
(9, 5, 'Подписание договора', '', '2014-06-19 14:33:36', 1),
(10, 6, 'новая', NULL, '2014-06-19 14:48:54', 1),
(11, 7, 'новая', NULL, '2014-06-19 14:49:54', 1),
(12, 8, 'новая', NULL, '2014-06-19 14:50:29', 1),
(13, 9, 'новая', NULL, '2014-06-19 14:50:42', 1),
(14, 10, 'новая', NULL, '2014-06-19 14:51:15', 1),
(15, 11, 'новая', NULL, '2014-06-19 14:51:28', 1),
(16, 12, 'новая', NULL, '2014-06-19 14:51:48', 1),
(17, 13, 'новая', NULL, '2014-06-19 14:52:02', 1),
(18, 14, 'новая', NULL, '2014-06-19 14:52:15', 1),
(19, 4, '', '', '2014-06-19 14:52:57', 1),
(20, 15, 'новая', NULL, '2014-06-19 15:18:02', 1),
(21, 16, 'новая', NULL, '2014-06-19 15:18:27', 1),
(22, 17, 'новая', NULL, '2014-06-19 15:18:41', 1),
(23, 17, 'Думает', '', '2014-06-19 17:19:13', 1),
(24, 16, 'Звонок - напоминание об анкете', '', '2014-06-19 17:19:21', 1),
(25, 15, 'ФИО не соответсвует заявке', '', '2014-06-19 17:19:29', 1),
(26, 14, 'Абонент не доступен', '', '2014-06-19 17:19:45', 1),
(27, 18, 'Новая заявка', NULL, '2014-06-20 14:35:12', 1),
(28, 19, 'Новая заявка', NULL, '2014-06-20 15:27:09', 1),
(29, 20, 'Новая заявка', NULL, '2014-06-20 15:27:50', 1),
(30, 21, 'Новая заявка', NULL, '2014-06-20 15:28:38', 1),
(31, 22, 'Новая заявка', NULL, '2014-06-20 15:29:51', 1),
(32, 23, 'Новая заявка', NULL, '2014-06-20 15:36:25', 1),
(33, 24, 'Новая заявка', NULL, '2014-06-20 15:38:04', 1),
(34, 25, 'Новая заявка', NULL, '2014-06-20 15:39:05', 1),
(35, 26, 'Новая заявка', NULL, '2014-06-20 15:39:42', 1),
(36, 27, 'Новая заявка', NULL, '2014-06-20 15:42:46', 1),
(37, 28, 'Новая заявка', NULL, '2014-06-20 15:52:44', 1),
(38, 29, 'Новая заявка', NULL, '2014-06-20 15:53:45', 1),
(39, 30, 'Новая заявка', NULL, '2014-06-20 15:54:24', 1),
(40, 31, 'Новая заявка', NULL, '2014-06-20 15:57:07', 1),
(41, 3, 'Подписание договора', '', '2014-06-20 16:50:29', 1),
(42, 31, 'Дубляж', '', '2014-06-20 16:55:23', 1),
(43, 32, 'Новая заявка', NULL, '2014-06-23 18:27:41', 1),
(44, 33, 'Новая заявка', NULL, '2014-06-23 18:28:32', 1),
(45, 34, 'Новая заявка', NULL, '2014-06-23 18:30:34', 1),
(47, 33, 'Новая заявка', '', '2014-06-26 15:03:18', 1),
(48, 33, 'Новая заявка', 'Лалала!', '2014-06-26 15:05:59', 1),
(49, 23, 'Подал заявление', 'asd', '2014-06-26 15:20:15', 2),
(50, 36, 'Новая заявка', NULL, '2014-06-26 16:03:58', 1),
(51, 37, 'Новая заявка', NULL, '2014-06-26 16:05:41', 1),
(52, 38, 'Новая заявка', NULL, '2014-06-26 16:06:21', 1),
(53, 39, 'Новая заявка', NULL, '2014-06-26 16:08:13', 1),
(54, 40, 'Новая заявка', NULL, '2014-06-26 16:09:36', 1),
(55, 41, 'Новая заявка', NULL, '2014-07-03 10:12:09', 1),
(56, 42, 'Новая заявка', NULL, '2014-07-03 14:51:26', 1),
(57, 43, 'Новая заявка', NULL, '2014-07-03 15:23:10', 1),
(58, 44, 'Новая заявка', NULL, '2014-07-03 16:01:04', 1),
(59, 45, 'Новая заявка', NULL, '2014-07-03 16:12:18', 1),
(60, 46, 'Новая заявка', NULL, '2014-07-04 15:15:57', 1),
(61, 47, 'Новая заявка', NULL, '2014-07-08 17:33:42', 1),
(62, 48, 'Новая заявка', NULL, '2014-07-08 17:35:56', 1),
(63, 49, 'Новая заявка', NULL, '2014-07-17 16:31:44', NULL),
(64, 50, 'Новая заявка', NULL, '2014-07-17 16:32:01', NULL),
(65, 51, 'Новая заявка', NULL, '2014-07-17 16:32:35', NULL),
(66, 52, 'Новая заявка', NULL, '2014-08-15 13:24:44', NULL),
(67, 53, 'Новая заявка', NULL, '2014-08-15 13:25:01', NULL),
(68, 54, 'Новая заявка', NULL, '2014-08-15 13:25:43', NULL),
(69, 55, 'Новая заявка', NULL, '2014-08-15 14:40:47', NULL),
(70, 56, 'Новая заявка', NULL, '2014-08-15 14:45:19', NULL),
(71, 57, 'Новая заявка', NULL, '2014-08-15 14:45:36', NULL),
(72, 58, 'Новая заявка', NULL, '2014-08-15 14:46:26', NULL),
(73, 59, 'Новая заявка', NULL, '2014-08-15 14:46:44', NULL),
(74, 60, 'Новая заявка', NULL, '2014-08-15 14:47:49', NULL),
(75, 61, 'Новая заявка', NULL, '2014-08-15 14:48:06', NULL),
(76, 62, 'Новая заявка', NULL, '2014-08-15 14:48:23', NULL),
(77, 63, 'Новая заявка', NULL, '2014-08-15 14:48:33', NULL),
(78, 64, 'Новая заявка', NULL, '2014-08-15 14:49:13', NULL),
(79, 65, 'Новая заявка', NULL, '2014-08-15 14:49:24', NULL),
(80, 66, 'Новая заявка', NULL, '2014-08-15 14:50:18', NULL),
(81, 67, 'Новая заявка', NULL, '2014-08-15 14:54:05', NULL),
(82, 68, 'Новая заявка', NULL, '2014-08-15 14:54:28', NULL),
(83, 69, 'Новая заявка', NULL, '2014-08-15 14:54:44', NULL),
(84, 70, 'Новая заявка', NULL, '2014-08-15 14:57:46', NULL),
(85, 71, 'Новая заявка', NULL, '2014-08-15 15:40:41', NULL),
(86, 72, 'Новая заявка', NULL, '2014-08-15 15:40:52', NULL),
(87, 73, 'Новая заявка', NULL, '2014-08-15 15:41:34', NULL),
(88, 74, 'Новая заявка', NULL, '2014-08-15 15:50:08', NULL),
(89, 75, 'Новая заявка', NULL, '2014-08-15 19:02:06', NULL),
(90, 76, 'Новая заявка', NULL, '2014-08-15 19:36:38', NULL),
(91, 77, 'Новая заявка', NULL, '2014-08-18 15:36:15', NULL),
(92, 78, 'Новая заявка', NULL, '2014-08-18 15:54:37', NULL),
(93, 79, 'Новая заявка', NULL, '2014-08-18 15:59:39', NULL),
(94, 80, 'Новая заявка', NULL, '2014-08-18 16:02:40', NULL),
(95, 81, 'Новая заявка', NULL, '2014-08-18 16:03:41', NULL),
(96, 82, 'Новая заявка', NULL, '2014-08-18 16:06:15', NULL),
(97, 81, 'Новая заявка', '', '2015-01-20 14:19:00', 1),
(98, 17, '', 'сказал туфта, а не товар', '2015-01-24 13:03:28', 1),
(99, 17, '', 'сказал туфта, а не товар', '2015-01-24 13:03:41', 1),
(100, 82, '', '', '2015-01-27 18:02:13', 1),
(101, 80, '', '', '2015-01-27 18:02:40', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `file`
--

CREATE TABLE IF NOT EXISTS `file` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `parent_id` int(10) unsigned NOT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mime` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `extension` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `size` int(10) unsigned NOT NULL,
  `width` int(10) unsigned NOT NULL,
  `height` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `create_time` int(10) unsigned NOT NULL,
  `update_time` int(10) unsigned NOT NULL,
  `path` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `parent_id` (`parent_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `filecategory`
--

CREATE TABLE IF NOT EXISTS `filecategory` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `hidden` tinyint(4) DEFAULT NULL,
  `title` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `good`
--

CREATE TABLE IF NOT EXISTS `good` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `img` varchar(200) NOT NULL,
  `price` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `good`
--

INSERT INTO `good` (`id`, `user_id`, `name`, `description`, `img`, `price`, `amount`) VALUES
(1, 0, 'Часы Ulise Nardine', 'Классные часы китайской сборки.', '', 650, 12),
(2, 0, 'G-Shock', 'Классные китайские часы', '', 650, 20);

-- --------------------------------------------------------

--
-- Структура таблицы `meta`
--

CREATE TABLE IF NOT EXISTS `meta` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Дамп данных таблицы `meta`
--

INSERT INTO `meta` (`id`, `key`, `value`, `parent_id`) VALUES
(1, 'invalid', 'Некорректные данные\r\nНе заполнил заявку\r\nДубляж\r\n5-ый звонок. Не берет трубку.\r\nФИО не соответсвует заявке\r\nНомер телефона не существует\r\nАбонент не доступен', 1),
(2, 'valid', 'Новая заявка\r\nСтараемся дозвониться\r\nДумает\r\nПерестал брать трубку\r\nОтправили письмо, думает, перезвоним \r\nОтказ, т.к. дорого (заявка)\r\nОтказ, т.к. дорого (анкета)\r\nОтказ – нет технической возможности\r\nЖдем анкету\r\nПодает заявление в филиале\r\nЗвонок - напоминание об анкете\r\nПодписание договора\r\nЖдем оплату\r\nПодал заявление\r\nПередумал\r\nХочет поступить позже\r\nНет результатов ЕГЭ\r\nНе может приехать в филиал или в Москву для поступления', 1),
(3, 'new_status', 'Новая заявка', 1),
(4, 'admin_email', '', 2),
(5, 'method', 'mail', 2),
(6, 'smtp_host', '', 2),
(7, 'smtp_login', '', 2),
(8, 'smtp_pass', '', 2),
(9, 'smtp_port', '', 2),
(10, 'smtp_ssl', '', 2),
(11, 'subject', '', 2),
(12, 'expired_status', 'Просрочена', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `options`
--

CREATE TABLE IF NOT EXISTS `options` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `optname` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `options`
--

INSERT INTO `options` (`id`, `optname`) VALUES
(1, 'FeedbackStatusOptions'),
(2, 'MailOptions');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `salt` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `date_created` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `email` (`email`(4)),
  KEY `username` (`username`(4))
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `email`, `username`, `password`, `salt`, `role`, `status`, `date_created`) VALUES
(1, 'admin@example.com', 'admin', '235c3072d3dd58d88ed495cb746b7fe4', 'lqrDqJ7TCVenHcr', 'admin', 1, NULL),
(2, 'manager@gmail.com', 'manager', '2b03d93f5f07521adf0f0667de9b50b0', 'LSNsSqwQ1yF9cvX', 'manager', 1, 1403101627);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
